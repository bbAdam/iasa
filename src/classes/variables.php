<?php

// SETTING UP THIS WAY HELPS KEEP THINGS OUT OF THE GLOBAL SPACE WHILE HAVING GLOBAL ACCESS
class Variables {

    function __construct() {
        $this->variables = array(
            "siteName" => "Illinois Association of School Administrators",
            "siteDomain" => "https://adamcruse.schoolwires.net",
            "homepageUrl" => "/Page/1",
            "subpageUrl" => "/Page/17",
            "subpageNoNavUrl" => "/Page/2",
            "siteAlias" => "dev1",
            "siteAddress" => "330 Innovation Blvd.",
            "siteCity" => "State College",
            "siteState" => "PA",
            "siteZip" => "16801",
            "sitePhone" => "814.272.7300",
            "siteFax" => "012.345.6789",
        );
    }

    public function Get($variable) {
        if(array_key_exists($variable, $this->variables)) {
            return $this->variables[$variable];
        } else {
            return null;
        }
    }

}

?>