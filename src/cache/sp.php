

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">

<html lang="en">
<head>
    <title>Employment / Sample Page</title>
    <!--
    <PageMap>
    <DataObject type="document">
    <Attribute name="siteid">4</Attribute>
    </DataObject>
    </PageMap>
    -->

    
    <meta property="og:type" content="website" />
<meta property="fb:app_id" content="411584262324304" />
<meta property="og:url" content="http%3A%2F%2Fadamcruse.schoolwires.net%2Fsite%2Fdefault.aspx%3FPageID%3D17" />
<meta property="og:title" content="Employment / Sample Page" />
<meta name="twitter:card" value="summary" />
<meta name="twitter:title" content="Employment / Sample Page" />
<meta itemprop="name" content="Employment / Sample Page" />

    <!-- Begin swuc.GlobalJS -->
<script type="text/javascript">
 staticURL = "https://adamcruse.schoolwires.net/Static/";
 SessionTimeout = "500";
 BBHelpURL = "";
</script>
<!-- End swuc.GlobalJS -->

    <script src='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/sri-failover.min.js' type='text/javascript'></script>

    <!-- Stylesheets -->
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Light.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Italic.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Regular.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-SemiBold.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/shepherd/shepherd-theme-default.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/App_Themes/SW/jquery.jgrowl.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static//site/assets/styles/system_2530.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static//site/assets/styles/apps.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/App_Themes/SW/jQueryUI.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/SchoolwiresMobile_2320.css" />
    
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/Styles/Grid.css" />

    <!-- Scripts -->
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/WCM-2520/WCM.js" type="text/javascript"></script>
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/WCM-2520/API.js" type="text/javascript"></script>
    <script src='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/jquery-3.0.0.min.js' type='text/javascript'></script>
    <script src='https://adamcruse.schoolwires.net/Static//GlobalAssets/Scripts/min/jquery-migrate-1.4.1.min.js' type='text/javascript'></script
    <script src='https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js' type='text/javascript'
        integrity='sha384-JO4qIitDJfdsiD2P0i3fG6TmhkLKkiTfL4oVLkVFhGs5frz71Reviytvya4wIdDW' crossorigin='anonymous'
        data-sri-failover='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/swfobject.js'></script>
    <script src='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/tether/tether.min.js' type='text/javascript'></script>
    <script src='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/shepherd/shepherd.min.js' type='text/javascript'></script>
   
    <script type="text/javascript">
        $(document).ready(function () {
            SetCookie('SWScreenWidth', screen.width);
            SetCookie('SWClientWidth', document.body.clientWidth);
            
            $("div.ui-article:last").addClass("last-article");
            $("div.region .app:last").addClass("last-app");

            // get on screen alerts
            var isAnyActiveOSA = 'False';
            var onscreenAlertCookie = GetCookie('Alerts');

            if (onscreenAlertCookie == '' || onscreenAlertCookie == undefined) {
                onscreenAlertCookie = "";
            }
            if (isAnyActiveOSA == 'True') {
                GetContent(homeURL + "/cms/Tools/OnScreenAlerts/UserControls/OnScreenAlertDialogListWrapper.aspx?OnScreenAlertCookie=" + onscreenAlertCookie + "&SiteID=4", "onscreenalert-holder", 2, "OnScreenAlertCheckListItem();");
            }

            

        });

    // ADA SKIP NAV
    $(document).ready(function () {
        $(document).on('focus', '#skipLink', function () {
            $("div.sw-skipnav-outerbar").animate({
                marginTop: "0px"
            }, 500);
        });

        $(document).on('blur', '#skipLink', function () {
            $("div.sw-skipnav-outerbar").animate({
                marginTop: "-30px"
            }, 500);
        });
    });

    // ADA MYSTART
    $(document).ready(function () {
        var top_level_nav = $('.sw-mystart-nav');

        // Set tabIndex to -1 so that top_level_links can't receive focus until menu is open
        // school dropdown
        $(top_level_nav).find('ul').find('a').attr('tabIndex', -1);

        // my account dropdown
        $(top_level_nav).next('ul').find('a').attr('tabIndex', -1);

        var openNavCallback = function(e, element) {
             // hide open menus
            hideMyStartBarMenu();

            // show school dropdown
            if ($(element).find('ul').length > 0) {
                $(element).find('.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false');
                $(element).find('.sw-dropdown').find('li:first-child a').focus()
            }

            // show my account dropdown
            if ($(element).next('ul').length > 0) {
                $(element).next('.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false');
                $(element).next('.sw-dropdown').find('li:first-child a').focus();
                $('#sw-mystart-account').addClass("clicked-state");
            }
        }

        $(top_level_nav).click(function (e) {
            openNavCallback(e, this);
        });

        $('.sw-dropdown-list li').click(function(e) {
            e.stopImmediatePropagation();
            $(this).focus();
        });
        
        // Bind arrow keys for navigation
        $(top_level_nav).keydown(function (e) {
            if (e.keyCode == 37) { //key left
                e.preventDefault();

                // This is the first item
                if ($(this).prev('.sw-mystart-nav').length == 0) {
                    $(this).parents('div').find('.sw-mystart-nav').last().focus();
                } else {
                    $(this).prev('.sw-mystart-nav').focus();
                }
            } else if (e.keyCode == 38) { //key up
                e.preventDefault();

                // show school dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('div.sw-dropdown').css('display', 'block').find('ul').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }

                // show my account dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('ul.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }
            } else if (e.keyCode == 39) { //key right
                e.preventDefault();

                // This is the last item
                if ($(this).next('.sw-mystart-nav').length == 0) {
                    $(this).parents('div').find('.sw-mystart-nav').first().focus();
                } else {
                    $(this).next('.sw-mystart-nav').focus();
                }
            } else if (e.keyCode == 40) { //key down
                e.preventDefault();

                // show school dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('div.sw-dropdown').css('display', 'block').find('ul').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
                }

                // show my account dropdown
                if ($(this).next('ul').length > 0) {
                    $(this).next('ul.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
                }
            } else if (e.keyCode == 13 || e.keyCode == 32) { //enter key
                // If submenu is hidden, open it
                e.preventDefault();

                
                openNavCallback(e, this);
                $(this).parent('li').find('ul[aria-hidden=true]').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
            } else if (e.keyCode == 27) { //escape key
                e.preventDefault();
                hideMyStartBarMenu();
            } else {
                $(this).parent('.sw-mystart-nav').find('ul[aria-hidden=false] a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        return false;
                    }
                });
            }
        });

        // school dropdown
        var startbarlinks = $(top_level_nav).find('ul').find('a');
        bindMyStartBarLinks(startbarlinks);

        // my account dropdown
        var myaccountlinks = $(top_level_nav).next('ul').find('a');
        bindMyStartBarLinks(myaccountlinks);

        function bindMyStartBarLinks(links) {
            $(links).keydown(function (e) {
                e.stopPropagation();

                if (e.keyCode == 38) { //key up
                    e.preventDefault();

                    // This is the first item
                    if ($(this).parent('li').prev('li').length == 0) {
                        if ($(this).parents('ul').parents('.sw-mystart-nav').length > 0) {
                            $(this).parents('ul').parents('.sw-mystart-nav').focus();
                        } else {
                            $(this).parents('ul').prev('.sw-mystart-nav').focus();
                        }
                    } else {
                        $(this).parent('li').prev('li').find('a').first().focus();
                    }
                } else if (e.keyCode == 40) { //key down
                    e.preventDefault();

                    if ($(this).parent('li').next('li').length == 0) {
                        if ($(this).parents('ul').parents('.sw-mystart-nav').length > 0) {
                            $(this).parents('ul').parents('.sw-mystart-nav').focus();
                        } else {
                            $(this).parents('ul').prev('.sw-mystart-nav').focus();
                        }
                    } else {
                        $(this).parent('li').next('li').find('a').first().attr('tabIndex', 0);
                        $(this).parent('li').next('li').find('a').first().focus();
                    }
                } else if (e.keyCode == 27 || e.keyCode == 37) { // escape key or key left
                    e.preventDefault();
                    hideMyStartBarMenu();
                } else if (e.keyCode == 32) { //enter key
                    e.preventDefault();
                    window.location = $(this).attr('href');
                } else {
                    var found = false;

                    $(this).parent('div').nextAll('li').find('a').each(function () {
                        if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                            $(this).focus();
                            found = true;
                            return false;
                        }
                    });

                    if (!found) {
                        $(this).parent('div').prevAll('li').find('a').each(function () {
                            if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                                $(this).focus();
                                return false;
                            }
                        });
                    }
                }
            });
        }
        
        // Hide menu if click or focus occurs outside of navigation
        $('#sw-mystart-inner').find('.sw-mystart-nav').last().keydown(function (e) {
            if (e.keyCode == 9) {
                // If the user tabs out of the navigation hide all menus
                hideMyStartBarMenu();
            }
        });

        /*$(document).click(function() { 
            hideMyStartBarMenu();
        });*/

        // try to capture as many custom MyStart bars as possible
        $('.sw-mystart-button').find('a').focus(function () {
            hideMyStartBarMenu();
        });

        $('#sw-mystart-inner').click(function (e) {
            e.stopPropagation();
        });

        $('ul.sw-dropdown-list').blur(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-mypasskey').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-sitemanager').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-myview').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-signin').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-register').focus(function () {
            hideMyStartBarMenu();
        });

        // button click events
        $('div.sw-mystart-button.home a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.pw a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.manage a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('#sw-mystart-account').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).addClass('clicked-state');
                $('#sw-myaccount-list').show();
            }
        });

        $('#sw-mystart-mypasskey a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.signin a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.register a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });
    });

    function hideMyStartBarMenu() {
        $('.sw-dropdown').attr('aria-hidden', 'true').css('display', 'none');
        $('#sw-mystart-account').removeClass("clicked-state");
    }

    // ADA CHANNEL NAV
    $(document).ready(function() {
        var channelCount;
        var channelIndex = 1;
        var settings = {
            menuHoverClass: 'hover'
        };

        // Add ARIA roles to menubar and menu items
        $('[id="channel-navigation"]').attr('role', 'menubar').find('li a').attr('role', 'menuitem').attr('tabindex', '0');

        var top_level_links = $('[id="channel-navigation"]').find('> li > a');
        channelCount = $(top_level_links).length;


        $(top_level_links).each(function() {
            $(this).attr('aria-posinset', channelIndex).attr('aria-setsize', channelCount);
            $(this).next('ul').attr({ 'aria-hidden': 'true', 'role': 'menu' });

            if ($(this).parent('li.sw-channel-item').children('ul').length > 0) {
                $(this).attr('aria-haspopup', 'true');
            }

            var sectionCount = $(this).next('ul').find('a').length;
            var sectionIndex = 1;
            $(this).next('ul').find('a').each(function() {
                $(this).attr('tabIndex', -1).attr('aria-posinset', sectionIndex).attr('aria-setsize', sectionCount);
                sectionIndex++;
            });
            channelIndex++;

        });

        $(top_level_links).focus(function () {
            //hide open menus
            hideChannelMenu();

            if ($(this).parent('li').find('ul').length > 0) {
                $(this).parent('li').addClass(settings.menuHoverClass).find('ul').attr('aria-hidden', 'false').css('display', 'block');
            }
        });

        // Bind arrow keys for navigation
        $(top_level_links).keydown(function (e) {
            if (e.keyCode == 37) { //key left
                e.preventDefault();

                // This is the first item
                if ($(this).parent('li').prev('li').length == 0) {
                    $(this).parents('ul').find('> li').last().find('a').first().focus();
                } else {
                    $(this).parent('li').prev('li').find('a').first().focus();
                }
            } else if (e.keyCode == 38) { //key up
                e.preventDefault();

                if ($(this).parent('li').find('ul').length > 0) {
                    $(this).parent('li').addClass(settings.menuHoverClass).find('ul').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }
            } else if (e.keyCode == 39) { //key right
                e.preventDefault();

                // This is the last item
                if ($(this).parent('li').next('li').length == 0) {
                    $(this).parents('ul').find('> li').first().find('a').first().focus();
                } else {
                    $(this).parent('li').next('li').find('a').first().focus();
                }
            } else if (e.keyCode == 40) { //key down
                e.preventDefault();

                if ($(this).parent('li').find('ul').length > 0) {
                    $(this).parent('li')
                         .addClass(settings.menuHoverClass)
                         .find('ul.sw-channel-dropdown').css('display', 'block')
                         .attr('aria-hidden', 'false')
                         .find('a').attr('tabIndex', 0)
                         .first().focus();
                }
            } else if (e.keyCode == 13 || e.keyCode == 32) { //enter key
                // If submenu is hidden, open it
                e.preventDefault();

                $(this).parent('li').find('ul[aria-hidden=true]').attr('aria-hidden', 'false').addClass(settings.menuHoverClass).find('a').attr('tabIndex', 0).first().focus();
            } else if (e.keyCode == 27) { //escape key
                e.preventDefault();
                hideChannelMenu();
            } else {
                $(this).parent('li').find('ul[aria-hidden=false] a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        return false;
                    }
                });
            }
        });

        var links = $(top_level_links).parent('li').find('ul').find('a');

        $(links).keydown(function (e) {
            if (e.keyCode == 38) {
                e.preventDefault();

                // This is the first item
                if ($(this).parent('li').prev('li').length == 0) {
                    $(this).parents('ul').parents('li').find('a').first().focus();
                } else {
                    $(this).parent('li').prev('li').find('a').first().focus();
                }
            } else if (e.keyCode == 40) {
                e.preventDefault();

                if ($(this).parent('li').next('li').length == 0) {
                    $(this).parents('ul').parents('li').find('a').first().focus();
                } else {
                    $(this).parent('li').next('li').find('a').first().focus();
                }
            } else if (e.keyCode == 27 || e.keyCode == 37) {
                e.preventDefault();
                $(this).parents('ul').first().prev('a').focus().parents('ul').first().find('.' + settings.menuHoverClass).removeClass(settings.menuHoverClass);
            } else if (e.keyCode == 32) {
                e.preventDefault();
                window.location = $(this).attr('href');
            } else {
                var found = false;

                $(this).parent('li').nextAll('li').find('a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        found = true;
                        return false;
                    }
                });

                if (!found) {
                    $(this).parent('li').prevAll('li').find('a').each(function () {
                        if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                            $(this).focus();
                            return false;
                        }
                    });
                }
            }
        });

        function hideChannelMenu() {
            $('li.sw-channel-item.' + settings.menuHoverClass).removeClass(settings.menuHoverClass).find('ul').attr('aria-hidden', 'true').css('display', 'none').find('a').attr('tabIndex', -1);
        }
        
        // Hide menu if click or focus occurs outside of navigation
        $('[id="channel-navigation"]').find('a').last().keydown(function (e) {
            if (e.keyCode == 9) {
                // If the user tabs out of the navigation hide all menus
                hideChannelMenu();
            }
        });

        $('[id="channel-navigation"]').find('a').first().keydown(function (e) {
            if (e.keyCode == 9) {
                // hide open MyStart Bar menus
                hideMyStartBarMenu();
            }
        });

        /*$(document).click(function() {
            hideChannelMenu();
        });*/

        $('[id="channel-navigation"]').click(function (e) {
            e.stopPropagation();
        });
    });

    $(document).ready(function() {
        $('input.required').each(function() {
            if ($('label[for="' + $(this).attr('id') + '"]').length > 0) {
                if ($('label[for="' + $(this).attr('id') + '"]').html().indexOf('recStar') < 0) {
                    $('label[for="' + $(this).attr('id') + '"]').prepend('<span class="recStar" aria-label="required item">*</span> ');
                }
            }
        });

        $(document).ajaxComplete(function() {
            $('input.required').each(function() {
                if ($('label[for="' + $(this).attr('id') + '"]').length > 0) {
                    if ($('label[for="' + $(this).attr('id') + '"]').html().indexOf('recStar') < 0) {
                        $('label[for="' + $(this).attr('id') + '"]').prepend('<span class="recStar" aria-label="required item">*</span> ');
                    }
                }
            });
        });
    });
    </script>

    <!-- Page -->
    
    <style type="text/css">/* MedaiBegin Standard */@import "https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/157/reset.css";

@charset "UTF-8";
/* TEMPLATE CSS COMPILED FROM SASS */

/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ---------------------------- ### GLOBAL ### ---------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Global */
/* ----- FONTS ----- */
@font-face {
  font-family: 'template-icon';
  src: url("data:application/x-font-ttf;charset=utf-8;base64,AAEAAAALAIAAAwAwT1MvMg8SBf4AAAC8AAAAYGNtYXAXVtKaAAABHAAAAFRnYXNwAAAAEAAAAXAAAAAIZ2x5ZhzuKxAAAAF4AAAMOGhlYWQdvJY+AAANsAAAADZoaGVhCMoE0QAADegAAAAkaG10eFLhA+8AAA4MAAAAYGxvY2EfIBvCAAAObAAAADJtYXhwAB0AowAADqAAAAAgbmFtZa8Uj4wAAA7AAAABknBvc3QAAwAAAAAQVAAAACAAAwPBAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADpEwPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAOAAAAAoACAACAAIAAQAg6RP//f//AAAAAAAg6QD//f//AAH/4xcEAAMAAQAAAAAAAAAAAAAAAQAB//8ADwABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAABAAD/wAQEA8AAWAAAEzwBNTQ3PgE3NjcxNjc+ATc2MzoBMzE6ATMyFx4BFxYXMRYXHgEXFhUcARUxHAEVFAcOAQcGBzEGBw4BBwYjKgEjMSoBIyInLgEnJicxJicuAScmNTwBNRUACgsmHBwiIygpWzIxNQIDAQIDAjQxMVooKCIjHRwoCgsKCygcHSMiKChaMTE0AgMCAQMCNTEyWykoIyIcHCYLCgHCAgMCNDExWigoIiIcHCYLCgoLJhwcIiIoKVoyMjUBAQEBAwE1MjJcKCkiIhwcJgsKCgsmHBwiIigoWjExNAMGAwEAAAH/+gA7A+EDZQBYAAATIQEOARUUFjMyNjcjATc1MDQ1MDQ1MTA0MTwBOQE8ATE8ATkBPAE1PAE1MTwBNTwBNTE8ATE8ATkBMDQ9AScBLgEjIgYVFBYXMQEhKgEjIgYVFBYzOgEzMTMDDv7yAwMeFQYMBgEBYwcH/qAGGA4VHg8MAQv88gIDARUeHhUBAwIBkv7zBQwGFR4DAwFjCAQBAQEBAgECAQEBAQEDAQICAgECAQICAQEBAQEEAgQJAWMMDh0VDhgH/vEeFRQeAAEASf+3A9MDwAAGAAATARUBNQkBSQOK/HYDEPzwA8D+VbH+U5sBZwFoAAAAAQAAAJEEEgMVAAUAACUJAScJAQOX/nL+cnsCCQIJkQGP/nF7Agn99wAAAAABAAD/wAQHA8AACwAABQkBIwkBMwkBMwkBA47+df52eQHI/lp6AWcBaHr+WgHIQAHN/jMCFAHs/lkBp/4U/ewAAgAAAUsD7AIAABoAOQAAASEGIiMiJicxLgE1PAE5ATwBNTQ2MzoBMzEhIyEwMjMyFhcxHgEVHAEVMRwBFRQGBzEOASMqAScxIQMc/T8BAgISHwsMDjMkAQIBAkFLAUEBARIgDAwODgwLHxIBAgH+vwFMAQ4MDCASAQEBAgEkMw4MCx8SAgIBAQIBER8MDA4BAAAAAAEAAP/ABAADwAB4AAABHgEVHAEVMRwBFRQGBzEOASMwIiMzIREwFDEUBgcxDgEjMCI5ATAiIyImJzEuATU8ATUxESEqATEiJicxLgE1MDQ5ATA0NTQ2NzE+ATM6ATMxIRE0JjU0NjcxPgEzOgE5ATAyMTIWFzEeARUwFBUxESEwMjMyFhcxA+YMDg4MDCETAQEB/rgODA0hEwIBARIhDAwO/rgBAhMhDAwODgwMIRIBAgEBSAEPDAwhEgEBAhMhDQwOAUgBARIhDAIBCyASAQIBAQIBEiEMDA7+uwEUIg0MDg4MDSETAQEBAUUODA0hEwIBARIhDA0PAUYBAwESIAwMDg4MDCETAgH+ug8NAAAAAQAA/8AEAAPAAEkAAAEnNTQmIzEjIgYVMRUnLgEnMQ4BBzEBDgEVMBQxFBYzMDI5ATMRFBYzMTMRNDYzMTMyFhUxETMyNjUxETMwMjEyNjUwNDkBNCYnA91dJRtAGyWACyEUFCEL/mMPFCUaAUAlG8AlG4AbJcAbJUABGiUUDwIAXeMbJSUbI4APEgICEg/+YxAbFQEaJf6AGyUBQBslJRv+wCUbAYAlGgEVGxAAAAACALr/0gMvA8AAIQAwAAABIgcOAQcGFTEUFx4BFxYxMDc+ATc2NTQnLgEnJiM4ATkBESImNTQ2MzIWFTEUBiMxAfRBOTlVGRkxMXYxMTIxdjExGRlVOTpBNElJNDRKSjQDwBkYVjk5QUF1dONUU1NU43R1QUE5OVYYGf5ISjQ0SUk0NEoAAAAAAQDF/8YDOgPAAAYAAAkCNwkBJwLC/gMB/Xj+ewGFeAPA/gP+A3gBhQGFeAABAMX/xgM6A8AABgAAEwkBFwkBB8UBhf57eAH9/gN4A0j+e/57eAH9Af14AAIAgP+/A2sDwAADAAcAABMzESMBMxEjgLW1Aja1tQPA+/8EAfv/AAADAAAAfAQCAw8AGgArADwAABMhMjY1NCYjMSEwIiMiBhUwFBUxFBYzOAE5AQUhIgYVFBYzMSEyNjU0JiMxESEiBhUUFjMxITI2NTQmIzEpA7ARGBgR/FABARAXGBEDsPxQERgYEQOwERgYEf1ZERgYEQKnERgYEQK8GRERGBcRAQERGM4YEREYGBERGP7hGRERGBgRERkAAAQA6f+6Aw0DwAADABIAKQA7AAAlIREhAyImNTQ2MzIWFTEUBiMxAzMeARUUBgcxIyoBIyImNTQ2MzoBMzE3ISIGFREUFhchPgE1ETQmIzEC6/4gAeDwFh4eFhUfHxUzZwYICAZnAQEBBwoKBwEBAe/+hyMyMiMBeSQyMiRmAq78yB8VFR8fFRUfA58BCQcGCgEKBwcKRTIk/KYjMgEBMiMDWiQyAAAAAAIAAf/BBAADvwAsAEoAACUOASMiJy4BJyY1NDc+ATc2MzIXHgEXFhUUBgc3AR4BFRQGBzEOASMiJicxAScyNz4BNzY1NCcuAScmIyIHDgEHBhUxFBceARcWMwKCMXhCVEpKbiAgICBuSkpUVEpKbiAgKCQBAQoKCgoKCRgODhgJ/vXtQTg5VBkYGBlUOThBQDk4VRgZGRhVODlA3iMoICBuSkpUVEpKbiAgICBuSkpUQngyAf71CRgODhgJCQsLCQELExgZVDk4QUA5OFUYGRkYVTg5QEE4OVQZGAABAAD/vwMzA8AAKgAAExEwFBUUFjMxMhYzMjYzIwE+ATU0JicxLgEnIwEuASMiBgcxDgEVMBQ5AQAOCgEEAgEEAgEDAgYGAgICBAIB/P8CBwQGCwMCAgOm/DQBAQkPAQEB6QQMBwQIAwIEAgHlAgIGBQMHBAEAAAABAAD/wAHoA8AAAwAACQEjAQHo/nhgAYgDwPwABAAAAAL////CBQ0DvQBAAKAAAAE0NicmBg8BBgcOAQcGBw4BIyoBIzEqASMiJiMiBhUUFhUxFgYVFBYXOgEzOgEzMhYXMRYXHgEXFhceATc+ATURAT4BNTQmJzEuASMiBgcxDgEHMQcOAQ8BLgEnLgEnLgEjIgYVFBYXMR4BHwEHDgEHDgEVFBYXMR4BMzI2NzE+AT8BFx4BFzEeATMyNjczPgE1NCYnMy4BJy4BJzc+AT8BAq8BEhEdDREpKChQKCcnDB8SAgQCIEIgAgQCHCkBAwMiKBw7HAIFAhktESgpKVIqKSkLGBAPBAJTBwgIBwcTCwkRBgcLBm0IEQkBDRcMGDEaBxUNFB0LCgoVCnA4GDEWCAkJCAYRCQsTBgsSCXR3ChULBg8ICxMGAQUGBgYBBxAHIEEjiQcNBgEDhBAgCQgUCwwhIiFDIiIiCwwBKRwCBQI0bDQnIQETECMiI0UiIiIJEgcHHQ8Dj/7dBhIKChIHBwgFBgUMBm8IDgYBDx0NGTMWCgwdFQwWBwoVCnE6GC8ZBhMLCxMGBgcJBwkTCXN7CxMJBQUJCAcPCQkQBgkPCCBBI4IHDQcBAAAAAAH////CAq8DvQBAAAABNDYnJgYPAQYHDgEHBgcOASMqASMxKgEjIiYjIgYVFBYVMRYGFRQWFzoBMzoBMzIWFzEWFx4BFxYXHgE3PgE1EQKvARIRHQ0RKSgoUCgnJwwfEgIEAiBCIAIEAhwpAQMDIigcOxwCBQIZLREoKSlSKikpCxgQDwQDhBAgCQgUCwwhIiFDIiIiCwwBKRwCBQI0bDQnIQETECMiI0UiIiIJEgcHHQ8DjwAAAAACAAD/wAQAA8AALgB2AAAFISImNTERNDYzMSE6ATMyFhUUBiMiJiMzIREhETQmNTQ2MzIWFRwBFTURFAYjMQkBFTAUMRQWFzEeATMyNjcxPgE9ATQwNTQmJzEuASMqASMxIyIwIyIGBzEOARUUFhcxHgEzMDIxMwEOARUUFhcxHgEzMjY3MQOx/J4gLy8gAaEBAwEVHR0VAQMCAf5yAzwBHRUUHS8g/sgBLgcFBhAKCRAGBgcIBgYPCQEBAfkBAQgPBgYHBwYGDwkBjf7XBgcHBgYQCQkQBkAvIANiIC8dFBUdAfzEAbQBAwEVHR0VAQMCAf45IC8CPAEskAEJDwYFBwcFBhAJ+QEBCRAGBggHBgYQCQkQBgYH/tMGEAkJEQYGBwcGAAAAAAEAAAABAAAoDyInXw889QALBAAAAAAA3Mso5QAAAADcyyjl//r/twUNA8AAAAAIAAIAAAAAAAAAAQAAA8D/wAAABQr/+v/uBQ0AAQAAAAAAAAAAAAAAAAAAABgEAAAAAAAAAAAAAAACAAAABAQAAAQA//oEAABJBAAAAAQHAAAEAAAABAAAAAQAAAAEAAC6BAAAxQQAAMUEAACABAAAAAQAAOkEAAABAzQAAAHoAAAFCv//Aq3//wQAAAAAAAAAAAoAFAAeAJQA+gEQASYBRgGQAhgCcAK0AsoC4AL0A0ADlAQCBD4ETgUsBYgGHAAAAAEAAAAYAKEABAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAOAK4AAQAAAAAAAQAIAAAAAQAAAAAAAgAHAGkAAQAAAAAAAwAIADkAAQAAAAAABAAIAH4AAQAAAAAABQALABgAAQAAAAAABgAIAFEAAQAAAAAACgAaAJYAAwABBAkAAQAQAAgAAwABBAkAAgAOAHAAAwABBAkAAwAQAEEAAwABBAkABAAQAIYAAwABBAkABQAWACMAAwABBAkABgAQAFkAAwABBAkACgA0ALBkaXN0cmljdABkAGkAcwB0AHIAaQBjAHRWZXJzaW9uIDEuMABWAGUAcgBzAGkAbwBuACAAMQAuADBkaXN0cmljdABkAGkAcwB0AHIAaQBjAHRkaXN0cmljdABkAGkAcwB0AHIAaQBjAHRSZWd1bGFyAFIAZQBnAHUAbABhAHJkaXN0cmljdABkAGkAcwB0AHIAaQBjAHRGb250IGdlbmVyYXRlZCBieSBJY29Nb29uLgBGAG8AbgB0ACAAZwBlAG4AZQByAGEAdABlAGQAIABiAHkAIABJAGMAbwBNAG8AbwBuAC4AAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA") format('truetype');
  font-weight: normal;
  font-style: normal;
}
/* ----- GENERAL ----- */
@-ms-viewport {
  width: device-width;
}
body {
  -webkit-text-size-adjust: none;
  -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
}

body.nav-open {
  overflow: hidden;
}

body:before {
  content: "desktop";
  display: none;
}

*[data-toggle=false] {
  display: none !important;
}

/* HIDDEN ELEMENTS */
#sw-mystart-outer,
#sw-footer-outer,
.hidden,
.hp-row.mobile,
.cs-fullscreen-video-background-photo {
  display: none;
}

.ui-clear:after {
  content: "";
}

.content-wrap {
  max-width: 1192px;
  padding: 0px 30px;
  margin: 0 auto;
  position: relative;
}

button {
  cursor: pointer;
  background: transparent;
  border: 0px;
  padding: 0px;
  margin: 0px;
}

div.sw-special-mode-bar {
  position: fixed !important;
  bottom: 0px;
  left: 0px;
  width: 100%;
}

#gb-page {
  width: 100%;
  position: relative;
  overflow: hidden;
  background: #fff;
}

/* ----- GLOBAL ICONS ----- */
#gb-icons {
  padding: 38px 0 52px;
}
#gb-icons .content-wrap {
  max-width: 870px;
}
#gb-icons::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: #000000;
  opacity: 0.62;
  opacity:.62 - default;
}
#gb-icons[data-icon-num="0"] {
  display: none;
}
#gb-icons[data-background-opacity="100"]::before {
  opacity: 1;
}

.cs-global-icons {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
}
.cs-global-icons li {
  width: 25%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  padding: 12px 20px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}
.cs-global-icons a {
  color: #fff;
  text-decoration: none;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  border: 2px solid;
  width: 179px;
  height: 143px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  padding: 20px 10px 10px;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
.cs-global-icons a:hover, .cs-global-icons a:focus {
  background: #fff;
  color: #686868;
  border-color: #fff;
}
.cs-global-icons .icon {
  font-size: 3.2rem;
}
.cs-global-icons .text {
  text-transform: uppercase;
  font-family: Fjalla One, sans-serif;
  font-size: 0.875rem;
  max-width: 120px;
  text-align: center;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  min-height: 40px;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ----------------------------- ### APPS ### ----------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Apps */
/* ----- GENERAL ----- */
.flexpage .ui-article a,
.announcements .ui-article a {
  color: #7e1416;
}

div.ui-widget-header p {
  margin-top: 10px;
}

.sp div.ui-widget-detail h1,
.sp div.ui-widget-detail h2,
.sp div.ui-widget-detail h3,
.sp div.ui-widget-detail h4 {
  margin-bottom: 5px;
}

.ui-widget-header.ui-helper-hidden {
  margin: 0px !important;
  padding: 0px !important;
}

div.ui-widget.app div.ui-widget-header:empty {
  display: none;
}

div.ui-widget.app div.ui-widget-header {
  position: relative;
  font-size: 1.9343rem;
  font-weight: 600;
  margin-bottom: 18px;
  color: #7e1416;
}

div.ui-widget.app div.ui-widget-header h1 {
  font-weight: inherit;
  font-size: inherit;
  margin: 0;
}

div.ui-widget-detail p:first-of-type {
  margin-top: 0px;
}

div.ui-article {
  margin-bottom: 30px;
}

h1.ui-article-title {
  font-size: 1.25rem;
  color: #7e1416;
  line-height: 1.2;
  font-weight: 600;
}

.view-calendar-link,
.more-link,
div.app-level-social-rss a.ui-btn-toolbar.rss {
  opacity: 1;
  display: inline-block;
  background: #7e1416;
  color: #fff;
  padding: 9px 12px 8px 37px;
  text-decoration: none;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
  position: relative;
  text-transform: capitalize;
  font-size: 0.94rem;
  margin: 0;
}
.view-calendar-link::before,
.more-link::before,
div.app-level-social-rss a.ui-btn-toolbar.rss::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e901";
  position: absolute;
  left: 12px;
  top: 8px;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
.view-calendar-link span,
.more-link span,
div.app-level-social-rss a.ui-btn-toolbar.rss span {
  padding: 0;
  line-height: 1;
  font-size: inherit;
  height: auto;
  background: none;
  display: inline-block;
}
.view-calendar-link:hover, .view-calendar-link:focus,
.more-link:hover,
.more-link:focus,
div.app-level-social-rss a.ui-btn-toolbar.rss:hover,
div.app-level-social-rss a.ui-btn-toolbar.rss:focus {
  background: #686868;
}
.view-calendar-link:hover::before, .view-calendar-link:focus::before,
.more-link:hover::before,
.more-link:focus::before,
div.app-level-social-rss a.ui-btn-toolbar.rss:hover::before,
div.app-level-social-rss a.ui-btn-toolbar.rss:focus::before {
  -webkit-transform: rotate(-35deg);
          transform: rotate(-35deg);
}

div.app-level-social-rss a.ui-btn-toolbar.rss span::after {
  display: none;
}


.accordion-setup .ui-widget.app.tabbed-content .content-accordion-button span {
	color:#231f20;
}
/* ----- NAVIGATION (SHORTCUTS & PAGELIST) ----- */
div.ui-widget.app.navigation li {
  border-bottom: 2px solid #c2c4c2;
  padding: 7px 30px 7px;
  position: relative;
}
div.ui-widget.app.navigation li::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: #c2c4c2;
  opacity: 0;
  width: 0;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
div.ui-widget.app.navigation li.hover::before {
  width: 100%;
  opacity: 0.4;
}
div.ui-widget.app.navigation li:last-child {
  border: 0;
}
div.ui-widget.app.navigation li a {
  color: #3a3a3a;
  display: table;
  position: relative;
}
div.ui-widget.app.navigation li div.bullet {
  background: none;
  position: absolute;
  left: 0;
  top: 0;
  height: 37px;
  font-size: 8px;
  width: 30px;
  text-align: center;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  padding-top: 15px;
}
div.ui-widget.app.navigation li div.bullet::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
div.ui-widget.app.navigation li div.bullet.expandable {
  cursor: pointer;
  background: none;
}
div.ui-widget.app.navigation li div.bullet.expandable::before {
  content: "\e906";
}
div.ui-widget.app.navigation li div.bullet.collapsible {
  cursor: pointer;
  background: none;
}
div.ui-widget.app.navigation li div.bullet.collapsible::before {
  content: "\e905";
}
div.ui-widget.app.navigation li ul {
  position: relative;
  margin: 7px 0 -2px;
  padding: 6px 0 0;
}
div.ui-widget.app.navigation li ul::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  height: 2px;
  background: #c2c4c2;
  width: calc(100% + 60px);
  left: -30px;
}
div.ui-widget.app.navigation li ul li {
  border: 0 !important;
  padding: 4px 24px;
}
div.ui-widget.app.navigation li ul li .bullet {
  width: 24px;
  padding-top: 11px;
}
div.ui-widget.app.navigation li ul li .bullet::before {
  content: "\e90a";
}
div.ui-widget.app.navigation li ul li a {
  font-size: 0.91rem;
}
div.ui-widget.app.navigation.pagenavigation li {
  border-bottom: 1px solid #cee1df;
}
div.ui-widget.app.navigation.pagenavigation li:last-child {
  border: 0;
}
div.ui-widget.app.navigation.pagenavigation li::before {
  background: #cee1df;
}
div.ui-widget.app.navigation.pagenavigation li.active a {
  font-weight: 500;
}
div.ui-widget.app.navigation.pagenavigation li.active::before {
  width: 100%;
  opacity: 0.4;
}
div.ui-widget.app.navigation.pagenavigation li ul::before {
  height: 1px;
  background: #cee1df;
}

/* ----- ANNOUNCEMENTS ----- */
.announcements li .ui-article {
  position: relative;
}
.announcements li .ui-article::after {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  height: 1px;
  top: auto;
  bottom: -8px;
  background: #c7c6c5;
}
.announcements li:last-child .ui-article::after {
  display: none;
}

/* ----- UPCOMING EVENTS ----- */
h1.sw-calendar-block-date {
  color: #fff;
  font-family: Fjalla One, sans-serif;
  text-transform: uppercase;
  font-weight: 400;
}
h1.sw-calendar-block-date span {
  display: block;
}
h1.sw-calendar-block-date .adam-hug {
  line-height: 1.1;
}
h1.sw-calendar-block-date .joel-month {
  font-size: 1.17rem;
  padding-top: 8px;
}
h1.sw-calendar-block-date .jeremy-date {
  font-size: 4.26rem;
}
h1.sw-calendar-block-date .joel-day {
  display: none;
}

.upcoming-column.right {
  padding: 26px 25px;
}
.upcoming-column.right .ui-article-description {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: reverse;
      -ms-flex-direction: column-reverse;
          flex-direction: column-reverse;
  margin-bottom: 16px;
}
.upcoming-column.right .sw-calendar-block-title {
  color: #7e1416;
  line-height: 1.1;
  font-weight: 500;
}
.upcoming-column.right .sw-calendar-block-title a:hover,
.upcoming-column.right .sw-calendar-block-title a:focus {
  text-decoration: underline;
}
.upcoming-column.right .sw-calendar-block-time {
  font-size: 0.94rem;
  padding-bottom: 5px;
}

/* ----- HOMEPAGE APPS ----- */
.ui-hp div.ui-widget.app div.ui-widget-header {
  font-size: 1.875rem;
  font-family: Fjalla One, sans-serif;
  text-transform: uppercase;
  letter-spacing: 1.7px;
  font-weight: 400;
}
.ui-hp div.ui-widget.app div.ui-widget-header p {
  font-size: 100%;
  text-transform: none;
  font-family: Work Sans, sans-serif;
  letter-spacing: 0;
}
.ui-hp div.ui-widget.app div.ui-widget-header a {
  text-transform: capitalize;
  font-size: 0.94rem;
  font-family: Work Sans, sans-serif;
  letter-spacing: 0;
}
.ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true] {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}
.ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true] * {
  position: relative;
}
.ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true] h1 {
  padding-right: 26px;
}
.ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true] a {
  border-left: 26px solid;
}
.ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true][data-slider-controls=true] a:nth-child(2), .ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true][data-slider-controls=true] a:nth-child(3) {
  border-left: 13px solid;
}
.ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true]::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  height: 2px;
  background: #c2c1c0;
  top: calc(50% + 1px);
}

.site-shortcuts-columns {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  max-width: 980px;
  margin: 0 auto;
}

.site-shortcuts-column {
  margin: 0;
  padding: 0;
  width: 254px;
  padding: 0 20px;
}

/* ----- HEADLINES ----- */
.headlines-column {
  width: 33.3333%;
}
.headlines-column ul {
  padding: 0;
  margin: 0;
  list-style: none outside none;
}

.cs-article-wrapper {
  background: #fff;
  -webkit-box-shadow: 0px 0px 3px 1px rgba(35, 31, 32, 0.25);
          box-shadow: 0px 0px 3px 1px rgba(35, 31, 32, 0.25);
  padding-bottom: 26px;
  margin: 23px;
}
.cs-article-wrapper .ui-article {
  padding: 0;
  margin: 0;
}
.cs-article-wrapper .ui-article-thumb {
  float: none;
}
.cs-article-wrapper .ui-article-thumb .img {
  margin: 0;
}
.cs-article-wrapper .ui-article-thumb .img img {
  max-width: none !important;
  max-height: none !important;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
.hover.cs-article-wrapper .ui-article-thumb .img img {
  -webkit-transform: scale(1.1);
          transform: scale(1.1);
}

.cs-article-wrapper .ui-article-title {
  padding: 21px 24px 0;
}
.cs-article-wrapper .ui-article-description {
  padding: 9px 24px 0;
  line-height: 1.3;
}

/* ----- ANNOUNCEMENTS ----- */
/* ----- UPCOMING EVENTS ----- */
[data-event-background-opacity="100"] .hp-row:not(.five) .upcomingevents::before {
  opacity: 1;
}

.hp-row:not(.five) .upcomingevents {
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}
.hp-row:not(.five) .upcomingevents::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: #000000;
  opacity: 0.62;
  opacity:.49 - default;
}
.hp-row:not(.five) .upcomingevents::after {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  height: 43px;
  top: auto;
  bottom: 0;
}
.hp-row:not(.five) .upcomingevents .ui-widget-footer {
  padding: 0;
}
.hp-row:not(.five) .upcomingevents .ui-articles {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin-right: -1px;
}
.hp-row:not(.five) .upcomingevents .ui-articles .ui-article {
  margin-bottom: 0;
  height: 100%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}
.hp-row:not(.five) .upcomingevents .ui-articles li {
  -ms-flex-preferred-size: 20%;
      flex-basis: 20%;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
}
.hp-row:not(.five) .upcomingevents .ui-articles .upcoming-column.left {
  position: relative;
}
.hp-row:not(.five) .upcomingevents .ui-articles .upcoming-column.left::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  left: calc(100% - 1px);
  width: 2px;
  background: #fff;
}
.hp-row:not(.five) .upcomingevents .ui-articles .adam-hug {
  min-height: 179px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: start;
      -ms-flex-align: start;
          align-items: flex-start;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  padding: 20px 33px;
}
.hp-row:not(.five) .upcomingevents .ui-articles .adam-hug::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  opacity: 0;
  background-color: #000000;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
.hp-row:not(.five) .upcomingevents .ui-articles .joel-month,
.hp-row:not(.five) .upcomingevents .ui-articles .jeremy-date {
  position: relative;
}
.hp-row:not(.five) .upcomingevents .ui-articles .joel-day {
  display: block;
  font-family: Work Sans, sans-serif;
  background: #e5e5e5;
  color: #282727;
  font-size: 0.92rem;
  border: 2px solid #fff;
  border-top-width: 4px;
  border-bottom: 0;
  padding: 15px 30px 14px;
  letter-spacing: 2.5px;
  position: relative;
}

.hp-row:not(.five) .upcomingevents .ui-articles li:first-child .joel-day {
  border-left: 0;
}
.hp-row:not(.five) .upcomingevents .ui-articles li:nth-child(5) .joel-day {
  border-right: 0;
}
.hp-row.three .upcomingevents .ui-articles .joel-day,
.hp-row.three .upcomingevents .ui-articles li:first-child .joel-day,
.hp-row.three .upcomingevents .ui-articles li:nth-child(5) .joel-day {
	border:2px solid #fff;
}
.hp-row:not(.five) .upcomingevents .ui-articles .upcoming-column.right {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
}
.hp-row:not(.five) .upcomingevents .cs-article-slider-control-wrapper {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}
#gb-page .hp-row:not(.five) .upcomingevents .cs-article-slider-control {
  display: none;
  border-radius: 0;
  background: #7e1416;
  color: #fff;
  text-decoration: none;
  width: auto;
  height: auto;
  margin: 0;
  padding: 9px 12px 8px;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
#gb-page .hp-row:not(.five) .upcomingevents .cs-article-slider-control span {
  padding: 0;
  line-height: 1;
  font-size: inherit;
  height: auto;
  background: none;
  display: inline-block;
}
#gb-page .hp-row:not(.five) .upcomingevents .cs-article-slider-control::before {
  display: none;
}
#gb-page .hp-row:not(.five) .upcomingevents .cs-article-slider-control:hover, #gb-page .hp-row:not(.five) .upcomingevents .cs-article-slider-control:focus {
  background: #686868;
}

.hp-row:not(.five) .upcomingevents .hover .adam-hug::before {
  opacity: 0.5;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ---------------------------- ### HEADER ### ---------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Header */
.header-row {
  position: relative;
}
.header-row.one {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  z-index: 10000;
}
.header-row.two {
  padding: 19px 0px;
  margin-bottom: -45px;
  overflow: hidden;
}

.header-column {
  position: relative;
}

#gb-header {
  z-index: 100;
  position: relative;
  background: #fff;
}

/* ----- MYSTART ----- */
.cs-mystart-dropdown,
.cs-mystart-button {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  position: relative;
}
.cs-mystart-dropdown::before,
.cs-mystart-button::before {
  display: block;
  content: "";
  position: absolute;
  right: 100%;
  width: 2px;
  background: #fff;
  height: 100%;
  top: 0;
  z-index: 2;
  min-height: 36px;
}
.cs-mystart-dropdown::after,
.cs-mystart-button::after {
  display: block;
  content: "";
  position: absolute;
  left: 100%;
  width: 2px;
  background: #fff;
  height: 100%;
  top: 0;
  z-index: 2;
}
.cs-mystart-dropdown.our-schools,
.cs-mystart-button.our-schools {
  position: static;
}

.cs-dropdown-selector,
.cs-button-selector {
  background: #e5e5e5;
  display: block;
  text-decoration: none;
  color: #151515;
  font-family: Work Sans, sans-serif;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  font-size: 0.81rem;
  padding: 14px 10px 10px;
  line-height: 1;
  cursor: pointer;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
.open .cs-dropdown-selector, .cs-dropdown-selector:hover, .cs-dropdown-selector:focus,
.open .cs-button-selector,
.cs-button-selector:hover,
.cs-button-selector:focus {
  background: #686868;
  color: #fff;
}

.cs-dropdown {
  position: absolute;
  top: 100%;
  width: 100%;
  left: 0;
  background: #e5e5e5;
}
.cs-dropdown .cs-dropdown-list {
  list-style: none;
  margin: 0;
  padding: 4px;
  position: relative;
}
.cs-dropdown a {
  color: #231f20;
  text-decoration: none;
  font-size: 0.88rem;
  display: block;
  padding: 4px 35px;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
.cs-dropdown a:hover, .cs-dropdown a:focus {
  background: #fff;
}

.cs-mystart-dropdown.translate .cs-dropdown {
  padding: 20px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  text-align: center;
}

#google_translate_element .goog-te-gadget,
#google_translate_element .goog-logo-link,
#google_translate_element .goog-logo-link:link,
#google_translate_element .goog-logo-link:visited,
#google_translate_element .goog-logo-link:hover,
#google_translate_element .goog-logo-link:active {
  color: #231f20;
}

#google_translate_element {
  text-transform: none;
  position: relative;
}

a#ui-btn-mypasskey {
  height: auto;
}

div#ui-mypasskey-overlay {
  margin-top: 30px;
  padding: 0;
}

/* ----- SCHOOLS ----- */
#gb-schools-wrapper {
  position: absolute;
  width: 100vw;
  height: 386px;
  display: none;
  top: 100%;
  right: 0;
  left: auto;
  z-index: 10000;
}
#gb-schools-wrapper::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: #7e1416;
  opacity: 0.8;
}
.open #gb-schools-wrapper {
  height: 40vh;
}
.ie11 .open #gb-schools-wrapper {
	height:386px;
}
.cs-mega-schools-tabs-master-container {
  max-width: 1192px;
  padding: 0px 30px;
  margin: 0 auto;
  position: relative;
  height: 275px;
  padding: 50px 30px 50px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}
.cs-mega-schools-tabs-master-container .cs-mega-schools-tabs-container {
  margin: 0px -3px;
}
.cs-mega-schools-tabs-master-container .cs-mega-schools-tab {
  background: #686868;
  color: #fff;
  font-size: 1.25rem;
  margin: 0 3px 6px;
  width: 0;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  text-align: center;
  padding: 22px 15px;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
  position: relative;
}
.ie11 .cs-mega-schools-tabs-master-container .cs-mega-schools-tab {
	height:71px;
    box-sizing:border-box;
}
.cs-mega-schools-tabs-master-container .cs-mega-schools-tab.active,
.cs-mega-schools-tabs-master-container .cs-mega-schools-tab:hover {
  background: #fff;
  color: #231f20;
}
.cs-mega-schools-tabs-master-container .cs-mega-schools-tab.active::after {
  opacity: 1;
  height: 6px;
}
.cs-mega-schools-tabs-master-container .cs-mega-schools-tab::after {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: #fff;
  top: 100%;
  opacity: 0;
  height: 0;
  -webkit-transition: all 0.1s ease 0s;
  transition: all 0.1s ease 0s;
}
.cs-mega-schools-tabs-master-container .cs-mega-schools-panel {
  background: #fff;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
}
.cs-mega-schools-tabs-master-container .cs-mega-schools-panel .cs-mega-schools-column-container {
  padding: 34px 30px;
}
.cs-mega-schools-tabs-master-container .cs-mega-schools-panel .cs-mega-schools-column {
  padding: 0 30px;
}
.cs-mega-schools-tabs-master-container .cs-mega-schools-panel li {
  padding: 6px 10px;
}
.cs-mega-schools-tabs-master-container .cs-mega-schools-panel a {
  font-size: 0.812rem;
  text-decoration: none;
  color: #231f20;
  position: relative;
}
.ie11 .cs-mega-schools-tabs-master-container .cs-mega-schools-panel a {
	overflow:hidden;
    width:100%;
    display:block;
}
.ie11 .cs-mega-schools-tabs-master-container .cs-mega-schools-panel li {
	overflow:hidden;
    position:relative;
}
.cs-mega-schools-tabs-master-container .cs-mega-schools-panel a::after {
  background: #7e1416;
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
  height: 1px;
  width: 0;
  top: 100%;
}
.ie11 .cs-mega-schools-tabs-master-container .cs-mega-schools-panel a::after {
	top:calc(100% - 1px);
}
.cs-mega-schools-tabs-master-container .cs-mega-schools-panel a:hover::after, .cs-mega-schools-tabs-master-container .cs-mega-schools-panel a:focus::after {
  width: 100%;
}

/* ----- SEARCH ----- */
#gb-search {
  position: absolute;
  width: 100vw;
  height: 0px;
  top: 100%;
  right: 0;
  left: auto;
  z-index: 10000;
  overflow: hidden;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
#gb-search.open {
  height: 50vh;
}
#gb-search::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: #7e1416;
  opacity: 0.8;
}
#gb-search .content-wrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  height: 100%;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

#gb-search-form {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  background: #fff;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  height: 68px;
}
#gb-search-form label {
  width: 0 !important;
  height: 0 !important;
  display: block !important;
  padding: 0 !important;
  margin: 0 !important;
  border: 0 !important;
  overflow: hidden !important;
}
#gb-search-form input {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  border: 0;
  font-size: 1.312rem;
  font-family: Work Sans, sans-serif;
  margin: 0;
  height: auto;
  padding: 10px 33px;
}
#gb-search-form button {
  width: 110px;
}
#gb-search-form button::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e90e";
  font-size: 34px;
}

#search-control-close {
  background: #231f20;
  color: #fff;
  height: 68px;
  width: 62px;
}
#search-control-close::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e904";
  font-size: 13px;
}

#header-background-title {
  position: absolute;
  right: 16px;
  top: -37px;
  font-size: 12.6rem;
  line-height: 1;
  text-transform: uppercase;
  color: #e5e5e5;
  font-family: Work Sans, sans-serif;
  font-style: italic;
  font-weight: 100;
}

#gb-logo-sitename {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

#gb-logo {
  padding-right: 21px;
}
#gb-logo a {
  display: block;
}
#gb-logo img {
  max-width: 100%;
  height: auto;
}

#gb-sitename h1 {
  font-size: 2.023rem;
  font-family: Fjalla One, sans-serif;
  line-height: 1.08;
  font-weight: 400;
  color: #151515;
}

.sitename {
  display: block;
  text-transform: uppercase;
}

/* ----- CHANNELS ----- */
#gb-channel-bar {
  top: 39px;
}

#sw-channel-list-container {
  background: #7e1416;
  width: 100%;
}

#navc-HP {
  padding-left: 0;
}
#navc-HP a {
  width: 44px;
}
#navc-HP span {
  display: none;
  text-align: center;
}
#navc-HP span::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e907";
  font-size: 21px;
}

ul.sw-channel-list li.sw-channel-item {
  height: 45px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  padding: 0px 8px;
  margin: 0;
}
ul.sw-channel-list li.sw-channel-item > a {
  color: #FFF;
  display: block;
  height: auto;
  padding: 0 10px;
  text-decoration: none;
  font-size: 0.875rem;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  position: relative;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
  z-index: 8000;
}
ul.sw-channel-list li.sw-channel-item > a::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
  background: #fff;
  width: 0;
  left: 50%;
  height: 51px;
  opacity: 0;
  top: -16px;
  -webkit-box-shadow: 0px 0px 4px 0px rgba(0, 0, 0, 0.3);
          box-shadow: 0px 0px 4px 0px rgba(0, 0, 0, 0.3);
}
ul.sw-channel-list li.sw-channel-item > a span {
  position: relative;
  padding: 0;
}
ul.sw-channel-list li.sw-channel-item.hover > a, ul.sw-channel-list li.sw-channel-item.active > a {
  color: #7e1416;
}
ul.sw-channel-list li.sw-channel-item.hover > a::before, ul.sw-channel-list li.sw-channel-item.active > a::before {
  opacity: 1;
  width: 100%;
  left: 0;
}

/* ----- DROPDOWNS ----- */
ul.sw-channel-dropdown {
  top: 100%;
  left: 8px;
  position: absolute;
  width: 170px;
  height: auto;
  max-height: 500px;
  overflow: auto;
  border: none;
  padding: 11px 4px;
  text-align: left;
  background: #fff;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  -webkit-box-shadow: 0px 0px 4px 0px rgba(0, 0, 0, 0.3);
          box-shadow: 0px 0px 4px 0px rgba(0, 0, 0, 0.3);
}
ul.sw-channel-dropdown li {
  width: auto;
}
ul.sw-channel-dropdown li a {
  font-size: 0.875rem;
  color: #231f20;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
  width: auto;
  padding: 5px 25px;
}
ul.sw-channel-dropdown li a:hover, ul.sw-channel-dropdown li a:focus {
  background: #dcdbdb;
}

.sw-channel-more-li {
  background: none transparent;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* -------------------------- ### HOMEPAGE ### ---------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Homepage */
.hp-row {
  position: relative;
}

.hp-column {
  position: relative;
}

/* ----- SLIDESHOW ----- */
[data-slideshow-option="Streaming Video"] .hp-row,
#gb-footer {
  z-index: 11;
  position: relative;
  background: #fff;
}

[data-slideshow-option="Streaming Video"] .multimedia-gallery {
  display: none;
}

#hp-slideshow-outer {
  position: relative;
  overflow: hidden;
}

#hp-video-content {
  z-index: 10;
  position: absolute;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
}

.cs-fullscreen-video-control-text {
  display: none;
}
[data-slideshow-option="Streaming Video"] #hp-slideshow-outer #hp-slideshow .mmg-description-outer {
	background:#fff;
}
#hp-slideshow-outer #hp-slideshow .mmg-description {
  background: #fff;
  position: relative;
  max-width: 900px;
}
#hp-slideshow-outer #hp-slideshow .mmg-description.sv {
	margin:0 auto;
    padding-top:45px;
    padding-bottom:26px;
}
#hp-slideshow-outer #hp-slideshow .mmg-description:not(.sv) {
  max-width: 1192px;
  padding: 0px 30px;
  margin: 0 auto;
  position: relative;
  padding-top: 32px;
  padding-bottom: 26px;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-title {
  font-family: Fjalla One, sans-serif;
  text-transform: uppercase;
  color: #7e1416;
  font-size: 1.25rem;
  text-align: center;
  font-weight: 400;
  letter-spacing: 1px;
  font-style: normal;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-caption {
  color: #000;
  text-align: center;
  font-family: Work Sans, sans-serif;
  font-size: 0.875rem;
  padding-top: 2px;
  line-height: 1.6;
  margin:5px 0;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-links {
  position: absolute;
  right: -12px;
  top: -16px;
  z-index: 11;
}
#hp-slideshow-outer #hp-slideshow .sv .mmg-description-links {
	top:-56px;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-link {
  font-family: Work Sans, sans-serif;
  font-size: 0.9375rem;
  color: #fff;
  background: #7e1416;
  text-decoration: none;
  position: relative;
  padding: 8px 12px 7px 37px;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-link:hover, #hp-slideshow-outer #hp-slideshow .mmg-description-link:focus {
  background: #686868;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-link:hover::before, #hp-slideshow-outer #hp-slideshow .mmg-description-link:focus::before {
  -webkit-transform: rotate(-35deg);
          transform: rotate(-35deg);
}
#hp-slideshow-outer #hp-slideshow .mmg-description-link::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e901";
  position: absolute;
  left: 13px;
  top: 10px;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
#hp-slideshow-outer #hp-slideshow .mmg-control,
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button {
  background: none;
  position: relative;
  width: 58px;
  height: 58px;
  border-radius: 100%;
  background: #7e1416;
  top: auto;
  left: auto;
  border: 3px solid #fff;
  color: #fff;
  margin: 0 7px;
  font-size: 23px;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
  text-align:center;
}
#hp-slideshow-outer #hp-slideshow .mmg-control:hover, #hp-slideshow-outer #hp-slideshow .mmg-control:focus,
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button:hover,
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button:focus {
  background: #686868;
}
#hp-slideshow-outer #hp-slideshow .mmg-control::before,
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
#hp-slideshow-outer #hp-slideshow .mmg-control.back::before,
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button.back::before {
  content: "\e909";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.next::before,
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button.next::before {
  content: "\e90a";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.play-pause.playing::before,
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button.pause-button::before {
  content: "\e90b";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.play-pause.paused::before, 
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button.play-button::before {
  content: "\e90f";
  padding-left:4px;
}
#hp-slideshow-outer #hp-slideshow .mmg-control.mute-button::before,
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button.mute-button::before {
  content: "\e911";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.unmute-button::before,
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button.unmute-button::before {
  content: "\e912";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.watch-video-button::before,
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button.watch-video-button::before {
  content: "\e913";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.play-pause span {
  display: none;
}
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button::before {
	display:block;
    padding-top:16px;
}
.mmg-control-wrapper,
.cs-fullscreen-video-buttons {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  position: absolute;
  width: 100%;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  bottom: -26px;
}

.cs-fullscreen-video-buttons {
  bottom: auto;
  top: -29px;
}

.video-muted .cs-fullscreen-video-button.mute-button,
.video-playing .cs-fullscreen-video-button.play-button,
.video-unmuted .cs-fullscreen-video-button.unmute-button,
.video-paused .cs-fullscreen-video-button.pause-button {
  display: none;
}

.hp-row.three {
  background: #e5e5e5;
  padding: 43px 0 0;
}
.hp-row.three .app {
  padding-bottom: 43px;
}
.hp-row.three .ui-widget-header {
  text-align: center;
}
.hp-row.three .headlines .ui-articles {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin: 0 -23px;
}
.hp-row.three .upcomingevents .ui-widget-header {
  padding-bottom: 16px;
  margin-bottom: 0;
  background: #e5e5e5;
}
.hp-row.three .upcomingevents .upcoming-column.right {
  background: #e5e5e5;
}
.hp-row.three .upcomingevents::after {
  background: #e5e5e5;
}
.hp-row.three .navigation a {
  margin: 0 auto;
  text-align: center;
}
.hp-row.three .navigation li {
  padding: 7px 20px;
}
.hp-row.three .ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true] h1 {
  background: #e5e5e5;
}
.ie11 .hp-row.three .ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true] h1 {
	flex-grow:1;
    -webkit-flex-grow:1;
}
.hp-row.three .ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true] a {
  border-color: #e5e5e5;
}
.hp-row.three div.ui-widget.app.headlines div.ui-widget-header {
  margin-bottom: 5px;
}

.hp-row.four {
  padding: 48px 0 0;
}
.hp-row.four .app {
  padding-bottom: 43px;
}
.hp-row.four .upcomingevents .ui-widget-header {
  padding-bottom: 16px;
  margin-bottom: 0;
  background: #fff;
}
.hp-row.four .upcomingevents .ui-widget-header[data-more-link=true]::before {
  top: calc(50% - 8px) !important;
}
.hp-row.four .upcomingevents .upcoming-column.right, .hp-row.four .upcomingevents::after {
  background: #fff;
}
.ie11 .hp-row.four .ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true] h1 {
	flex-grow:1;
    -webkit-flex-grow:1;
}
.hp-row.four .navigation li {
  padding: 7px 20px;
}
.hp-row.four .headlines .ui-articles {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin: 0 -23px;
}
.hp-row.four .ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true] h1 {
  background: #fff;
}
.hp-row.four div.ui-widget.app.headlines div.ui-widget-header {
  margin-bottom: 5px;
}

.hp-row.five {
  background: #7e1416;
  padding-top: 56px;
}
.hp-row.five .double-wrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin: 0 -20px;
}
.hp-row.five .double-wrap .hp-column {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  width: 0;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  padding: 0 20px;
}
.hp-row.five .double-wrap .hp-column .fb_iframe_widget {
  width: 100%;
}
.hp-row.five .double-wrap .hp-column .fb_iframe_widget span {
  width: 100% !important;
}
.hp-row.five .double-wrap .hp-column .fb_iframe_widget span iframe {
  width: 100% !important;
}
.hp-row.five .double-wrap .hp-column iframe {
  max-width: 100%;
}
.hp-row.five .app {
  color: #fff;
  padding-bottom: 30px;
}
.hp-row.five .app .flexpage .ui-article a,
.hp-row.five .app .annoucements .ui-article a,
.hp-row.five .app .upcoming-column.right .sw-calendar-block-title {
  color: #fff;
  text-decoration: underline;
}
.hp-row.five .app.navigation li a {
  color: #fff;
}
.hp-row.five .app h1.ui-article-title {
  color: #fff;
}
.hp-row.five div.ui-widget.app div.ui-widget-header {
  color: #fff;
}
.hp-row.five div.ui-widget.app div.ui-widget-header * {
  color: #fff;
}
.hp-row.five .site-shortcuts-columns {
  display: block;
}
.hp-row.five .site-shortcuts-column {
  width: auto;
  padding: 0;
}
.hp-row.five .view-calendar-link,
.hp-row.five .more-link,
.hp-row.five div.app-level-social-rss a.ui-btn-toolbar.rss {
  background: #fff;
  color: #7e1416;
}
.hp-row.five .view-calendar-link:hover, .hp-row.five .view-calendar-link:focus,
.hp-row.five .more-link:hover,
.hp-row.five .more-link:focus,
.hp-row.five div.app-level-social-rss a.ui-btn-toolbar.rss:hover,
.hp-row.five div.app-level-social-rss a.ui-btn-toolbar.rss:focus {
  background: #686868;
  color: #fff;
}
.hp-row.five .upcoming-column.left {
	background:#fff;
    padding:10px 30px;
}
.hp-row.five .app.upcomingevents h1.ui-article-title {
	color:#7e1416;
}
/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* --------------------------- ### SUBPAGE ### ---------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Subpage */
.sp .header-row.two {
  margin-bottom: 0;
  padding-bottom: 12px;
}
.sp .header-row.three::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: #231f20;
  height: 50%;
  top: auto;
  bottom: 0;
}
.sp #gb-channel-bar {
  top: auto;
}

#sp-breadcrumbs {
  background: #231f20;
  padding: 3px 0 6px;
}

#sp-content {
  min-height: 500px;
}

.sp-row.two {
  padding-top: 73px;
}
.sp-row.two .content-wrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.ui-return {
  display: block;
  margin-bottom: 30px;
}

ul.ui-breadcrumbs {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  width: 100%;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
}
ul.ui-breadcrumbs > li {
  position: relative;
  color: #fff;
  font-size: 0.813rem;
  margin: 0 10px;
  font-weight: 600;
}
ul.ui-breadcrumbs > li.ui-breadcrumb-first > a > span {
  display: none;
}
ul.ui-breadcrumbs > li > a {
  background: none;
  text-decoration: none;
  color: #fff;
}
ul.ui-breadcrumbs > li > a:hover, ul.ui-breadcrumbs > li > a:focus {
  text-decoration: underline;
}
ul.ui-breadcrumbs > li::after {
  content: "\e902";
  position: absolute;
  right: -5px;
  top: 5px;
  font-size: 8px;
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
ul.ui-breadcrumbs > li:last-child {
  font-weight: 400;
}
ul.ui-breadcrumbs > li:last-child::after {
  display: none;
}

.sp-column.one {
  min-width: 250px;
}
.sp-column.one div.ui-widget.app div.ui-widget-header {
  margin-bottom: 4px;
  font-size: 2.32rem;
}

.sp-column.two {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  -webkit-flex-grow: 1;
  padding-left: 25px;
}
.sp-column.two #sw-content-layout-wrapper > div[id*=sw-content-layout] {
  width: calc(100% + 25px);
}
.sp-column.two div.ui-widget.app {
  padding: 0px 25px;
  margin-bottom: 40px;
}
.sp-column.two .upcoming-column.left {
  background: #7e1416;
  padding: 10px 30px;
}
.sp-column.two .upcoming-column.right {
  padding-bottom: 0;
}
.sp.spn .sp-column.two {
  padding-left: 0;
  margin-left:-25px;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ---------------------------- ### FOOTER ### ---------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Footer */
.footer-row {
  position: relative;
}

.footer-column {
  position: relative;
}

.footer-row.one .content-wrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}
.footer-row.one .footer-column {
  -ms-flex-preferred-size: 50%;
      flex-basis: 50%;
  height: 300px;
}
.footer-row.one .footer-column.one {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

#gb-social {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  padding: 26px 20px 26px 0;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
}

.gb-social-media-icon {
  background-image: url("https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/157/social-sprite.svg");
  background-size: 556px 75px;
  background-repeat: no-repeat;
  width: 35px;
  height: 35px;
  margin: 0px 5px;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.gb-social-media-icon.facebook {
  background-position: 9px -2px;
}
.gb-social-media-icon.facebook:hover, .gb-social-media-icon.facebook:focus {
  background-position: 9px -42px;
}
.gb-social-media-icon.twitter {
  background-position: -52px -2px;
}
.gb-social-media-icon.twitter:hover, .gb-social-media-icon.twitter:focus {
  background-position: -52px -42px;
}
.gb-social-media-icon.youtube {
  background-position: -114px -2px;
}
.gb-social-media-icon.youtube:hover, .gb-social-media-icon.youtube:focus {
  background-position: -114px -42px;
}
.gb-social-media-icon.instagram {
  background-position: -176px 0px;
}
.gb-social-media-icon.instagram:hover, .gb-social-media-icon.instagram:focus {
  background-position: -176px -40px;
}
.gb-social-media-icon.linkedin {
  background-position: -240px 2px;
}
.gb-social-media-icon.linkedin:hover, .gb-social-media-icon.linkedin:focus {
  background-position: -240px -38px;
}
.gb-social-media-icon.vimeo {
  background-position: -299px -2px;
}
.gb-social-media-icon.vimeo:hover, .gb-social-media-icon.vimeo:focus {
  background-position: -299px -42px;
}
.gb-social-media-icon.flickr {
  background-position: -360px -2px;
}
.gb-social-media-icon.flickr:hover, .gb-social-media-icon.flickr:focus {
  background-position: -360px -42px;
}
.gb-social-media-icon.pinterest {
  background-position: -421px -2px;
}
.gb-social-media-icon.pinterest:hover, .gb-social-media-icon.pinterest:focus {
  background-position: -421px -42px;
}
.gb-social-media-icon.rss {
  background-position: -527px -2px;
}
.gb-social-media-icon.rss:hover, .gb-social-media-icon.rss:focus {
  background-position: -527px -42px;
}

#gb-contact {
  background: #e5e5e5;
  position: relative;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  padding-left: 12px;
}
#gb-contact * {
  position: relative;
}
#gb-contact h2 {
  font-family: Fjalla One, sans-serif;
  text-transform: uppercase;
  font-size: 1.58rem;
  margin-bottom: 15px;
  font-style: normal;
  color: #151515;
}
#gb-contact p {
  margin: 0 0 17px;
  font-size: 1.31rem;
  font-weight: 600;
  color: #383838;
}
.contact-info[data-content=""] {
	display:none;
}
#gb-contact::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: #e5e5e5;
  left: auto;
  right: 100%;
  width: 50000px;
}

.contact-column {
  padding-left: 30px;
}
.contact-column[data-content=""] {
	display:none;
}
.contact-column::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  position: absolute;
  left: 0;
  top: 0;
  font-size: 22px;
}
.contact-column.two {
  padding-left: 40px;
}
.contact-column.two::before {
  content: "\e910";
  left: 15px;
  font-size: 15px;
  top: 3px;
}

.address .contact-column.one::before {
  content: "\e908";
}

.phone-fax .contact-column.one::before {
  content: "\e90d";
}

#gb-google-map-link {
  position: absolute;
  left: 0;
  width: 200%;
  height: 100%;
  top: 0;
}

#gb-to-top {
  background: #151515;
  color: #fff;
  text-align: center;
  font-family: Fjalla One, sans-serif;
  text-transform: uppercase;
  font-size: 1.2rem;
  letter-spacing: 1.1px;
  padding: 10px 0 8px;
  cursor: pointer;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
#gb-to-top:hover, #gb-to-top:focus {
  background: #686868;
}
#gb-to-top span {
  position: relative;
}
#gb-to-top span::after {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e903";
  font-size: 13px;
  top: -1px;
  padding-left: 11px;
  position: relative;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
:hover#gb-to-top span::after {
  top: -6px;
}

:focus#gb-to-top span::after {
  top: -6px;
}

.footer-row.three {
  padding: 15px 0px 20px;
}
.footer-row.three .content-wrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}
.footer-row.three .footer-column.two {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: end;
      -ms-flex-align: end;
          align-items: flex-end;
}

#bb-footer-outer {
  background: #000;
  position: relative;
  padding-bottom: 80px;
}

#gb-bb-footer {
  position: relative;
  display: flex;
  display: -webkit-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-align-items: center;
}

.gb-bb-footer.logo {
  padding: 8px 0px 0px;
  margin: 0px 7px 0px -3px;
}
.ie11 .gb-bb-footer.logo {
	background:#000;
    padding:8px;
}
.gb-bb-footer.logo a {
  display: block;
}
.gb-bb-footer.logo img {
  display: block;
  -webkit-filter: invert(1);
          filter: invert(1);
}

.gb-bb-footer.links {
  padding: 2px 0px 0px;
  text-align: left;
}
.gb-bb-footer.links ul {
  list-style: none;
  margin: 0;
  padding: 0;
  display: flex;
  display: -webkit-box;
  display: -ms-flexbox;
  display: -webkit-flex;
}
.gb-bb-footer.links li:last-child a::after {
  display: none;
}
.gb-bb-footer.links a {
  display: inline-block;
  text-decoration: none;
  font-family: 'Arial', "Helvetica Neue", 'Helvetica', sans-serif;
  font-size:12px;
  padding: 3px 10px 2px 4px;
  position: relative;
  margin: 0px 2px 0px 0px;
  color: #000;
}
.gb-bb-footer.links a::after {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: '\e900';
  font-size: 3px;
  position: absolute;
  right: 1px;
  top: 9px;
}

.gb-bb-footer.copyright {
  position: relative;
  right: auto;
  top: auto;
  font-family: 'Arial', "Helvetica Neue", 'Helvetica', sans-serif;
  font-size:12px;
  text-align: left;
  margin: 0px 0px 0px 3px;
  clear: both;
  padding: 0px 0px 0px;
  white-space: nowrap;
  font-style: italic;
  color: #000;
}

.footer-link {
  background: #7e1416;
  color: #fff;
  text-decoration: none;
  margin-left: 10px;
  position: relative;
  display: inline-block;
  padding: 9px 12px 8px 37px;
  font-size: 0.94rem;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
.footer-link:hover, .footer-link:focus {
  background: #686868;
}
.footer-link:hover::before, .footer-link:focus::before {
  -webkit-transform: rotate(-35deg);
          transform: rotate(-35deg);
}
.footer-link span {
  padding: 0;
  line-height: 1;
  font-size: inherit;
  height: auto;
  background: none;
  display: inline-block;
}
.footer-link::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e901";
  position: absolute;
  left: 12px;
  top: 8px;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### EDITOR STYLES ### ------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
html {
  font-size: 16px;
  line-height: 1.375;
}

/* GroupBegin EditorStyles */
body {
  font-family: Work Sans, sans-serif;
  font-weight: 400;
  color: #231f20;
  text-rendering: optimizeLegibility !important;
  -webkit-font-smoothing: antialiased !important;
  -moz-font-smoothing: antialiased !important;
  font-smoothing: antialiased !important;
  -moz-osx-font-smoothing: grayscale;
}

h1,
li h1 {
  font-size: 1.875rem;
  font-weight: 600;
  margin: 0px 0px 0px 0px;
  padding: 0px 0px 0px 0px;
  color: #7e1416;
}

h2 {
  font-size: 1.75rem;
  font-weight: 500;
  font-style: italic;
  margin: 0px 0px 0px 0px;
  padding: 0px 0px 0px 0px;
  color: #7e1416;
}

h3 {
  font-size: 1.31rem;
  font-weight: 600;
  margin: 0px 0px 0px 0px;
  padding: 0px 0px 0px 0px;
  color: #7e1416;
}

h4 {
  font-size: 1rem;
  font-weight: 600;
  margin: 0px 0px 0px 0px;
  padding: 0px 0px 0px 0px;
  color: #7e1416;
}

.title {
  font-weight: bold;
}

.subtitle {
  font-style: italic;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### MEDIA QUERIES ### ------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Media Queries */
/* GroupEnd *//* MediaEnd *//* MediaBegin 768+ */ @media (max-width: 1023px) {@charset "UTF-8";
/* SASS MIXINS */
/* GroupBegin Global */
body:before {
  content: "768";
}

.ui-column-one-quarter.region {
  width: 50%;
  float: left;
  clear: left;
}

.ui-column-one-third.region {
  width: 50%;
  float: left;
}

.ui-column-two-thirds.region {
  width: 50%;
  float: left;
}

.region.right {
  float: left;
}

.region.clearleft {
  clear: none;
}

/* HIDDEN ELEMENTS */
#header-background-title,
.header-row.one,
.header-row.three,
.footer-row.two {
  display: none;
}

#gb-icons {
  padding: 30px 0 35px;
}
#gb-icons .content-wrap {
  padding: 0 18px;
}

.cs-global-icons li {
  padding: 10px;
}
.cs-global-icons a {
  width: 160px;
  height: 128px;
}
.cs-global-icons .icon {
  font-size: 2.8rem;
}

/* GroupEnd */
/* GroupBegin Header */
.header-row.mobile {
  background: #7e1416;
}

#rs-menu-btn {
  float: none;
  color: #fff;
  font-size: 0.93rem;
  height: 36px;
  width: 130px;
}
#rs-menu-btn::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e90c";
  background: transparent;
  font-size: 26px;
  left: 20px;
  top: 6px;
  padding: 0;
  position: absolute;
}
#rs-menu-btn span {
  padding: 9px 0px 0px 63px;
}

.header-row.two {
  margin-bottom: 0;
}
.header-row.two .content-wrap {
  padding: 0 20px;
}

#gb-logo {
  padding-right: 14px;
}

/* GroupEnd */
/* GroupBegin Footer */
.footer-row.one .content-wrap {
  display: block;
  padding: 0;
}
.footer-row.one .footer-column {
  height: auto;
}
.footer-row.one .footer-column.two {
  height: 350px;
}

#gb-social {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  padding: 26px 0;
}

.gb-social-media-icon {
  margin: 0 16px;
}

#gb-contact {
  max-width: 1192px;
  padding: 0px 30px;
  margin: 0 auto;
  position: relative;
  width: 100%;
  padding: 30px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}

#gb-map {
  height: 100%;
}

#gb-google-map-link {
  position: relative;
  width: 100% !important;
}

.footer-row.three {
  padding-top: 23px;
}
.footer-row.three .content-wrap {
  -webkit-box-orient: vertical;
  -webkit-box-direction: reverse;
      -ms-flex-direction: column-reverse;
          flex-direction: column-reverse;
  -webkit-box-align: start;
      -ms-flex-align: start;
          align-items: flex-start;
}
.footer-row.three .footer-column.two {
  padding-bottom: 20px;
}

.footer-link {
  margin: 0 10px 0 0;
}

/* GroupEnd */
/* GroupBegin Apps */
/* GroupEnd */
/* GroupBegin Homepage */
#hp-video-content {
	height: auto !important;
    top: auto;
    bottom: 0px;
}
#hp-slideshow-outer #hp-slideshow .mmg-description:not(.sv) {
  padding-top: 48px;
  padding-bottom: 33px;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-links {
  position: relative;
  right: auto;
  top: auto;
  text-align: center;
  padding-top: 10px;
}
#hp-slideshow-outer #hp-slideshow .sv .mmg-description-links {
	top:auto;
}
.headlines-column {
  width: 50%;
}
.headlines-column:last-child {
  display: none;
}
.site-shortcuts-columns {
	display:block;
}
.site-shortcuts-column {
	margin:0 auto;
}
.hp-row.five {
  padding-top: 26px;
}
.hp-row.five .double-wrap {
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
}
.hp-row.five .double-wrap .hp-column {
  width: 50%;
  max-width: 50%;
}

/* GroupEnd */
/* GroupBegin Subpage */
#sp-content {
  min-height: 0;
}

.sp-row.two {
  padding-top: 40px;
}
.sp-row.two .content-wrap {
  display: block;
  padding: 0 5px;
}
.sp-row.two .sp-column.one {
  padding: 0 25px;
}
.sp-row.two .sp-column.one div.ui-widget.app div.ui-widget-header {
  cursor: pointer;
}
.sp-row.two .sp-column.one div.ui-widget.app div.ui-widget-header::after {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e90a";
  position: absolute;
  right: 5px;
  top: 15px;
  font-size: 20px;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
}
.sp-row.two .sp-column.one .open div.ui-widget-header::after {
  -webkit-transform: rotate(90deg);
          transform: rotate(90deg);
}
.sp-row.two .sp-column.two {
  padding: 0;
}
.sp-row.two .sp-column.two #sw-content-layout-wrapper > div[id*=sw-content-layout] {
  width: 100%;
}
.sp.spn .sp-column.two {
  margin-left:0;
}

/* GroupEnd */} /* MediaEnd *//* MediaBegin 640+ */ @media (max-width: 767px) {/* GroupBegin Global */
body:before {
  content: "640";
}

.show640 {
  display: block;
}

.hide640 {
  display: none;
}

.ui-spn .ui-column-one-third {
  width: auto !important;
  float: none !important;
}

.ui-column-one.region {
  width: auto;
  clear: none;
}

.ui-column-one-quarter.region {
  width: auto;
  float: none;
}

.ui-column-one-half.region {
  width: auto;
  float: none;
}

.ui-column-one-third.region {
  width: auto;
  float: none;
}

.ui-column-two-thirds.region {
  width: auto;
  float: none;
}

.region.right {
  float: none;
}

/* GroupEnd */
/* GroupBegin Header */
/* GroupEnd */
/* GroupBegin Footer */
.gb-bb-footer.links li:nth-child(2) a::after {
  display: none;
}

.gb-bb-footer.links ul {
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  -webkit-flex-wrap: wrap;
}

/* GroupEnd */
/* GroupBegin Apps */
.hp-row.three .site-shortcuts-column {
  margin: 0;
  padding: 0;
  width: auto;
}

.hp-row.four .site-shortcuts-column {
  margin: 0;
  padding: 0;
  width: auto;
}
/* GroupEnd */
/* GroupBegin Homepage */
.cs-fullscreen-video-buttons {
	display:none;
}
#hp-video-content {
	position:relative;
}
#hp-slideshow-outer #hp-slideshow .mmg-description.sv {
	padding-top:15px;
}
/* GroupEnd */
/* GroupBegin Subpage */
/* GroupEnd */} /* MediaEnd *//* MediaBegin 480+ */ @media (max-width: 639px) {/* SASS MIXINS */
/* GroupBegin Global */
body:before {
  content: "480";
}

.show480 {
  display: block;
}

.hide480 {
  display: none;
}

#gb-icons {
  padding: 10px 0;
}
#gb-icons .content-wrap {
  padding: 0;
}

.cs-global-icons li {
  width: 50%;
}
.cs-global-icons a {
  width: 100%;
  height: auto;
  padding-bottom: 5px;
}
.cs-global-icons .text {
  font-size: 0.75rem;
  min-height: 32px;
}
.cs-global-icons .icon {
  font-size: 2rem;
  margin-bottom: 10px;
}

/* GroupEnd */
/* GroupBegin Header */
.header-row.two {
  padding-bottom: 22px;
}
.header-row.two .content-wrap {
  padding: 0 17px;
}

#gb-sitename h1 {
  font-size: 1.39rem;
}

#gb-logo {
  width: 68px;
}

/* GroupEnd */
/* GroupBegin Footer */
#gb-social {
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  padding-bottom: 12px;
}

.gb-social-media-icon {
  margin: 0 14px 18px;
}

#gb-contact {
  padding: 30px 23px 20px;
}
#gb-contact p:last-child {
  margin-bottom: 0;
}
#gb-contact h2 {
  font-size: 1.25rem;
}

.contact-column {
  display: block;
}
.contact-column.two {
  padding-left: 30px;
}
.contact-column.two::before {
  display: none;
}

.footer-row.one .footer-column.two {
  height: 160px;
}

.footer-row.three .content-wrap {
  padding: 0 10px;
}
.footer-row.three .footer-column.two {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-box-align: start;
      -ms-flex-align: start;
          align-items: flex-start;
  padding-bottom: 9px;
}

.footer-link {
  margin-bottom: 6px;
}

#gb-bb-footer {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

.gb-bb-footer.logo {
  margin-right: 0;
}

.gb-bb-footer.links ul {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}
.gb-bb-footer.links a {
  text-align: center;
}
.gb-bb-footer.links li:last-child {
  margin-top: -8px;
}

.gb-bb-footer.copyright {
  text-align: center;
}

/* GroupEnd */
/* GroupBegin Apps */
/* GroupEnd */
/* GroupBegin Homepage */
#hp-slideshow-outer #hp-slideshow .mmg-control-wrapper,
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-buttons {
  bottom: -30px;
}
#hp-slideshow-outer #hp-slideshow .mmg-control,
#hp-slideshow-outer #hp-slideshow .cs-fullscreen-video-button {
  height: 42px;
  width: 42px;
  border-width: 2px;
  margin: 0 4px;
  font-size: 18px;
}
#hp-slideshow-outer #hp-slideshow .mmg-description,
#hp-slideshow-outer #hp-slideshow .mmg-description:not(.sv) {
  padding: 46px 15px 30px;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-caption {
  padding-top: 5px;
  line-height: 1.7;
}

.ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true]::before {
  display: none;
}

.ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true] a {
  border: 0;
  margin-top: 6px;
}

.ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true] {
  display: block;
}

.headlines-column {
  width: 100%;
}
.headlines-column:nth-child(2) {
  display: none;
}
.headlines-column .cs-article-wrapper {
  padding-bottom: 16px;
}
.headlines-column .cs-article-wrapper .ui-article-title {
  padding: 13px 15px 0;
}
.headlines-column .cs-article-wrapper .ui-article-description {
  padding: 9px 15px 0;
}

.hp-row.three .headlines .ui-articles,
.hp-row.four .headlines .ui-articles {
  display: block;
}

.ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true][data-slider-controls=true] .view-calendar-link {
  padding-left: 12px;
  margin-top: 0;
}
.ui-hp div.ui-widget.app div.ui-widget-header[data-more-link=true][data-slider-controls=true] .view-calendar-link::before {
  display: none;
}

.hp-row.five .content-wrap {
  display: block;
}
.hp-row.five .content-wrap .hp-column {
  width: auto;
  max-width: none;
}

/* GroupEnd */
/* GroupBegin Subpage */
/* GroupEnd */} /* MediaEnd *//* MediaBegin 320+ */ @media (max-width: 479px) {/* GroupBegin Default */
body:before {
  content: "320";
}

.show320 {
  display: block;
}

.hide320 {
  display: none;
}

/* GroupEnd */
/* GroupBegin Header */
/* GroupEnd */
/* GroupBegin Footer */
/* GroupEnd */
/* GroupBegin Apps */
/* GroupEnd */
/* GroupBegin Homepage */
/* GroupEnd */
/* GroupBegin Subpage */
/* GroupEnd */} /* MediaEnd */#sw-content-layout-wrapper { background: url("https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Page/Icon/B17.jpg") center center no-repeat;}</style>
    <script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/tools/api/cs.api.min.js"></script>

<script type = 'text/javascript' >
//FOR CUSTOM ALERT APP
$(window).load(function () {
	//console.log(document.location.protocol);
    // AVOID SIGN IN AND SIGNED OUT PAGE - IT BREAKS THE REDIRECT FOR SAML
    if(window.location.href.indexOf("PageType=7") === -1 && window.location.href.indexOf("PageType=8") === -1) {
        if ('https:' == document.location.protocol) {
        	if("eisd" != "hisd" || ("eisd" == "hisd" && $(".spn").length) || ("eisd" == "hisd" && $(".sp").length)) {
                $('#sw-mystart-outer').after('<div id=\'sp-houston-alert-cont\'></div>');

                CSAPI.FlexData.GetFlexData(13980, function(response) {
                    // SUCCESS
                    if(response.successful) {
                        var articles = '';
                        var show = [];
                        $.each(response.data, function(index, article) {

                            var schools = article.cd_schools;
                            var articleClass = "";
                            if (schools != '') {
                                schools = schools.split(';');
                                for (var s in schools) {
                                    var check = schools[s].split(':');
                                    if ('eisd' == check[0] && check[1] == 'true') {
                                        articleClass = check[0] + '-' + check[1];
                                        show.push("true");
                                    } else {
                                    	show.push("false");
                                    }
                                }
                            }

                            articles += '<li class="' + articleClass + '">' +
                                            '<div class="ui-article">' +
                                                '<div class="ui-article-description">' +
                                                    article.cd_Alert +
                                                '</div>' +
                                            '</div>' +
                                        '</li>';
                        });
						
                        if(show.indexOf("true") > -1) {
                            $("#sp-houston-alert-cont").html('' +
                                '<div id="cs-houston-alert-outer">' +
                                    '<div id="cs-houston-alert">' +
                                        '<div class="ui-widget app houston-alert">' +
                                            '<div class="ui-widget-header ui-helper-hidden"></div>' +
                                            '<div class="ui-widget-detail">' +
                                                '<ul class="ui-articles">' +
                                                    articles +
                                                '</ul>' +
                                            '</div>' +
                                            '<div class="ui-widget-footer"></div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '');

                            $('#cs-houston-alert div.ui-widget.app.houston-alert .ui-articles li.eisd-true:eq(0)').css('border-top', '0px none');
                            $('.ui-widget.app.houston-alert .ui-articles li.eisd-true').show();
                            $('#cs-houston-alert-outer').removeClass('hidden');
                        }
                        
                        setTimeout(function(){
                        	var alertBody = document.querySelector('#sp-houston-alert-cont');
                        	sessionStorage.setItem("CS_HISD_CUSTOM_ALERT", alertBody.innerHTML);
                        }, 2000)
                        
                        
                    }

                    // ERROR
                    else {
                        console.log(response.data);
                    }
                });
            }
        }
    }
    
});
</script><meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" type="text/css" href="//extend.schoolwires.com/creative/scripts/creative/tools/creative-icons-v4/css/creativeIcons.v4.min.css" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Fjalla+One&family=Work+Sans:ital,wght@0,400;0,500;0,600;1,100;1,500&display=swap" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="//extend.schoolwires.com/creative/scripts/creative/global/css/cs.global.min.css" />

<script type="text/javascript" src="https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/157/cs.mega.school.tabs.min.js"></script>

<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/global/js/cs.global.min.js"></script>
<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/tools/head.min.js"></script>
<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/tools/streaming-video/cs.fullscreen.video.min.js"></script>
<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/tools/creative-icons-v4/creativeIcons.v4.min.js"></script>
<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/responsive/creative-responsive-menu-v3/creative.responsive.menu.v3.min.js"></script>
<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/joel/mod-events/joel.mod-events.min.js"></script>
<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/tools/creative-translate/creative.translate.min.js"></script>
<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/responsive/creative-app-accordion/creative.app.accordion.min.js"></script>
<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/responsive/creative-article-slider/cs.article.slider.min.js"></script>
<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/tools/tcw-upgrade/cs.tcw.upgrade.min.js"></script>
<script type="text/javascript" src="https://az50000698.schoolwires.net/cms/lib/AZ50000698/Centricity/Domain/4//SLM/school-list-config.txt"></script>


<script type="text/javascript">/******/ (function() { // webpackBootstrap
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

$(window).on("load", function () {
  CreativeTemplate.WindowLoad();
});
$(function () {
  CreativeTemplate.Init();
});
window.CreativeTemplate = {
  // PROPERTIES
  "KeyCodes": {
    "tab": 9,
    "enter": 13,
    "esc": 27,
    "space": 32,
    "end": 35,
    "home": 36,
    "left": 37,
    "up": 38,
    "right": 39,
    "down": 40
  },
  "IsMyViewPage": false,
  // UPDATES IN SetTemplateProps METHOD
  "ShowDistrictHome": true,
  "ShowSchoolList": false,
  // UPDATES IN SetTemplateProps METHOD
  "ShowTranslate": true,
  // METHODS
  "Init": function Init() {
    // FOR SCOPE
    var _this = this;

    csGlobalJs.OpenInNewWindowWarning();
    this.SetTemplateProps();
    this.MyStart();
    this.JsMediaQueries();
    this.SchoolList();
    this.Translate();
    this.Header();
    this.Search();
    this.RsMenu();
    this.ChannelBar();
    this.StickyChannelBar();

    if ($("#gb-page.hp").length) {
      this.Slideshow();
      this.CheckSlideshow();
      this.StreamingVideo();
      this.Homepage();
      this.Shortcuts();
      this.Headlines();
    }

    this.Body();
    this.ModEvents();
    this.GlobalIcons();
    this.SocialIcons();
    this.AppAccordion();
    this.Footer();
    $(window).resize(function () {
      _this.WindowResize();
    });
    $(window).scroll(function () {
      _this.WindowScroll();
    });
  },
  "SetTemplateProps": function SetTemplateProps() {
    // MYVIEW PAGE CHECK
    if ($("#pw-body").length) this.IsMyViewPage = true; // SCHOOL LIST CHECK

    if ($(".sw-mystart-dropdown.schoollist").length) this.ShowSchoolList = true;
  },
  "WindowLoad": function WindowLoad() {},
  "WindowResize": function WindowResize() {
    this.JsMediaQueries();
    this.CheckSlideshow();
    this.Headlines();
    this.Shortcuts();
  },
  "WindowScroll": function WindowScroll() {
    this.CheckSlideshow();
  },
  "JsMediaQueries": function JsMediaQueries() {
    switch (this.GetBreakPoint()) {
      case "desktop":

		break;

      case "768":
        break;

      case "640":
        break;

      case "480":
        break;

      case "320":
        break;
    }
  },
  "MyStart": function MyStart() {
    // FOR SCOPE
    var _this = this; // BUILD USER OPTIONS DROPDOWN


    var userOptionsItems = ""; 
    
    
    // SIGNIN BUTTON
    if ($(".sw-mystart-button.signin").length) {
      userOptionsItems += "<li>" + $(".sw-mystart-button.signin").html() + "</li>";
    } 
    
    // REGISTER BUTTON
    if ($(".sw-mystart-button.register").length) {
      userOptionsItems += "<li>" + $(".sw-mystart-button.register").html() + "</li>";
    }
    
    


    $(".cs-mystart-dropdown.user-options .cs-dropdown-list").html(userOptionsItems); // BIND DROPDOWN EVENTS

    this.DropdownActions({
      "dropdownParent": ".cs-mystart-dropdown.user-options",
      "dropdownSelector": ".cs-dropdown-selector",
      "dropdown": ".cs-dropdown",
      "dropdownList": ".cs-dropdown-list"
    });
  },
  "SchoolList": function SchoolList() {
    $("#gb-schools-wrapper").megaSchoolTabs({
      "tabNames": ["Pre-Kindergarten", "K-6", "K-8", "Middle"], //MUST MATCH SCHOOL TYPE IN SLM
      "columnsInTabs": true,
      "numberOfColumnsPerTab": 4,
      "createRsMenuSchools": true,
      "addSchoolWordToRsMenuItems": true,
      "parentFocusSelector": ".cs-mystart-dropdown.our-schools .cs-dropdown-selector",
      "allLoaded": function allLoaded() {
      	$(".cs-mystart-dropdown.our-schools").mouseleave(function () {
          $("#gb-schools-wrapper").slideUp(300, "swing").attr("aria-hidden","true");
          $(".cs-mystart-dropdown.our-schools .cs-dropdown-selector").attr("aria-expanded","false");
          $(".cs-mystart-dropdown.our-schools .cs-dropdown-selector").parent().removeClass("open");
          setTimeout(function(){
          	if(!$(".cs-mystart-dropdown.translate .cs-dropdown-selector").has(":focus")){
            	$(".cs-mystart-dropdown.our-schools .cs-dropdown-selector").focus();
            }
              
          }, 100);
        });
      }
    });
  },
  "Translate": function Translate() {
    // ADD TRANSLATE
    if (this.ShowTranslate) {
      $(".cs-mystart-dropdown.translate .cs-dropdown").creativeTranslate({
        "type": 2,
        // 1 = FRAMESET, 2 = BRANDED, 3 = API
        "languages": [// ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
        ["Afrikaans", "Afrikaans", "af"], ["Albanian", "shqiptar", "sq"], ["Amharic", "አማርኛ", "am"], ["Arabic", "العربية", "ar"], ["Armenian", "հայերեն", "hy"], ["Azerbaijani", "Azərbaycan", "az"], ["Basque", "Euskal", "eu"], ["Belarusian", "Беларуская", "be"], ["Bengali", "বাঙালি", "bn"], ["Bosnian", "bosanski", "bs"], ["Bulgarian", "български", "bg"], ["Burmese", "မြန်မာ", "my"], ["Catalan", "català", "ca"], ["Cebuano", "Cebuano", "ceb"], ["Chichewa", "Chichewa", "ny"], ["Chinese Simplified", "简体中文", "zh-CN"], ["Chinese Traditional", "中國傳統的", "zh-TW"], ["Corsican", "Corsu", "co"], ["Croatian", "hrvatski", "hr"], ["Czech", "čeština", "cs"], ["Danish", "dansk", "da"], ["Dutch", "Nederlands", "nl"], ["Esperanto", "esperanto", "eo"], ["Estonian", "eesti", "et"], ["Filipino", "Pilipino", "tl"], ["Finnish", "suomalainen", "fi"], ["French", "français", "fr"], ["Galician", "galego", "gl"], ["Georgian", "ქართული", "ka"], ["German", "Deutsche", "de"], ["Greek", "ελληνικά", "el"], ["Gujarati", "ગુજરાતી", "gu"], ["Haitian Creole", "kreyòl ayisyen", "ht"], ["Hausa", "Hausa", "ha"], ["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"], ["Hebrew", "עִברִית", "iw"], ["Hindi", "हिंदी", "hi"], ["Hmong", "Hmong", "hmn"], ["Hungarian", "Magyar", "hu"], ["Icelandic", "Íslenska", "is"], ["Igbo", "Igbo", "ig"], ["Indonesian", "bahasa Indonesia", "id"], ["Irish", "Gaeilge", "ga"], ["Italian", "italiano", "it"], ["Japanese", "日本語", "ja"], ["Javanese", "Jawa", "jw"], ["Kannada", "ಕನ್ನಡ", "kn"], ["Kazakh", "Қазақ", "kk"], ["Khmer", "ភាសាខ្មែរ", "km"], ["Korean", "한국어", "ko"], ["Kurdish", "Kurdî", "ku"], ["Kyrgyz", "Кыргызча", "ky"], ["Lao", "ລາວ", "lo"], ["Latin", "Latinae", "la"], ["Latvian", "Latvijas", "lv"], ["Lithuanian", "Lietuvos", "lt"], ["Luxembourgish", "lëtzebuergesch", "lb"], ["Macedonian", "Македонски", "mk"], ["Malagasy", "Malagasy", "mg"], ["Malay", "Malay", "ms"], ["Malayalam", "മലയാളം", "ml"], ["Maltese", "Malti", "mt"], ["Maori", "Maori", "mi"], ["Marathi", "मराठी", "mr"], ["Mongolian", "Монгол", "mn"], ["Myanmar", "မြန်မာ", "my"], ["Nepali", "नेपाली", "ne"], ["Norwegian", "norsk", "no"], ["Nyanja", "madambwe", "ny"], ["Pashto", "پښتو", "ps"], ["Persian", "فارسی", "fa"], ["Polish", "Polskie", "pl"], ["Portuguese", "português", "pt"], ["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"], ["Romanian", "Română", "ro"], ["Russian", "русский", "ru"], ["Samoan", "Samoa", "sm"], ["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"], ["Serbian", "Српски", "sr"], ["Sesotho", "Sesotho", "st"], ["Shona", "Shona", "sn"], ["Sindhi", "سنڌي", "sd"], ["Sinhala", "සිංහල", "si"], ["Slovak", "slovenský", "sk"], ["Slovenian", "slovenski", "sl"], ["Somali", "Soomaali", "so"], ["Spanish", "Español", "es"], ["Sundanese", "Sunda", "su"], ["Swahili", "Kiswahili", "sw"], ["Swedish", "svenska", "sv"], ["Tajik", "Тоҷикистон", "tg"], ["Tamil", "தமிழ்", "ta"], ["Telugu", "తెలుగు", "te"], ["Thai", "ไทย", "th"], ["Turkish", "Türk", "tr"], ["Ukrainian", "український", "uk"], ["Urdu", "اردو", "ur"], ["Uzbek", "O'zbekiston", "uz"], ["Vietnamese", "Tiếng Việt", "vi"], ["Welsh", "Cymraeg", "cy"], ["Western Frisian", "Western Frysk", "fy"], ["Xhosa", "isiXhosa", "xh"], ["Yiddish", "ייִדיש", "yi"], ["Yoruba", "yorùbá", "yo"], ["Zulu", "Zulu", "zu"]],
        "advancedOptions": {
          "addMethod": "append",
          // PREPEND OR APPEND THE TRANSLATE ELEMENT
          "dropdownHandleText": "Translate",
          // ONLY FOR FRAMESET AND API VERSIONS AND NOT USING A CUSTOM ELEMENT
          "customElement": {
            // ONLY FOR FRAMESET AND API VERSIONS
            "useCustomElement": false,
            "translateItemsList": true,
            // true = THE TRANSLATE ITEMS WILL BE AN UNORDERED LIST, false = THE TRANSLATE ITEMS WILL JUST BE A COLLECTION OF <a> TAGS
            "customElementMarkup": "" // CUSTOM HTML MARKUP THAT MAKES THE CUSTOM TRANSLATE ELEMENT/STRUCTURE - USE [$CreativeTranslateListItems$] ACTIVE BLOCK IN THE MARKUP WHERE THE TRANSLATE ITEMS SHOULD BE ADDED

          },
          "apiKey": "",
          // ONLY FOR API VERSION
          "brandedLayout": 1,
          // 1 = VERTICAL (TEXT UNDER SELECT), 2 = HORIZONTAL (TEXT BESIDE SELECT), 3 = NON-RESPONSIVE MEGA DROPDOWN
          "removeBrandedDefaultStyling": false // 1 = VERTICAL (TEXT UNDER SELECT), 2 = HORIZONTAL (TEXT BESIDE SELECT), 3 = NON-RESPONSIVE MEGA DROPDOWN

        },
        "translateLoaded": function translateLoaded() {}
      }); // BIND DROPDOWN EVENTS

      this.DropdownActions({
        "dropdownParent": ".cs-mystart-dropdown.translate",
        "dropdownSelector": ".cs-dropdown-selector",
        "dropdown": ".cs-dropdown",
        "dropdownList": ".cs-dropdown-list"
      });
    }
  },
  "DropdownActions": function DropdownActions(params) {
    // FOR SCOPE
    var template = this;
    var dropdownParent = params.dropdownParent;
    var dropdownSelector = params.dropdownSelector;
    var dropdown = params.dropdown;
    var dropdownList = params.dropdownList;
    $(dropdownParent + " " + dropdownList + " a").attr("tabindex", "-1"); // MYSTART DROPDOWN SELECTOR CLICK EVENT

    $(dropdownParent).on("click", dropdownSelector, function (e) {
      e.preventDefault();

      if ($(this).parent().hasClass("open")) {
        $("+ " + dropdownList + " a").attr("tabindex", "-1");
        $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
      } else {
        $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing");
      }
    }); // MYSTART DROPDOWN SELECTOR KEYDOWN EVENTS

    $(dropdownParent).on("keydown", dropdownSelector, function (e) {
      // CAPTURE KEY CODE
      switch (e.keyCode) {
        // CONSUME LEFT AND UP ARROWS
        case template.KeyCodes.enter:
        case template.KeyCodes.space:
          e.preventDefault(); // IF THE DROPDOWN IS OPEN, CLOSE IT

          if ($(dropdownParent).hasClass("open")) {
            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
          } else {
            $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing", function () {
              $(dropdownList + " li:first-child a", this).attr("tabindex", "0").focus();
            });
          }

          break;
        // CONSUME TAB KEY

        case template.KeyCodes.tab:
          if ($("+ " + dropdown + " " + dropdownList + " a").length) {
            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
          }

          break;
        // CONSUME LEFT AND UP ARROWS

        case template.KeyCodes.down:
        case template.KeyCodes.right:
          e.preventDefault();
          $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
          $("+ " + dropdown + " " + dropdownList + " li:first-child > a", this).attr("tabindex", "0").focus();
          break;
      }
    }); // MYSTART DROPDOWN LINK KEYDOWN EVENTS

    $(dropdownParent).on("keydown", dropdownList + " li a", function (e) {
      // CAPTURE KEY CODE
      switch (e.keyCode) {
        // CONSUME LEFT AND UP ARROWS
        case template.KeyCodes.left:
        case template.KeyCodes.up:
          e.preventDefault(); // IS FIRST ITEM

          if ($(this).parent().is(":first-child")) {
            // FOCUS DROPDOWN BUTTON
            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
            $(this).closest(dropdownParent).find(dropdownSelector).focus();
          } else {
            // FOCUS PREVIOUS ITEM
            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
            $(this).parent().prev("li").find("> a").attr("tabindex", "0").focus();
          }

          break;
        // CONSUME RIGHT AND DOWN ARROWS

        case template.KeyCodes.right:
        case template.KeyCodes.down:
          e.preventDefault(); // IS LAST ITEM

          if ($(this).parent().is(":last-child")) {
            // FOCUS FIRST ITEM
            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
            $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
          } else {
            // FOCUS NEXT ITEM
            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
            $(this).parent().next("li").find("> a").attr("tabindex", "0").focus();
          }

          break;
        // CONSUME TAB KEY

        case template.KeyCodes.tab:
          if (e.shiftKey) {
            e.preventDefault(); // FOCUS DROPDOWN BUTTON

            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
            $(this).closest(dropdownParent).find(dropdownSelector).focus();
          }

          break;
        // CONSUME HOME KEY

        case template.KeyCodes.home:
          e.preventDefault(); // FOCUS FIRST ITEM

          $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
          $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
          break;
        // CONSUME END KEY

        case template.KeyCodes.end:
          e.preventDefault(); // FOCUS LAST ITEM

          $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
          $(this).closest(dropdownList).find("li:last-child > a").attr("tabindex", "0").focus();
          break;
        // CONSUME ESC KEY

        case template.KeyCodes.esc:
          e.preventDefault(); // FOCUS DROPDOWN BUTTON AND CLOSE DROPDOWN

          $(this).closest(dropdownParent).find(dropdownSelector).focus();
          $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
          $(dropdownSelector).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
          break;
      }
    });
    $(dropdownParent).mouseleave(function () {
      $(dropdownList + " a", this).attr("tabindex", "-1");
      $(dropdownSelector, this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
    }).focusout(function () {
      var thisDropdown = this;
      setTimeout(function () {
        if (!$(thisDropdown).find(":focus").length) {
          $(dropdownSelector, thisDropdown).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
        }
      }, 500);
    });
  },
  "Header": function Header() {
    // ADD LOGO
    var logoSrc = jQuery.trim('https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images//Faces/default-man.jpg');
    var srcSplit = logoSrc.split("/");
    var srcSplitLen = srcSplit.length;

    if (logoSrc != "" && srcSplit[srcSplitLen - 1] != "default-man.jpg") {
      $("#gb-logo").append("<a href='https://adamcruse.schoolwires.net/eisd'><img src='" + logoSrc + "' alt='Cartwright School District Logo' /></a>").removeClass("hidden");
    } else {
      $("#gb-logo").append("<a href='https://adamcruse.schoolwires.net/eisd'><img width='100' height='94' src='https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/157/defaults/default-logo.png' alt='Cartwright School District Logo' /></a>").removeClass("hidden");
    } //REMOVE SITENAME & TAGLINE IF EMPTY OR NOT IN USE


    if ($(".sitename-one").text().trim() == "" || $(".sitename-one").data("showSchoolName") === false) {
      $(".sitename-one").remove();
    }

    if ($(".sitename-tagline").text().trim() == "" || $(".sitename-tagline").data("showTagline") === false) {
      $(".sitename-tagline").remove();
    }
  },
  "ChannelBar": function ChannelBar() {
    $("#navc-HP span").text("").css("display", "block");
    $(".sw-channel-item").off('hover');
    $(".sw-channel-item").hover(function () {
      $(".sw-channel-item ul").stop(true, true);
      var subList = $(this).children('ul');

      if ($.trim(subList.html()) !== "") {
        subList.slideDown(300, "swing");
      }

      $(this).addClass("hover");
    }, function () {
      $(".sw-channel-dropdown").slideUp(300, "swing");
      $(this).removeClass("hover");
    });
  },
  "StickyChannelBar": function StickyChannelBar() {
    var headerHeight = $("#gb-header-bottom").outerHeight();
    var navHeight = $("#gb-header-top").outerHeight();
    $(document).scroll(function () {
      distanceFromTop = $(this).scrollTop();

      if (distanceFromTop >= headerHeight) {
        $("#gb-page").addClass("sticky-header");
      } else {
        $("#gb-page").removeClass("sticky-header");
      }

      if ($(".sp").length && $(".ui-widget.app.calendar").length) {
        if ($(".wcm-controls").hasClass("wcm-stuck")) {
          $(".wcm-controls").css("margin-top", navHeight);
        }
      }

      $("#sw-maincontent").css({
        "display": "block",
        "position": "relative",
        "top": "-" + navHeight + "px"
      });
    });
  },
  "Body": function Body() {
    // FOR SCOPE
    var _this = this; // AUTO FOCUS SIGN IN FIELD


    $("#swsignin-txt-username").focus(); // APPLY RESPONSIVE DIMENSIONS TO CONTENT IMAGES

    $(".ui-widget.app .ui-widget-detail img").not($(".ui-widget.app.multimedia-gallery .ui-widget-detail img")).each(function () {
      if ($(this).attr('width') !== undefined && $(this).attr('height') !== undefined) {
        // IMAGE HAS INLINE DIMENSIONS
        $(this).css({
          "display": "inline-block",
          "width": "100%",
          "max-width": $(this).attr("width") + "px",
          "height": "auto",
          "max-height": $(this).attr("height") + "px"
        });
      }
    }); // ADJUST FIRST BREADCRUMB

    $("li.ui-breadcrumb-first > a > span").text("Home").show(); // CHECK PAGELIST HEADER

    if ($.trim($(".ui-widget.app.pagenavigation .ui-widget-header").text()) == "") {
      $(".ui-widget.app.pagenavigation .ui-widget-header").html("<h1>About Us</h1>");
    } // NAVIGATION APP HOVER / FOCUS


    $(".navigation li a").hover(function () {
      $(this).closest("li").addClass("hover");
    }, function () {
      $(this).closest("li").removeClass("hover");
    });
    $(".navigation li a").focus(function () {
      $(this).closest("li").addClass("hover");
    });
    $(".navigation li a").focusout(function () {
      $(this).closest("li").removeClass("hover");
    });
  },
  "Homepage": function Homepage() {
    // FOR SCOPE
    var _this = this;

    if ($("#gb-page.hp").length) {
      //MOVE HEADLINES MORE LINK
      $(".hp-row.three .headlines, .hp-row.four .headlines").each(function () {
        if ($(this).find(".more-link").length > 0) {
          $(this).find(".more-link").appendTo($(this).find(".ui-widget-header").attr("data-more-link", "true"));
        }
      }); //MOVE VIEW CALENDAR LINK

      $(".hp-row.three .upcomingevents, .hp-row.four .upcomingevents").each(function () {
        if ($(this).find(".view-calendar-link").length > 0) {
          $(this).find(".view-calendar-link").appendTo($(this).find(".ui-widget-header").attr("data-more-link", "true"));
        }
      }); //UPCOMING EVENTS BACKGROUND IMAGE

      var bgSrc = jQuery.trim('https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images//Faces/default-man.jpg');
      var srcSplit = bgSrc.split("/");
      var srcSplitLen = srcSplit.length;

      if (bgSrc != "" && srcSplit[srcSplitLen - 1] != "default-man.jpg") {
        $(".hp-row:not(.five) .upcomingevents").css("background-image", "url(" + bgSrc + ")");
      } else {
        $(".hp-row:not(.five) .upcomingevents").css("background-image", "url('https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/157/defaults/default-events-bg.jpg')");
      }
    }
  },
  "Shortcuts": function Shortcuts() {
    var _this = this; // EDIT THESE TWO VARS


    var columnNums = [3, 1, 1, 1, 1]; // [960, 768, 640, 480, 320]

    var selector = ".hp-row.three div.ui-widget.app.siteshortcuts, .hp-row.four div.ui-widget.app.siteshortcuts"; // RETURN BREAKPOINT INDEX

    var bp = function bp() {
      switch (_this.GetBreakPoint()) {
        case "desktop":
          return 0;
          break;

        case "768":
          return 1;
          break;

        case "640":
          return 2;
          break;

        case "480":
          return 3;
          break;

        case "320":
          return 4;
          break;
      }
    }; // SET COLUMN NUM AND OTHER VARS


    var columnNum = columnNums[bp()];
    var endRange;
    $(selector).each(function () {
      // RETURN THE LI'S TO THE ORIGINAL UL
      $(".ui-widget-detail > ul.site-shortcuts", this).append($(".site-shortcuts-column > li", this)); // REMOVE COLUMN CONTAINER FOR REBUILD

      $(".site-shortcuts-columns", this).remove(); // GET SHORTCUT NUM

      var shortcutNum = $(".ui-widget-detail > ul.site-shortcuts > li", this).length; // ADD COLUMN CONTAINER

      $(".ui-widget-detail", this).prepend('<div class="site-shortcuts-columns"></div>'); // LOOP TO BUILD COLUMNS

      for (var i = 0; i < columnNum; i++) {
        // KEEP FROM ADDING EMPTY UL'S TO THE DOM
        if (i < shortcutNum) {
          // IF shortcutNum / columnNum REMAINDER IS BETWEEN .0 AND .5 AND THIS IS THE FIRST LOOP ITERATION
          // WE'LL ADD 1 TO THE END RANGE SO THAT THE EXTRA LINK GOES INTO THE FIRST COLUMN
          if (shortcutNum / columnNum % 1 > 0.0 && shortcutNum / columnNum % 1 < 0.5 && i == 0) {
            endRange = Math.round(shortcutNum / columnNum) + 1;
          } else if (shortcutNum / columnNum % 1 == 0.5 && i >= columnNum / i) {
            endRange = Math.round(shortcutNum / columnNum) - 1;
          } else {
            endRange = Math.round(shortcutNum / columnNum);
          } // ADD THE COLUMN UL


          $(".site-shortcuts-columns", this).append('<ul class="site-shortcuts-column column-' + (i + 1) + '"></ul>'); // MOVE THE RANGE OF LI'S TO THE COLUMN UL

          $(".site-shortcuts-column.column-" + (i + 1), this).append($(".ui-widget-detail > ul.site-shortcuts > li:nth-child(n+1):nth-child(-n+" + endRange + ")", this));
        }
      } // HIDE THE ORIGINAL UL


      $(".ui-widget-detail > ul.site-shortcuts", this).hide();
    });
  },
  "Headlines": function Headlines() {
    var _this = this;

    $(".hp-row.three .headlines, .hp-row.four .headlines").each(function () {
      var headline = this; //ADD COLUMN STRUCTURE TO THE APP DETAIL

      var articleCount = 1;
      var totalCount = $(this).find("li").length;

      switch (_this.GetBreakPoint()) {
        case "desktop":
          if ($(this).find(".cs-article-wrapper").length == 0) {
            $(this).find("li").each(function () {
              $(this).addClass("cs-article-wrapper").attr("data-orig-position", $(this).index() + 1);
            });
            $("<li class='headlines-column'><ul></ul></li><li class='headlines-column'><ul></ul></li><li class='headlines-column'><ul></ul></li>").prependTo($(this).find(".ui-articles"));
          }

          for (i = 1; i <= totalCount; i++) {
            if (articleCount > 3) {
              articleCount = 1;
            }

            $(".cs-article-wrapper[data-orig-position='" + i + "']").appendTo(".headlines-column:nth-child(" + articleCount + ") ul");
            articleCount++;
          }

          break;

        case "768":
        case "640":
          if ($(this).find(".cs-article-wrapper").length == 0) {
            $(this).find("li").each(function () {
              $(this).addClass("cs-article-wrapper").attr("data-orig-position", $(this).index() + 1);
            });
            $("<li class='headlines-column'><ul></ul></li><li class='headlines-column'><ul></ul></li><li class='headlines-column'><ul></ul></li>").prependTo($(this).find(".ui-articles"));
          }

          for (i = 1; i <= totalCount; i++) {
            if (articleCount > 2) {
              articleCount = 1;
            }

            $(".cs-article-wrapper[data-orig-position='" + i + "']").appendTo(".headlines-column:nth-child(" + articleCount + ") ul");
            articleCount++;
          }

          break;

        case "480":
        case "320":
          if ($(this).find(".cs-article-wrapper").length > 0) {
            for (i = 1; i <= totalCount; i++) {
              $(".cs-article-wrapper[data-orig-position='" + i + "']").appendTo(".headlines-column:first-child ul");
            }
          } else {
            $(this).find("li").each(function () {
              $(this).addClass("cs-article-wrapper").attr("data-orig-position", $(this).index() + 1);
            });
            $("<li class='headlines-column'><ul></ul></li><li class='headlines-column'><ul></ul></li><li class='headlines-column'><ul></ul></li>").prependTo($(this).find(".ui-articles"));

            for (i = 1; i <= totalCount; i++) {
              if (articleCount > 3) {
                articleCount = 1;
              }

              $(".cs-article-wrapper[data-orig-position='" + i + "']").appendTo(".headlines-column:nth-child(" + articleCount + ") ul");
              articleCount++;
            }
          }

          break;
      }
    }); //HOVER / FOCUS

    $(".cs-article-wrapper").hover(function () {
      $(this).addClass("hover");
    }, function () {
      $(this).removeClass("hover");
    });
    $(".cs-article-wrapper .ui-article-title a").focus(function () {
      $(this).closest("li").addClass("hover");
    });
    $(".cs-article-wrapper .ui-article-title a").focusout(function () {
      $(this).closest("li").removeClass("hover");
    });
  },
  "Footer": function Footer() {
    // FOR SCOPE
    var _this = this; // MOVE Bb FOOTER STUFF


    $(".gb-bb-footer.logo").html($("#sw-footer-logo").html());
    var schoolwiresLinks = '';
    schoolwiresLinks += '<li>' + $.trim($("#sw-footer-links li:eq(0)").html().replace("|", "")) + '</li>';
    schoolwiresLinks += '<li>' + $.trim($("#sw-footer-links li:eq(2)").html().replace("|", "")) + '</li>';
    schoolwiresLinks += '<li>' + $.trim($("#sw-footer-links li:eq(1)").html().replace("|", "")) + '</li>';
    $(".gb-bb-footer.links").append('<ul>' + schoolwiresLinks + '</ul>');
    $(".gb-bb-footer.copyright").append($("#sw-footer-copyright").html()); //SET MAP CONTAINER SIZE

    var footerDifference = $(window).width() - $(".footer-row.one .content-wrap").width();
    var mapWidth = $(".footer-row.one .footer-column.two").width() + footerDifference / 2;
    $("#gb-google-map-link").css("width", mapWidth); //BACK TO TOP

    $("#gb-to-top").on("click keydown", function (e) {
      if (_this.AllyClick(e)) {
        e.preventDefault();
        $("html, body").animate({
          scrollTop: 0
        }, "slow", function () {
          if (_this.GetBreakPoint() != "desktop") {
            $("#rs-menu-btn").focus();
          } else {
            $(".cs-mystart-button.home a").focus();
          }
        });
      }
    });
  },
  "Slideshow": function Slideshow() {
    // FOR SCOPE
    var _this = this;

    if('Multimedia Gallery' == "Multimedia Gallery;Streaming Video" || 'Multimedia Gallery' == "Multimedia Gallery") {
                if($("#sw-content-container10 .ui-widget.app.multimedia-gallery").length) {
                    this.MMGPlugin();
                }
            }
  },
  "MMGPlugin": function() {
            // FOR SCOPE
            var _this = this;

            var mmg = eval("multimediaGallery" + $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").attr("data-pmi"));
            mmg.props.defaultGallery = false;

            $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").csMultimediaGallery({
                "efficientLoad" : true,
                "imageWidth" : 1500,
                "imageHeight" : 700,
                "mobileDescriptionContainer": [960, 768, 640, 480, 320], // [960, 768, 640, 480, 320]
                "galleryOverlay" : false,
                "linkedElement" : [],  // ["image", "title", "overlay"]
                "playPauseControl" : true,
                "backNextControls" : true,
                "bullets" : false,
                "thumbnails" : false,
                "thumbnailViewerNum": [3, 4, 3, 3, 2], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
                "autoRotate" : true,
                "hoverPause" : true,
                "transitionType" : "fade",
                "transitionSpeed" : 2,
                "transitionDelay" : 6,
                "fullScreenRotator" : false,
                "fullScreenBreakpoints" : [960], // NUMERICAL - [960, 768, 640, 480, 320]
                "onImageLoad" : function(props) {}, // props.element, props.recordIndex, props.mmgRecords
                "allImagesLoaded" : function(props) {}, // props.element, props.mmgRecords
                "onTransitionStart" : function(props) {

                },
                "onTransitionEnd" : function(props) {}, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.mmgRecords
                "allLoaded" : function(props) {
                    //WRAP CONTROLS
                    $(".mmg-control.play-pause").insertAfter(".mmg-control.back");
                    $(".mmg-control").wrapAll("<div class='mmg-control-wrapper'></div>");

                }, // props.element, props.mmgRecords
                "onWindowResize": function(props) {

                } // props.element, props.mmgRecords
            });
        },
    
  "CheckSlideshow": function CheckSlideshow() {
    // FOR SCOPE
    var _this = this;

    if ($(".hp").length && this.GetBreakPoint() != "desktop" &&'Multimedia Gallery' != "Streaming Video") {
      if ($(window).scrollTop() <= $("#hp-slideshow").offset().top + $("#hp-slideshow").height()) {
        if (this.SlideshowDescFixed) {
          $("#hp-slideshow").removeAttr("style");
          this.SlideshowDescFixed = false;
        }
      } else {
        if (!this.SlideshowDescFixed) {
          $("#hp-slideshow").css({
            "height": $("#hp-slideshow").height(),
            "overflow": "hidden"
          });
          this.SlideshowDescFixed = true;
        }
      }
    }
  },
  "StreamingVideo": function() {
    // FOR SCOPE
    var _this = this;

    if('Multimedia Gallery' == "Streaming Video") {
        var videoVendor = ('Youtube' == "YouTube;Vimeo") ? "youtube" : 'Youtube';
        var mobilePhoto = 'https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images//Faces/default-man.jpg';

        if($.trim(mobilePhoto) == "" || mobilePhoto.indexOf("default-man.jpg") > -1) {
            mobilePhoto = "https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/template/157/defaults/default-events-bg.jpg";
        }

        $("#sw-content-container10.region.ui-hp").fullScreenRotator({
            "videoSource" : videoVendor, // OPTIONS ARE: youtube, vimeo
            "videoID": '', // YouTube and Vimeo ids are set as default in the script
            "fullScreenBreakpoints" : [960], // OPTIONS ARE [960, 768, 640, 480, 320]
            "showControls" : true,
            "showWatchVideoButton": true,
            "showProgressBar" : false, // CURRENTLY NOT IN USE
            "progressBarType" : "line", // CURRENTLY NOT IN USE BUT OPTIONS ARE: line, circle
            "progressBarCircleWidth": 70, // pixels
            "progressBarCircleHeight": 70, // pixels
            "progressBarStrokeWidth": 10, // pixels
            "progressBarStrokeColor": "#FFF",
            "showMobileBackgroundPhoto" : true,
            "mobileBackgroundPhoto" : mobilePhoto,
            "mobileBackgroundPhotoBreakpoint" : 640, // OPTIONS ARE 768, 640, 480, 320
            "onReady" : function() {

                //BUILD TITLE & CAPTION AREA
                var videoTextHTML = "";


                videoTextHTML += "<div id='hp-video-content'><div class='mmg-description-outer'><div class='mmg-description sv'><div class='content-wrap'>";

                if(true != false){ //
                    videoTextHTML += '<h2 class="mmg-description-title">Video Title</h2>';
                }

                if(true != false){
                    videoTextHTML += '<p class="mmg-description-caption">Video Caption</p>';
                }

                if(true != false){
                    videoTextHTML += '<div class="mmg-description-links"><a class="mmg-description-link read-more" href="#">Video Link Text</a></div>';
                }

                videoTextHTML += "</div></div></div></div>";

                $(videoTextHTML).insertAfter("#cs-fullscreen-video-outer");

                //MOVE CONTROLS
                $(".mute-button, .unmute-button").appendTo(".cs-fullscreen-video-buttons");
                $(".cs-fullscreen-video-buttons").prependTo(".mmg-description");

                //VIDEO TEXT POSITION
                if(_this.GetBreakPoint() == "desktop") {
            		$("#hp-video-content").css("height", "calc(100vh - "+$("#gb-header").outerHeight()+"px)");
          		}
                

                //VIDEO BUTTON INTERACTION
                $("#hp-slideshow .cs-fullscreen-video-button.mute-button").attr("aria-label", "Mute Background Video");
                $("#hp-slideshow .cs-fullscreen-video-button.unmute-button").attr("aria-label", "Unmute Background Video");
                $("#hp-slideshow .cs-fullscreen-video-button.play-button").attr("aria-label", "Play Background Video");
                $("#hp-slideshow .cs-fullscreen-video-button.pause-button").attr("aria-label", "Pause Background Video");

                $("#hp-slideshow").on("click keydown", ".cs-fullscreen-video-button.play-button", function(e) {
                    if(_this.AllyClick(e)) {
                        setTimeout(function () {
                            $("#hp-slideshow .cs-fullscreen-video-button.pause-button").focus();
                        }, 100);
                    }
                });
                $("#hp-slideshow").on("click keydown", ".cs-fullscreen-video-button.pause-button", function(e) {
                    if(_this.AllyClick(e)) {
                        setTimeout(function () {
                            $("#hp-slideshow .cs-fullscreen-video-button.play-button").focus();
                        }, 100);
                    }
                });
                $("#hp-slideshow").on("click keydown", ".cs-fullscreen-video-button.mute-button", function(e) {
                    if(_this.AllyClick(e)) {
                        setTimeout(function () {
                            $("#hp-slideshow .cs-fullscreen-video-button.unmute-button").focus();
                        }, 100);
                    }
                });
                $("#hp-slideshow").on("click keydown", ".cs-fullscreen-video-button.unmute-button", function(e) {
                    if(_this.AllyClick(e)) {
                        setTimeout(function () {
                            $("#hp-slideshow .cs-fullscreen-video-button.mute-button").focus();
                        }, 100);
                    }
                });
            },
            "onStateChange" : function() { }
        });
    }
  },
  "ModEvents": function ModEvents() {
    // FOR SCOPE
    var template = this;
    $(".ui-widget.app.upcomingevents").modEvents({
      columns: "yes",
      monthLong: "no",
      dayWeek: "yes"
    });
    eventsByDay(".upcomingevents .ui-articles");

    function eventsByDay(container) {
      $(".ui-article", container).each(function () {
        if (!$(this).find("h1.ui-article-title.sw-calendar-block-date").size()) {
          var moveArticle = $(this).html();
          $(this).parent().prev().children().children().next().append(moveArticle);
          $(this).parent().remove();
        }

      });
      
      $(".ui-article", container).each(function () {
        var newDateTime = $('.upcoming-column.left h1', this).html().toLowerCase().split("</span>");
        var newDate = '';
        var dateIndex = newDateTime.length > 2 ? 2 : 1; //if the dayWeek is set to yes we need to account for that in our array indexing
        //if we have the day of the week make sure to include it.
        
        //add a zero to the date if it is a single digit
		if( newDateTime[dateIndex].length == 1 ) newDateTime[dateIndex] = "0" + newDateTime[dateIndex];

        if (dateIndex > 1) {
          newDate += newDateTime[0] + "</span>";
        } //add in the month


        newDate += newDateTime[dateIndex - 1] + '</span>'; //the month is always the in the left array position to the date
        //wrap the date in a new tag

        newDate += '<span class="jeremy-date">' + newDateTime[dateIndex] + '</span>'; //append the date and month back into their columns

        $('.upcoming-column.left h1', this).html(newDate); //add an ALL DAY label if no time was given

        $('.upcoming-column.right .ui-article-description', this).each(function () {
          if ($('.sw-calendar-block-time', this).length < 1) {
            //if it doesnt exist add it
            $(this).prepend('<span class="sw-calendar-block-time">ALL DAY</span>');
          }

          $(".sw-calendar-block-time", this).appendTo($(this));
        }); //WRAP DATE AND MONTH IN A CONTAINER

        $(".jeremy-date, .joel-month", this).wrapAll("<span class='adam-hug'></span>");
        $(".adam-hug", this).insertBefore($(this).find(".joel-day"));
      });
    } //ADD NO EVENTS TEXT


    $(".upcomingevents").each(function () {
      if (!$(this).find(".ui-article").length) {
        $("<li><p>There are no upcoming events to display.</p></l1>").appendTo($(this).find(".ui-articles"));
      }
    }); //UPCOMING EVENTS SLIDER

    $(".hp-row.three .upcomingevents, .hp-row.four .upcomingevents").csArticleSlider({
      "viewerNum": [5, 3, 3, 1, 1],
      // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
      "transitionSpeed": .5,
      // SECONDS
      "slideFullView": false,
      "extend": function extend(config, element, ArticleSlider) {}
    });
    $(".hp-row.three .upcomingevents, .hp-row.four .upcomingevents").each(function () {
      $(this).find(".cs-article-slider-control").insertBefore($(this).find(".view-calendar-link"));
      $(this).find(".cs-article-slider-control, .view-calendar-link").wrapAll("<div class='cs-article-slider-control-wrapper'></div>");
      $(".cs-article-slider-control.back").html("<span>Previous</span>");
      $(".cs-article-slider-control.next").html("<span>Next</span>");
      $(this).find(".ui-widget-header").attr("data-slider-controls", "true");
      $(".cs-article-slider-control").show();
    }); //HOVER / FOCUS

    $(".upcomingevents .cs-article-viewer li").hover(function () {
      $(this).addClass("hover");
    }, function () {
      $(this).removeClass("hover");
    });
    $(".upcomingevents .cs-article-viewer li a").focus(function () {
      $(this).closest("li").addClass("hover");
    });
    $(".upcomingevents .cs-article-viewer li a").focusout(function () {
      $(this).closest("li").removeClass("hover");
    });
  },
  "GlobalIcons": function GlobalIcons() {
    //BACKGROUND IMAGE
    var bgSrc = jQuery.trim('https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images//Faces/default-man.jpg');
    var srcSplit = bgSrc.split("/");
    var srcSplitLen = srcSplit.length;

    if (bgSrc != "" && srcSplit[srcSplitLen - 1] != "default-man.jpg") {
      $("#gb-icons").css("background-image", "url(" + bgSrc + ")");
    } else {
      $("#gb-icons").css("background-image", "url('https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/157/defaults/default-icons-bg.jpg')");
    }

    $("#gb-icons .content-wrap").creativeIcons({
      "iconNum":'8',
      "defaultIconSrc": "",
      "icons": [{
        "image":'People/11.png',
        "showText": true,
        "text":'Enrollment',
        "url":'#',
        "target":'_self'
      }, {
        "image":'Classroom/44.png',
        "showText": true,
        "text":'Student Meals',
        "url":'#',
        "target":'_self'
      }, {
        "image":'Communication/54.png',
        "showText": true,
        "text":'Calendar',
        "url":'#',
        "target":'_self'
      }, {
        "image":'Site Utility/29.png',
        "showText": true,
        "text":'Transportation',
        "url":'#',
        "target":'_self'
      }, {
        "image":'Classroom/29.png',
        "showText": true,
        "text":'Family Engagements',
        "url":'#',
        "target":'_self'
      }, {
        "image":'Classroom/47.png',
        "showText": true,
        "text":'Social - Emotional Learning',
        "url":'#',
        "target":'_self'
      }, {
        "image":'Site Utility/15.png',
        "showText": true,
        "text":'Governing Board',
        "url":'#',
        "target":'_self'
      }, {
        "image":'Classroom/92.png',
        "showText": true,
        "text":'Staffing',
        "url":'#',
        "target":'_self'
      }],
      "siteID": "4",
      "siteAlias": "eisd",
      "calendarLink": "/Page/2",
      "contactEmail": "webmaster@email.com",
      "allLoaded": function allLoaded() {}
    });
  },
  "SocialIcons": function SocialIcons() {
    var socialIcons = [{
      "show":true,
      "label": "Facebook",
      "class": "facebook",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "Twitter",
      "class": "twitter",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "YouTube",
      "class": "youtube",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "Instagram",
      "class": "instagram",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "LinkedIn",
      "class": "linkedin",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "Vimeo",
      "class": "vimeo",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "Flickr",
      "class": "flickr",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "Pinterest",
      "class": "pinterest",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "RSS",
      "class": "rss",
      "url":'#',
      "target":'_blank'
    }];
    var icons = '';
    $.each(socialIcons, function (index, icon) {
      if (icon.show) {
        icons += '<a class="gb-social-media-icon ' + icon.class + ' district" href="' + icon.url + '" target="' + icon.target + '" aria-label="' + icon.label + '"></a>';
      }
    });

    if (icons.length) {
      $("#gb-social").prepend(icons);
    } else {
      $("#gb-social").hide();
    }
  },
  "RsMenu": function RsMenu() {
    // FOR SCOPE
    var _this = this;

    $.csRsMenu({
      "breakPoint": 768,
      // SYSTEM BREAK POINTS - 768, 640, 480, 320
      "slideDirection": "left-to-right",
      // OPTIONS - left-to-right, right-to-left
      "menuButtonParent": ".header-row.mobile",
      "menuBtnText": "MENU",
      "colors": {},
      "showDistrictHome": _this.ShowDistrictHome,
      "districtHomeText": "District Home",
      "showSchools": false,
      "schoolMenuText": "Schools",
      "showTranslate": true,
      "translateMenuText": "Translate",
      "translateVersion": 2,
      // 1 = FRAMESET, 2 = BRANDED
      "translateId": "",
      "translateLanguages": [// ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
      ["Afrikaans", "Afrikaans", "af"], ["Albanian", "shqiptar", "sq"], ["Amharic", "አማርኛ", "am"], ["Arabic", "العربية", "ar"], ["Armenian", "հայերեն", "hy"], ["Azerbaijani", "Azərbaycan", "az"], ["Basque", "Euskal", "eu"], ["Belarusian", "Беларуская", "be"], ["Bengali", "বাঙালি", "bn"], ["Bosnian", "bosanski", "bs"], ["Bulgarian", "български", "bg"], ["Burmese", "မြန်မာ", "my"], ["Catalan", "català", "ca"], ["Cebuano", "Cebuano", "ceb"], ["Chichewa", "Chichewa", "ny"], ["Chinese Simplified", "简体中文", "zh-CN"], ["Chinese Traditional", "中國傳統的", "zh-TW"], ["Corsican", "Corsu", "co"], ["Croatian", "hrvatski", "hr"], ["Czech", "čeština", "cs"], ["Danish", "dansk", "da"], ["Dutch", "Nederlands", "nl"], ["Esperanto", "esperanto", "eo"], ["Estonian", "eesti", "et"], ["Filipino", "Pilipino", "tl"], ["Finnish", "suomalainen", "fi"], ["French", "français", "fr"], ["Galician", "galego", "gl"], ["Georgian", "ქართული", "ka"], ["German", "Deutsche", "de"], ["Greek", "ελληνικά", "el"], ["Gujarati", "ગુજરાતી", "gu"], ["Haitian Creole", "kreyòl ayisyen", "ht"], ["Hausa", "Hausa", "ha"], ["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"], ["Hebrew", "עִברִית", "iw"], ["Hindi", "हिंदी", "hi"], ["Hmong", "Hmong", "hmn"], ["Hungarian", "Magyar", "hu"], ["Icelandic", "Íslenska", "is"], ["Igbo", "Igbo", "ig"], ["Indonesian", "bahasa Indonesia", "id"], ["Irish", "Gaeilge", "ga"], ["Italian", "italiano", "it"], ["Japanese", "日本語", "ja"], ["Javanese", "Jawa", "jw"], ["Kannada", "ಕನ್ನಡ", "kn"], ["Kazakh", "Қазақ", "kk"], ["Khmer", "ភាសាខ្មែរ", "km"], ["Korean", "한국어", "ko"], ["Kurdish", "Kurdî", "ku"], ["Kyrgyz", "Кыргызча", "ky"], ["Lao", "ລາວ", "lo"], ["Latin", "Latinae", "la"], ["Latvian", "Latvijas", "lv"], ["Lithuanian", "Lietuvos", "lt"], ["Luxembourgish", "lëtzebuergesch", "lb"], ["Macedonian", "Македонски", "mk"], ["Malagasy", "Malagasy", "mg"], ["Malay", "Malay", "ms"], ["Malayalam", "മലയാളം", "ml"], ["Maltese", "Malti", "mt"], ["Maori", "Maori", "mi"], ["Marathi", "मराठी", "mr"], ["Mongolian", "Монгол", "mn"], ["Myanmar", "မြန်မာ", "my"], ["Nepali", "नेपाली", "ne"], ["Norwegian", "norsk", "no"], ["Nyanja", "madambwe", "ny"], ["Pashto", "پښتو", "ps"], ["Persian", "فارسی", "fa"], ["Polish", "Polskie", "pl"], ["Portuguese", "português", "pt"], ["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"], ["Romanian", "Română", "ro"], ["Russian", "русский", "ru"], ["Samoan", "Samoa", "sm"], ["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"], ["Serbian", "Српски", "sr"], ["Sesotho", "Sesotho", "st"], ["Shona", "Shona", "sn"], ["Sindhi", "سنڌي", "sd"], ["Sinhala", "සිංහල", "si"], ["Slovak", "slovenský", "sk"], ["Slovenian", "slovenski", "sl"], ["Somali", "Soomaali", "so"], ["Spanish", "Español", "es"], ["Sundanese", "Sunda", "su"], ["Swahili", "Kiswahili", "sw"], ["Swedish", "svenska", "sv"], ["Tajik", "Тоҷикистон", "tg"], ["Tamil", "தமிழ்", "ta"], ["Telugu", "తెలుగు", "te"], ["Thai", "ไทย", "th"], ["Turkish", "Türk", "tr"], ["Ukrainian", "український", "uk"], ["Urdu", "اردو", "ur"], ["Uzbek", "O'zbekiston", "uz"], ["Vietnamese", "Tiếng Việt", "vi"], ["Welsh", "Cymraeg", "cy"], ["Western Frisian", "Western Frysk", "fy"], ["Xhosa", "isiXhosa", "xh"], ["Yiddish", "ייִדיש", "yi"], ["Yoruba", "yorùbá", "yo"], ["Zulu", "Zulu", "zu"]],
      "showAccount": true,
      "accountMenuText": "User Options",
      "usePageListNavigation": false,
      "extraMenuOptions": CreativeTemplate.rsSchoolMenuItems,
      "siteID": "4",
      "allLoaded": function allLoaded() {}
    });
  },
  "AppAccordion": function AppAccordion() {
    $(".sp-column.one").csAppAccordion({
      "accordionBreakpoints": [768, 640, 480, 320]
    });
  },
  "Search": function Search() {
    // FOR SCOPE
    var _this = this; //SEARCH CONTROLS


    $(".cs-mystart-button.search .cs-button-selector, #search-control-close").on('click keydown', function (event) {
      if (_this.AllyClick(event) === true) {
        //DONT LET THE PAGE JUMP ON KEYPRESS
        event.preventDefault();

        if (!$(this).hasClass("open") && !$(this).parent().hasClass("open")) {
          openSearch();
        } else {
          closeSearch();
        }
      }
    });

    function openSearch() {
      $(".cs-mystart-button.search .cs-button-selector, #search-control-close").attr("aria-expanded", "true");
      $(".cs-mystart-button.search, #search-control-close").addClass("open");
      $("#search-control-close").attr("aria-expanded", "true");
      $("#gb-search").addClass("open").attr("aria-hidden", "false");
      $("#gb-search-input, #gb-search-button, #search-control-close").attr("tabindex", "0");
      $("#gb-search-input").focus();
    }

    function closeSearch() {
      $(".cs-mystart-button.search .cs-button-selector, #search-control-close").attr("aria-expanded", "false");
      $(".cs-mystart-button.search, #search-control-close").removeClass("open");
      $("#gb-search").removeClass("open").attr("aria-hidden", "true");
      $("#gb-search-input, #gb-search-button, #search-control-close").attr("tabindex", "-1");
      $(".cs-mystart-button.search .cs-button-selector").focus();
    } //SEARCH FORM SUBMISSION
	
    $(".cs-mystart-button.search").mouseleave(function () {
      closeSearch();
    });

    $("#gb-search-form").submit(function (e) {
      e.preventDefault();

      if ($.trim($("#gb-search-input").val()) != "Search..." && $.trim($("#gb-search-input").val()) != "") {
        window.location.href = "/site/Default.aspx?PageType=6&SiteID=4&SearchString=" + $("#gb-search-input").val();
      }
    }); //SEARCH INPUT TEXT

    $("#gb-search-input").focus(function () {
      if ($(this).val() == "Search...") {
        $(this).val("");
      }
    });
    $("#gb-search-input").blur(function () {
      if ($(this).val() == "") {
        $(this).val("Search...");
      }
    }); //SEARCH KEYBOARD NAV

    $("#gb-search-input, #gb-search-button, #search-control-close").on("keydown", function (e) {
      // CAPTURE KEY CODE
      switch (e.keyCode) {
        case _this.KeyCodes.esc:
          if (!e.shiftKey) {
            e.preventDefault();
            closeSearch();
          }

          break;
      }
    });
  },
  "AllyClick": function AllyClick(event) {
    if (event.type == "click") {
      return true;
    } else if (event.type == "keydown") {
      if (event.keyCode == this.KeyCodes.space || event.keyCode == this.KeyCodes.enter) {
        return true;
      }
    } else {
      return false;
    }
  },
  "GetBreakPoint": function GetBreakPoint() {
    return window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"|'/g, "");
    /*"*/
  }
};
/******/ })()
;</script>

    <!-- App Preview -->
    


    <style type="text/css">
        /* HOMEPAGE EDIT THUMBNAIL STYLES */

        div.region {
            ;
        }

            div.region span.homepage-thumb-region-number {
                font: bold 100px verdana;
                color: #fff;
            }

        div.homepage-thumb-region {
            background: #264867; /*dark blue*/
            border: 5px solid #fff;
            text-align: center;
            padding: 40px 0 40px 0;
            display: block;
        }

            div.homepage-thumb-region.region-1 {
                background: #264867; /*dark blue*/
            }

            div.homepage-thumb-region.region-2 {
                background: #5C1700; /*dark red*/
            }

            div.homepage-thumb-region.region-3 {
                background: #335905; /*dark green*/
            }

            div.homepage-thumb-region.region-4 {
                background: #B45014; /*dark orange*/
            }

            div.homepage-thumb-region.region-5 {
                background: #51445F; /*dark purple*/
            }

            div.homepage-thumb-region.region-6 {
                background: #3B70A0; /*lighter blue*/
            }

            div.homepage-thumb-region.region-7 {
                background: #862200; /*lighter red*/
            }

            div.homepage-thumb-region.region-8 {
                background: #417206; /*lighter green*/
            }

            div.homepage-thumb-region.region-9 {
                background: #D36929; /*lighter orange*/
            }

            div.homepage-thumb-region.region-10 {
                background: #6E5C80; /*lighter purple*/
            }

        /* END HOMEPAGE EDIT THUMBNAIL STYLES */
    </style>

    <style type="text/css" media="print">
        .noprint {
            display: none !important;
        }
    </style>

    <style type ="text/css">
        .ui-txt-validate {
            display: none;
        }
    </style>

    

<!-- Begin Schoolwires Traffic Code --> 

<script type="text/javascript">

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-5173826-6', 'auto', 'BBTracker' );
    ga('BBTracker.set', 'dimension1', 'AWS');
    ga('BBTracker.set', 'dimension2', 'False');
    ga('BBTracker.set', 'dimension3', 'SWCS000009');
    ga('BBTracker.set', 'dimension4', '12');
    ga('BBTracker.set', 'dimension5', '3');
    ga('BBTracker.set', 'dimension6', '17');

    ga('BBTracker.send', 'pageview');

</script>

<!-- End Schoolwires Traffic Code --> 

    <!-- Ally Alternative Formats Loader    START   -->
    
    <!-- Ally Alternative Formats Loader    END     -->

</head>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info = {"beacon":"bam-cell.nr-data.net","errorBeacon":"bam-cell.nr-data.net","licenseKey":"e84461d315","applicationID":"14859287","transactionName":"Z1MEZEtSVkoFBxIKX14ZJ2NpHEtQEAFJB1VWVxNcTR1ZShQc","queueTime":0,"applicationTime":306,"agent":"","atts":""}</script><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"e84461d315",applicationID:"14859287"};window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var i=e[n]={exports:{}};t[n][0].call(i.exports,function(e){var i=t[n][1][e];return r(i||e)},i,i.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<n.length;i++)r(n[i]);return r}({1:[function(t,e,n){function r(){}function i(t,e,n){return function(){return o(t,[u.now()].concat(f(arguments)),e?null:this,n),e?void 0:this}}var o=t("handle"),a=t(8),f=t(9),c=t("ee").get("tracer"),u=t("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],p="api-",l=p+"ixn-";a(d,function(t,e){s[e]=i(p+e,!0,"api")}),s.addPageAction=i(p+"addPageAction",!0),s.setCurrentRouteName=i(p+"routeName",!0),e.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(t,e){var n={},r=this,i="function"==typeof e;return o(l+"tracer",[u.now(),t,n],r),function(){if(c.emit((i?"":"no-")+"fn-start",[u.now(),r,i],n),i)try{return e.apply(this,arguments)}catch(t){throw c.emit("fn-err",[arguments,this,t],n),t}finally{c.emit("fn-end",[u.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(t,e){m[e]=i(l+e)}),newrelic.noticeError=function(t,e){"string"==typeof t&&(t=new Error(t)),o("err",[t,u.now(),!1,e])}},{}],2:[function(t,e,n){function r(t){if(NREUM.init){for(var e=NREUM.init,n=t.split("."),r=0;r<n.length-1;r++)if(e=e[n[r]],"object"!=typeof e)return;return e=e[n[n.length-1]]}}e.exports={getConfiguration:r}},{}],3:[function(t,e,n){function r(){return f.exists&&performance.now?Math.round(performance.now()):(o=Math.max((new Date).getTime(),o))-a}function i(){return o}var o=(new Date).getTime(),a=o,f=t(10);e.exports=r,e.exports.offset=a,e.exports.getLastTimestamp=i},{}],4:[function(t,e,n){function r(t){return!(!t||!t.protocol||"file:"===t.protocol)}e.exports=r},{}],5:[function(t,e,n){function r(t,e){var n=t.getEntries();n.forEach(function(t){"first-paint"===t.name?d("timing",["fp",Math.floor(t.startTime)]):"first-contentful-paint"===t.name&&d("timing",["fcp",Math.floor(t.startTime)])})}function i(t,e){var n=t.getEntries();n.length>0&&d("lcp",[n[n.length-1]])}function o(t){t.getEntries().forEach(function(t){t.hadRecentInput||d("cls",[t])})}function a(t){if(t instanceof m&&!g){var e=Math.round(t.timeStamp),n={type:t.type};e<=p.now()?n.fid=p.now()-e:e>p.offset&&e<=Date.now()?(e-=p.offset,n.fid=p.now()-e):e=p.now(),g=!0,d("timing",["fi",e,n])}}function f(t){d("pageHide",[p.now(),t])}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var c,u,s,d=t("handle"),p=t("loader"),l=t(7),m=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){c=new PerformanceObserver(r);try{c.observe({entryTypes:["paint"]})}catch(v){}u=new PerformanceObserver(i);try{u.observe({entryTypes:["largest-contentful-paint"]})}catch(v){}s=new PerformanceObserver(o);try{s.observe({type:"layout-shift",buffered:!0})}catch(v){}}if("addEventListener"in document){var g=!1,h=["click","keydown","mousedown","pointerdown","touchstart"];h.forEach(function(t){document.addEventListener(t,a,!1)})}l(f)}},{}],6:[function(t,e,n){function r(t,e){if(!i)return!1;if(t!==i)return!1;if(!e)return!0;if(!o)return!1;for(var n=o.split("."),r=e.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var f=navigator.userAgent,c=f.match(a);c&&f.indexOf("Chrome")===-1&&f.indexOf("Chromium")===-1&&(i="Safari",o=c[1])}e.exports={agent:i,version:o,match:r}},{}],7:[function(t,e,n){function r(t){function e(){t(a&&document[a]?document[a]:document[i]?"hidden":"visible")}"addEventListener"in document&&o&&document.addEventListener(o,e,!1)}e.exports=r;var i,o,a;"undefined"!=typeof document.hidden?(i="hidden",o="visibilitychange",a="visibilityState"):"undefined"!=typeof document.msHidden?(i="msHidden",o="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(i="webkitHidden",o="webkitvisibilitychange",a="webkitVisibilityState")},{}],8:[function(t,e,n){function r(t,e){var n=[],r="",o=0;for(r in t)i.call(t,r)&&(n[o]=e(r,t[r]),o+=1);return n}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],9:[function(t,e,n){function r(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var r=-1,i=n-e||0,o=Array(i<0?0:i);++r<i;)o[r]=t[e+r];return o}e.exports=r},{}],10:[function(t,e,n){e.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(t,e,n){function r(){}function i(t){function e(t){return t&&t instanceof r?t:t?u(t,c,a):a()}function n(n,r,i,o,a){if(a!==!1&&(a=!0),!l.aborted||o){t&&a&&t(n,r,i);for(var f=e(i),c=v(n),u=c.length,s=0;s<u;s++)c[s].apply(f,r);var p=d[w[n]];return p&&p.push([b,n,r,f]),f}}function o(t,e){y[t]=v(t).concat(e)}function m(t,e){var n=y[t];if(n)for(var r=0;r<n.length;r++)n[r]===e&&n.splice(r,1)}function v(t){return y[t]||[]}function g(t){return p[t]=p[t]||i(n)}function h(t,e){l.aborted||s(t,function(t,n){e=e||"feature",w[n]=e,e in d||(d[e]=[])})}var y={},w={},b={on:o,addEventListener:o,removeEventListener:m,emit:n,get:g,listeners:v,context:e,buffer:h,abort:f,aborted:!1};return b}function o(t){return u(t,c,a)}function a(){return new r}function f(){(d.api||d.feature)&&(l.aborted=!0,d=l.backlog={})}var c="nr@context",u=t("gos"),s=t(8),d={},p={},l=e.exports=i();e.exports.getOrSetContext=o,l.backlog=d},{}],gos:[function(t,e,n){function r(t,e,n){if(i.call(t,e))return t[e];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return t[e]=r,r}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],handle:[function(t,e,n){function r(t,e,n,r){i.buffer([t],r),i.emit(t,e,n)}var i=t("ee").get("handle");e.exports=r,r.ee=i},{}],id:[function(t,e,n){function r(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:a(t,o,function(){return i++})}var i=1,o="nr@id",a=t("gos");e.exports=r},{}],loader:[function(t,e,n){function r(){if(!R++){var t=M.info=NREUM.info,e=v.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(t&&t.licenseKey&&t.applicationID&&e))return u.abort();c(E,function(e,n){t[e]||(t[e]=n)});var n=a();f("mark",["onload",n+M.offset],null,"api"),f("timing",["load",n]);var r=v.createElement("script");0===t.agent.indexOf("http://")||0===t.agent.indexOf("https://")?r.src=t.agent:r.src=l+"://"+t.agent,e.parentNode.insertBefore(r,e)}}function i(){"complete"===v.readyState&&o()}function o(){f("mark",["domContent",a()+M.offset],null,"api")}var a=t(3),f=t("handle"),c=t(8),u=t("ee"),s=t(6),d=t(4),p=t(2),l=p.getConfiguration("ssl")===!1?"http":"https",m=window,v=m.document,g="addEventListener",h="attachEvent",y=m.XMLHttpRequest,w=y&&y.prototype,b=!d(m.location);NREUM.o={ST:setTimeout,SI:m.setImmediate,CT:clearTimeout,XHR:y,REQ:m.Request,EV:m.Event,PR:m.Promise,MO:m.MutationObserver};var x=""+location,E={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1209.min.js"},O=y&&w&&w[g]&&!/CriOS/.test(navigator.userAgent),M=e.exports={offset:a.getLastTimestamp(),now:a,origin:x,features:{},xhrWrappable:O,userAgent:s,disabled:b};if(!b){t(1),t(5),v[g]?(v[g]("DOMContentLoaded",o,!1),m[g]("load",r,!1)):(v[h]("onreadystatechange",i),m[h]("onload",r)),f("mark",["firstbyte",a.getLastTimestamp()],null,"api");var R=0}},{}],"wrap-function":[function(t,e,n){function r(t,e){function n(e,n,r,c,u){function nrWrapper(){var o,a,s,p;try{a=this,o=d(arguments),s="function"==typeof r?r(o,a):r||{}}catch(l){i([l,"",[o,a,c],s],t)}f(n+"start",[o,a,c],s,u);try{return p=e.apply(a,o)}catch(m){throw f(n+"err",[o,a,m],s,u),m}finally{f(n+"end",[o,a,p],s,u)}}return a(e)?e:(n||(n=""),nrWrapper[p]=e,o(e,nrWrapper,t),nrWrapper)}function r(t,e,r,i,o){r||(r="");var f,c,u,s="-"===r.charAt(0);for(u=0;u<e.length;u++)c=e[u],f=t[c],a(f)||(t[c]=n(f,s?c+r:r,i,c,o))}function f(n,r,o,a){if(!m||e){var f=m;m=!0;try{t.emit(n,r,o,e,a)}catch(c){i([c,n,r,o],t)}m=f}}return t||(t=s),n.inPlace=r,n.flag=p,n}function i(t,e){e||(e=s);try{e.emit("internal-error",t)}catch(n){}}function o(t,e,n){if(Object.defineProperty&&Object.keys)try{var r=Object.keys(t);return r.forEach(function(n){Object.defineProperty(e,n,{get:function(){return t[n]},set:function(e){return t[n]=e,e}})}),e}catch(o){i([o],n)}for(var a in t)l.call(t,a)&&(e[a]=t[a]);return e}function a(t){return!(t&&t instanceof Function&&t.apply&&!t[p])}function f(t,e){var n=e(t);return n[p]=t,o(t,n,s),n}function c(t,e,n){var r=t[e];t[e]=f(r,n)}function u(){for(var t=arguments.length,e=new Array(t),n=0;n<t;++n)e[n]=arguments[n];return e}var s=t("ee"),d=t(9),p="nr@original",l=Object.prototype.hasOwnProperty,m=!1;e.exports=r,e.exports.wrapFunction=f,e.exports.wrapInPlace=c,e.exports.argsToArray=u},{}]},{},["loader"]);</script><body>

    <input type="hidden" id="hidFullPath" value="https://adamcruse.schoolwires.net/" />
    <input type="hidden" id="hidActiveChannelNavType" value="0" />
    <input type="hidden" id="hidActiveChannel" value ="6" />
    <input type="hidden" id="hidActiveSection" value="12" />

    <!-- OnScreen Alert Dialog Start -->
    <div id="onscreenalert-holder"></div>
    <!-- OnScreen Alert Dialog End -->

    <!-- ADA Skip Nav -->
    <div class="sw-skipnav-outerbar">
        <a href="#sw-maincontent" id="skipLink" class="sw-skipnav" tabindex="0">Skip to Main Content</a>
    </div>

    <!-- DashBoard SideBar Start -->
    
    <!-- DashBoard SideBar End -->

    <!-- off-canvas menu enabled-->
    

    

<style type="text/css">
	/* SPECIAL MODE BAR */
	div.sw-special-mode-bar {
		background: #FBC243 url('https://adamcruse.schoolwires.net/Static//GlobalAssets/Images/special-mode-bar-background.png') no-repeat;
		height: 30px;
		text-align: left;
		font-size: 12px;
		position: relative;
		z-index: 10000;
	}
	div.sw-special-mode-bar > div {
		padding: 8px 0 0 55px;
		font-weight: bold;
	}
	div.sw-special-mode-bar > div > a {
		margin-left: 20px;
		background: #A0803D;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		color: #fff;
		padding: 4px 6px 4px 6px;
		font-size: 11px;
	}

	/* END SPECIAL MODE BAR */
</style>

<script type="text/javascript">
	
	function SWEndPreviewMode() { 
		var data = "{}";
		var success = "window.location='';";
		var failure = "CallControllerFailure(result[0].errormessage);";
		CallController("https://adamcruse.schoolwires.net/site/SiteController.aspx/EndPreviewMode", data, success, failure);
	}
	
    function SWEndEmulationMode() {
        var data = "{}";
        var success = "DeleteCookie('SourceEmulationUserID');DeleteCookie('SidebarIsClosed');window.location='https://adamcruse.schoolwires.net/ums/Users/Users.aspx';";
        var failure = "CallControllerFailure(result[0].errormessage);";
        CallController("https://adamcruse.schoolwires.net/site/SiteController.aspx/EndEmulationMode", data, success, failure);
	}

	function SWEndPreviewConfigMode() {
	    var data = "{}";
	    var success = "window.location='';";
	    var failure = "CallControllerFailure(result[0].errormessage);";
	    CallController("https://adamcruse.schoolwires.net/site/SiteController.aspx/EndPreviewConfigMode", data, success, failure);
	}
</script>
            

    <!-- BEGIN - MYSTART BAR -->
<div id='sw-mystart-outer' class='noprint'>
<div id='sw-mystart-inner'>
<div id='sw-mystart-left'>
<div class='sw-mystart-nav sw-mystart-button home'><a tabindex="0" href="https://adamcruse.schoolwires.net/Domain/4" alt="District Home" title="Return to the homepage on the district site."><span>District Home<div id='sw-home-icon'></div>
</span></a></div>
<div class='sw-mystart-nav sw-mystart-dropdown schoollist' tabindex='0' aria-label='Select a School' role='navigation'>
<div class='selector' aria-hidden='true'>Select a School...</div>
<div class='sw-dropdown' aria-hidden='false'>
<div class='sw-dropdown-selected' aria-hidden='true'>Select a School</div>
<ul class='sw-dropdown-list' aria-hidden='false' aria-label='Schools'>
<li><a href="https://adamcruse.schoolwires.net/Domain/9">Dev Site 2 (es)</a></li>
<li><a href="https://adamcruse.schoolwires.net/Domain/10">Raph (hs)</a></li>
<li><a href="https://adamcruse.schoolwires.net/Domain/11">Mikey (ea)</a></li>
<li><a href="https://adamcruse.schoolwires.net/Domain/8">Leo (ms) (es)</a></li>
<li><a href="https://adamcruse.schoolwires.net/Domain/29">Styler Training (ct) (hs) (es)</a></li>
</ul>
</div>
<div class='sw-dropdown-arrow' aria-hidden='true'></div>
</div>
</div>
<div id='sw-mystart-right'>
<div id='ui-btn-signin' class='sw-mystart-button signin'><a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=7&SiteID=4&IgnoreRedirect=true"><span>Sign In</span></a></div>
<div id='ui-btn-register' class='sw-mystart-button register'><a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=10&SiteID=4"><span>Register</span></a></div>
<div id='sw-mystart-search' class='sw-mystart-nav'>
<script type="text/javascript">
$(document).ready(function() {
    $('#sw-search-input').keyup(function(e) {        if (e.keyCode == 13) {
            SWGoToSearchResultsPageswsearchinput();
        }
    });
    $('#sw-search-input').val($('#swsearch-hid-word').val())});
function SWGoToSearchResultsPageswsearchinput() {
window.location.href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=6&SiteID=4&SearchString=" + $('#sw-search-input').val();
}
</script>
<label for="sw-search-input" class="hidden-label">Search Our Site</label>
<input id="sw-search-input" type="text" title="Search Term" aria-label="Search Our Site" placeholder="Search this Site..." />
<a tabindex="0" id="sw-search-button" title="Search" href="javascript:;" role="button" aria-label="Submit Site Search" onclick="SWGoToSearchResultsPageswsearchinput();"><span><img src="https://adamcruse.schoolwires.net/Static//globalassets/images/sw-mystart-search.png" alt="Search" /></span></a><script type="text/javascript">
$(document).ready(function() {
    $('#sw-search-button').keyup(function(e) {        if (e.keyCode == 13) {
            SWGoToSearchResultsPageswsearchinput();        }
    });
});
</script>

</div>
<div class='clear'></div>
</div>
</div>
</div>
<!-- END - MYSTART BAR -->
<div id="gb-page" class="sp eisd">
	<header id="gb-header">
		<section class="header-row mobile"></section>
		<section class="header-row one">
			<div class="cs-mystart-button home">
				<a class="cs-button-selector" href="https://adamcruse.schoolwires.net/">District Home</a>
			</div>
			<div class="cs-mystart-dropdown our-schools">
				<div class="cs-dropdown-selector" tabindex="0" aria-label='Our Schools' role="button" aria-expanded="false" aria-haspopup="true">Our Schools</div>
				<div id="gb-schools-wrapper" aria-hidden="true" style="display:none;"></div>
			</div>
			<div class="cs-mystart-dropdown translate">
				<div class="cs-dropdown-selector" tabindex="0" aria-label='Translate' role="button" aria-expanded="false" aria-haspopup="true">Translate</div>
					<div class="cs-dropdown" aria-hidden="true" style="display:none;">
				</div>
			</div>
			<div class="cs-mystart-dropdown user-options">
				<div class="cs-dropdown-selector" tabindex="0" aria-label='User Options' role="button" aria-expanded="false" aria-haspopup="true">User Options</div>
				<div class="cs-dropdown" aria-hidden="true" style="display:none;">
					<ul class="cs-dropdown-list"></ul>
				</div>
			</div>
			<div class="cs-mystart-button search">
				<div class="cs-button-selector" tabindex="0" aria-label='Open Search' role="button" aria-expanded="false" aria-haspopup="true">Search</div>
				<div id="gb-search" aria-hidden="true">
					<div class="content-wrap">
						<form id="gb-search-form">
		                    <label for="gb-search-input">Search</label>
		                    <input type="text" id="gb-search-input" tabindex="-1" value="Search...">
							<button type="submit" form="gb-search-form" tabindex="-1" id="gb-search-button" aria-label="Submit Search"></button>
		                </form>
						<button id="search-control-close" aria-label="Close Search" tabindex="-1" aria-expanded="false" aria-controls="gb-search"></button>
					</div>
                </div>
			</div>
		</section>
		<section class="header-row two">
			<div role="presentation" id="header-background-title">Arizona</div>
			<div class="content-wrap">
				<div id="gb-logo-sitename">
					<div id="gb-logo"></div>
					<div id="gb-sitename">
						<h1>
							<span class="sitename">Cartwright</span>
							<span class="sitename">School District</span>
						</h1>
					</div>
				</div>
			</div>
		</section>
		<section class="header-row three">
			<nav aria-label="Site Navigation" id="gb-channel-bar" class="content-wrap" >
				<div id="sw-channel-list-container" role="navigation">
<ul id="channel-navigation" class="sw-channel-list" role="menubar">
<li id="navc-HP" class="sw-channel-item" ><a href="https://adamcruse.schoolwires.net/Page/1" aria-label="Home"><span>Home</span></a></li>
<li id="navc-6" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=1&SiteID=4&ChannelID=6&DirectoryType=6
"">
<span>About Us</span></a>
<div class="hidden-sections"><ul>"
<li id="navs-12" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/12"><span>Employment</span></a></li>
<li id="navs-13" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/13"><span>Embed Code</span></a></li>
<li id="navs-14" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/14"><span>Section 3</span></a></li>
<li id="navs-75" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/75"><span>Breaking Bad</span></a></li>
<li id="navs-76" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/76"><span>30 Rock</span></a></li>
<li id="navs-77" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/77"><span>Parks and Rec</span></a></li>
<li id="navs-102" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/102"><span>Ulysses</span></a></li>
<li id="navs-103" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/103"><span>The Great Gatsby</span></a></li>
<li id="navs-104" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/104"><span>A portrait of the Artist as a Young Man</span></a></li>
<li id="navs-105" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/105"><span>Lolita</span></a></li>
<li id="navs-106" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/106"><span>Brave New World</span></a></li>
<li id="navs-107" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/107"><span>The Sound and the Fury</span></a></li>
<li id="navs-108" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/108"><span>Catch 22</span></a></li>
<li id="navs-109" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/109"><span>Darkness at Noon</span></a></li>
<li id="navs-110" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/110"><span>Sons and Lovers</span></a></li>
<li id="navs-111" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/111"><span>The Grapes of Wrath</span></a></li>
<li id="navs-112" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/112"><span>Under the Volcano</span></a></li>
<li id="navs-114" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/114"><span>1984</span></a></li>
<li id="navs-115" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/115"><span>I, Claudius</span></a></li>
<li id="navs-116" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/116"><span>To the Lighthouse</span></a></li>
<li id="navs-117" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/117"><span>An American Tragedy</span></a></li>
<li id="navs-118" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/118"><span>The Heart is a Lonely Hunter</span></a></li>
<li id="navs-119" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/119"><span>Slaughterhouse-Five</span></a></li>
<li id="navs-120" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/120"><span>Invisible Man</span></a></li>
<li id="navs-121" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/121"><span>Native Son</span></a></li>
<li id="navs-122" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/122"><span>Henderson the Rain King</span></a></li>
<li id="navs-123" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/123"><span>Appointment in Samarra</span></a></li>
<li id="navs-124" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/124"><span>U.S.A.</span></a></li>
<li id="navs-125" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/125"><span>Winesburg, Ohio</span></a></li>
<li id="navs-113" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/113"><span>The Way of All Flesh</span></a></li>

</ul></div>
<ul class="dropdown-hidden">
</ul>
</li><li id="navc-203" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/domain/204"">
<span>App Testing</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-204"><a href="https://adamcruse.schoolwires.net/domain/204"><span>Apps</span></a></li>
</ul>
</li><li id="navc-19" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/28"">
<span>Teachers</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-28"><a href="https://adamcruse.schoolwires.net/domain/28"><span>Section 1</span></a></li>
</ul>
</li><li id="navc-55" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/59"">
<span>Our Parents</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-59"><a href="https://adamcruse.schoolwires.net/domain/59"><span>Section 1</span></a></li>
</ul>
</li><li id="navc-56" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/60"">
<span>Our Students</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-60"><a href="https://adamcruse.schoolwires.net/domain/60"><span>Section 1</span></a></li>
</ul>
</li><li id="navc-18" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/domain/25"">
<span>Employment</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-25"><a href="https://adamcruse.schoolwires.net/domain/25"><span>Section 1</span></a></li>
<li id="navs-26"><a href="https://adamcruse.schoolwires.net/domain/26"><span>Section 2</span></a></li>
<li id="navs-27"><a href="https://adamcruse.schoolwires.net/domain/27"><span>Section 3</span></a></li>
<li id="navs-177"><a href="https://adamcruse.schoolwires.net/domain/177"><span>Section 4</span></a></li>
</ul>
</li><li id="navc-57" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/61"">
<span>For Staff</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-61"><a href="https://adamcruse.schoolwires.net/domain/61"><span>Section 1</span></a></li>
</ul>
</li><li id="navc-73" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/domain/74"">
<span>Schools</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-74"><a href="https://adamcruse.schoolwires.net/domain/74"><span>School Name 1</span></a></li>
<li id="navs-133"><a href="https://adamcruse.schoolwires.net/domain/133"><span>School Name 2</span></a></li>
<li id="navs-134"><a href="https://adamcruse.schoolwires.net/domain/134"><span>School Name 3</span></a></li>
<li id="navs-135"><a href="https://adamcruse.schoolwires.net/domain/135"><span>School Name 4</span></a></li>
<li id="navs-136"><a href="https://adamcruse.schoolwires.net/domain/136"><span>School Name 5</span></a></li>
</ul>
</li><li id="navc-145" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/151"">
<span>Athletics</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-151"><a href="https://adamcruse.schoolwires.net/domain/151"><span>Athletics - Section 1</span></a></li>
<li id="navs-152"><a href="https://adamcruse.schoolwires.net/domain/152"><span>Athletics - Section 2</span></a></li>
<li id="navs-153"><a href="https://adamcruse.schoolwires.net/domain/153"><span>Athletics - Section 3</span></a></li>
</ul>
</li><li id="navc-146" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/domain/154"">
<span>Parents</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-154"><a href="https://adamcruse.schoolwires.net/domain/154"><span>Parents - Section 1</span></a></li>
<li id="navs-155"><a href="https://adamcruse.schoolwires.net/domain/155"><span>Parents - Section 2</span></a></li>
<li id="navs-156"><a href="https://adamcruse.schoolwires.net/domain/156"><span>Parents - Section 3</span></a></li>
<li id="navs-157"><a href="https://adamcruse.schoolwires.net/domain/157"><span>Parents - Section 4</span></a></li>
<li id="navs-158"><a href="https://adamcruse.schoolwires.net/domain/158"><span>Parents - Section 5</span></a></li>
<li id="navs-159"><a href="https://adamcruse.schoolwires.net/domain/159"><span>Parents - Section 6</span></a></li>
<li id="navs-176"><a href="https://adamcruse.schoolwires.net/domain/176"><span>Parents - Section 7</span></a></li>
</ul>
</li><li id="navc-147" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/160"">
<span>Upcoming Events</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-160"><a href="https://adamcruse.schoolwires.net/domain/160"><span>Upcoming Events - Section 1</span></a></li>
<li id="navs-161"><a href="https://adamcruse.schoolwires.net/domain/161"><span>Upcoming Events - Section 2</span></a></li>
</ul>
</li><li id="navc-54" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/domain/58"">
<span>Our District</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-58"><a href="https://adamcruse.schoolwires.net/domain/58"><span>Our Community</span></a></li>
<li id="navs-62"><a href="https://adamcruse.schoolwires.net/domain/62"><span>Section 2</span></a></li>
<li id="navs-63"><a href="https://adamcruse.schoolwires.net/domain/63"><span>Section 3</span></a></li>
<li id="navs-64"><a href="https://adamcruse.schoolwires.net/domain/64"><span>Section 4</span></a></li>
<li id="navs-65"><a href="https://adamcruse.schoolwires.net/domain/65"><span>Section 5</span></a></li>
<li id="navs-66"><a href="https://adamcruse.schoolwires.net/domain/66"><span>Section 6</span></a></li>
<li id="navs-67"><a href="https://adamcruse.schoolwires.net/domain/67"><span>Section 7</span></a></li>
<li id="navs-68"><a href="https://adamcruse.schoolwires.net/domain/68"><span>Section 8</span></a></li>
<li id="navs-69"><a href="https://adamcruse.schoolwires.net/domain/69"><span>Section 9</span></a></li>
<li id="navs-70"><a href="https://adamcruse.schoolwires.net/domain/70"><span>Section 10</span></a></li>
<li id="navs-71"><a href="https://adamcruse.schoolwires.net/domain/71"><span>Section 11</span></a></li>
<li id="navs-72"><a href="https://adamcruse.schoolwires.net/domain/72"><span>Section 12</span></a></li>
</ul>
</li><li id="navc-148" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/162"">
<span>Communities</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-162"><a href="https://adamcruse.schoolwires.net/domain/162"><span>Communities - Section 1</span></a></li>
<li id="navs-163"><a href="https://adamcruse.schoolwires.net/domain/163"><span>Communities - Section 2</span></a></li>
<li id="navs-164"><a href="https://adamcruse.schoolwires.net/domain/164"><span>Communities - Section 3</span></a></li>
</ul>
</li><li id="navc-149" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/165"">
<span>Contact Us</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-165"><a href="https://adamcruse.schoolwires.net/domain/165"><span>Contact Us - Section 1</span></a></li>
<li id="navs-166"><a href="https://adamcruse.schoolwires.net/domain/166"><span>Contact Us - Section 2</span></a></li>
</ul>
</li><li id="navc-150" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/domain/167"">
<span>Students</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-167"><a href="https://adamcruse.schoolwires.net/domain/167"><span>Students - Section 1</span></a></li>
<li id="navs-168"><a href="https://adamcruse.schoolwires.net/domain/168"><span>Students - Section 2</span></a></li>
<li id="navs-169"><a href="https://adamcruse.schoolwires.net/domain/169"><span>Students - Section 3</span></a></li>
</ul>
</li><li id="navc-17" class="sw-channel-item">
<a href="https://adamcruse.schoolwires.net/domain/20"">
<span>Departments</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-20"><a href="https://adamcruse.schoolwires.net/domain/20"><span>Section 1</span></a></li>
<li id="navs-21"><a href="https://adamcruse.schoolwires.net/domain/21"><span>Section 2</span></a></li>
<li id="navs-22"><a href="https://adamcruse.schoolwires.net/domain/22"><span>Section 3</span></a></li>
<li id="navs-23"><a href="https://adamcruse.schoolwires.net/domain/23"><span>Section 4</span></a></li>
<li id="navs-24"><a href="https://adamcruse.schoolwires.net/domain/24"><span>Section 5</span></a></li>
<li id="navs-126"><a href="https://adamcruse.schoolwires.net/domain/126"><span>Another Test</span></a></li>
<li id="navs-127"><a href="https://adamcruse.schoolwires.net/domain/127"><span>Testing</span></a></li>
</ul>
</li><li id="navc-178" class="hidden-channel">
<a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageID=272"">
<span>Community</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-179"><a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageID=272"><span>District Application Programs</span></a></li>
<li id="navs-180"><a href="http://www.google.com"><span>Test Section</span></a></li>
</ul>
</li><li id="navc-CA" class="sw-channel-item "><a href="https://adamcruse.schoolwires.net/Page/2"><span>Calendar</span></a></li>
</ul><div class='clear'></div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        channelHoverIE();
        channelTouch();
        closeMenuByPressingKey();
    });

    function channelTouch() {
        // this will change the dropdown behavior when it is touched vs clicked.
        // channels will be clickable on second click. first click simply opens the menu.
        $('#channel-navigation > .sw-channel-item > a').on({
            'touchstart': function (e) {
                
                // see if has menu
                if ($(this).siblings('ul.sw-channel-dropdown').children('li').length > 0)  {
                    var button = $(this);

                    // add href as property if not already set
                    // then remove href attribute
                    if (!button.prop('linkHref')) {
                        button.prop('linkHref', button.attr('href'));
                        button.removeAttr('href');
                    }

                    // check to see if menu is already open
                    if ($(this).siblings('ul.sw-channel-dropdown').is(':visible')) {
                        // if open go to link
                        window.location.href = button.prop('linkHref');
                    } 

                } 
            }
        });
    }


    
    function channelHoverIE(){
		// set z-index for IE7
		var parentZindex = $('#channel-navigation').parents('div:first').css('z-index');
		var zindex = (parentZindex > 0 ? parentZindex : 8000);
		$(".sw-channel-item").each(function(ind) {
			$(this).css('z-index', zindex - ind);
			zindex --;
		});
	    $(".sw-channel-item").hover(function(){
	        var subList = $(this).children('ul');
	        if ($.trim(subList.html()) !== "") {
	            subList.show();
	            subList.attr("aria-hidden", "false").attr("aria-expanded", "true");
		    }
		    $(this).addClass("hover");
	    }, function() {
	        $(".sw-channel-dropdown").hide();
	        $(this).removeClass("hover");
	        var subList = $(this).children('ul');
	        if ($.trim(subList.html()) !== "") {
	            subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
	        }
	    });
    }

    function closeMenuByPressingKey() {
        $(".sw-channel-item").each(function(ind) {
            $(this).keyup(function (event) {
                if (event.keyCode == 27) { // ESC
                    $(".sw-channel-dropdown").hide();
                    $(this).removeClass("hover");
                    var subList = $(this).children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                }
                if (event.keyCode == 13 || event.keyCode == 32) { //enter or space
                    $(this).find('a').get(0).click();
                }
            }); 
        });

        $(".sw-channel-item a").each(function (ind) {
            $(this).parents('.sw-channel-item').keydown(function (e) {
                if (e.keyCode == 9) { // TAB
                    $(".sw-channel-dropdown").hide();
                    $(this).removeClass("hover");
                    var subList = $(this).children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                }
            });
        });

        $(".sw-channel-dropdown li").each(function(ind) {
            $(this).keydown(function (event) {
                if (event.keyCode == 9) { // TAB
                    $(".sw-channel-dropdown").hide();
                    var parentMenuItem = $(this).parent().closest('li');
                    parentMenuItem.removeClass("hover");
                    var subList = parentMenuItem.children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                    parentMenuItem.next().find('a:first').focus();
                    event.preventDefault();
                    event.stopPropagation();
                }

                if (event.keyCode == 37 || // left arrow
                    event.keyCode == 39) { // right arrow
                    $(".sw-channel-dropdown").hide();
                    var parentMenuItem = $(this).parent().closest('li');
                    parentMenuItem.removeClass("hover");
                    var subList = parentMenuItem.children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                    if (event.keyCode == 37) {
                        parentMenuItem.prev().find('a:first').focus();
                    } else {
                        parentMenuItem.next().find('a:first').focus();
                    }
                    event.preventDefault();
                    event.stopPropagation();
                }
            });
        });
    }

</script>


			</nav>
		</section>
	</header>
	<main id="sp-content">
		<section id="sp-breadcrumbs" class="sp-row one noprint">
			<div class="content-wrap">
				<ul class="ui-breadcrumbs" role="navigation" aria-label="Breadcrumbs">
<li class="ui-breadcrumb-first"><a href="https://adamcruse.schoolwires.net/Domain/4"><span>Dev Site 1</span></a></li>

<li data-bccID="6"><a href=""><span></span></a></li>

<li class="ui-breadcrumb-last" data-bcsID="12"><a href=""><span></span></a></li>

<li class="ui-breadcrumb-last""><span>Sample Page</span></li>

</ul>

			</div>
		</section>
		<section class="sp-row two">
			<div class="content-wrap">
				<div class="sp-column one noprint">
					<div class='ui-widget app navigation pagenavigation'>
<div class="ui-widget-header">
<h1>
Employment</h1>
</div>
<div class='ui-widget-detail'>
<script type="text/javascript" src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/sw-ada.js"></script><script type="text/javascript">$(document).ready(function () {WCM.ADA.tree = new WCM.ADA.Tree();});</script><ul class="page-navigation">
<li id="pagenavigation-17" class="active">
<a href="https://adamcruse.schoolwires.net/Page/17"
><span>Sample Page</span>
</a>
</li>
<li id="pagenavigation-57" class="">
<a href="https://adamcruse.schoolwires.net/Page/57"
><span>Parent Page</span>
</a>
<ul>
<li id="pagenavigation-58" class="">
<a href="https://adamcruse.schoolwires.net/Page/58"
><span>Child Page</span>
</a>
</li>
<li id="pagenavigation-59" class="">
<a href="https://adamcruse.schoolwires.net/Page/59"
><span>Another Child Page</span>
</a>
</li></ul>
</li>
<li id="pagenavigation-60" class="">
<a href="https://adamcruse.schoolwires.net/Page/60"
><span>Page Number Three</span>
</a>
</li>
<li id="pagenavigation-62" class="">
<a href="https://adamcruse.schoolwires.net/Page/62"
><span>Another Parent Page</span>
</a>
<ul>
<li id="pagenavigation-63" class="">
<a href="https://adamcruse.schoolwires.net/Page/63"
><span>Yo, Sup?</span>
</a>
</li>
<li id="pagenavigation-64" class="">
<a href="https://adamcruse.schoolwires.net/Page/64"
><span>Calendar</span>
</a>
</li></ul>
</li>
<li id="pagenavigation-61" class="">
<a href="https://adamcruse.schoolwires.net/Page/61"
><span>Hey, I'm a Page Too</span>
</a>
</li>
<li id="pagenavigation-236" class="">
<a href="https://adamcruse.schoolwires.net/Page/236"
><span>Restricted Test</span>
</a>
</li>
<li id="pagenavigation-277" class="">
<a href="https://adamcruse.schoolwires.net/Page/277"
><span>Broward Test</span>
</a>
</li></ul>
</div>
<div class="ui-widget-footer">
</div>
</div>

				</div>
				<div class="sp-column two">
					<!-- Start Centricity Content Area --><div id="sw-content-layout-wrapper" class="ui-sp ui-print" role="main"><a id="sw-maincontent" name="sw-maincontent" tabindex="-1"></a><div id="sw-content-layout7"><div id="sw-content-container1" class="ui-column-one region"><div id='pmi-90'>



<div id='sw-module-170'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '12';
            var PageID = '17';
            var RenderLoc = '0';
            var MIID = '17';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-17" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header">
		<h1>App Name</h1>
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
	<div class="ui-article">
		<div class="ui-article-description">
        	<span><span ><h1>H1 Title</h1>
<h2>H2 Title</h2>
<h3>H3 Title</h3>
<h4>H4 Title</h4>
<p>&nbsp;</p>
<p><span class="Dropcap">B</span>ody Text Example - Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et. Donec quam pretium quis, sem. <a href="http://www.google.com" target="_blank" rel="noopener noreferrer">Text Link Example</a> Nulla consequat massa Text Link Example - Hover quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, magnis dis parturient montes, nascetur ridiculus mus arcu. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penat. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, magnis.</p>
<p><span class="Serif_Font">Serif Font Editor Style. Dolor sit amet, link - conse ctetur adipiscing elit. Ut ultricies molestie lacinia. Lorem m r adipiscing elit. <a href="http://www.google.com" target="_blank" rel="noopener noreferrer">I am a link within some content.</a> Ut ultricies molestie lacinia.Lorem ipsum dolor sit amet, conse ctetur adipiscing elit. Ut ultricies molestie lacinia.</span></p>
<p><span class="Call_To_Action_Button"><a href="http://www.google.com" target="_blank" rel="noopener noreferrer">Call-To-Action-Button</a></span></p>
<p>&nbsp;</p>
<p><span class="Image_Drop_Shadow"><img title="test" src="https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Domain/12/crayons.jpg" alt="test " width="210" height="131" /></span></p></span></span>
        </div>
		<div class="clear"></div>
	</div>
</li>
</ul>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div><div id="sw-content-container2" class="ui-column-one-half region" ><div id='pmi-197'>



<div id='sw-module-140'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '12';
            var PageID = '17';
            var RenderLoc = '0';
            var MIID = '14';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-14" >
<div class="ui-widget app headlines">
	
	<div class="ui-widget-header">
		<h1>What&#39;s Happening</h1>
	</div>
	
	<div class="ui-widget-detail" id="sw-app-headlines-14">
		<ul class="ui-articles">
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt=" test" height="133" width="200" src="https://adamcruse.schoolwires.net/..//cms/lib/SWCS000009/Centricity/Domain/4/ew-hf-2.jpg" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=12&ModuleInstanceID=14&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=6&PageID=17"><span>Title for the Headline</span></a>
            </h1> 
        <p class="ui-article-description">Justo lacus ad in proin integer volutpat adipiscing sapien facilisis torquent massa duis a viverra luctus fermentum orci tortor et a placerat justo</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Title for the Headline" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=12&ModuleInstanceID=14&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=6&PageID=17&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt="" height="275" width="330"  />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=12&ModuleInstanceID=14&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=7&PageID=17"><span>Title for the Headline</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac nisl a metus euismod lacinia. Praesent vulputate magna eu ornare</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Title for the Headline" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=12&ModuleInstanceID=14&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=7&PageID=17&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt="" height="133" width="200" src="https://adamcruse.schoolwires.net/..//cms/lib/SWCS000009/Centricity/Domain/4/ew-hf-1.jpg" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=12&ModuleInstanceID=14&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=128&PageID=17"><span>Alumni Speak Out</span></a>
            </h1> 
        <p class="ui-article-description">Five HCS high schools have placed in the top 25 high schools in South Carolina! Congratulations to St. James High (#14), Carolina Forest </p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Alumni Speak Out" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=12&ModuleInstanceID=14&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=128&PageID=17&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
</ul><div class="ui-read-more"><a id='MoreLinkButton14' class='more-link' aria-label='Go to more records' onclick='MoreViewClick(this);' href='https://adamcruse.schoolwires.net/site/default.aspx?PageType=14&DomainID=12&PageID=17&ModuleInstanceID=14&ViewID=c83d46ac-74fe-4857-8c9a-5922a80225e2&IsMoreExpandedView=True'><span>more</span></a><div class='more-link-under'>&nbsp;</div></div>
</ul>
	</div>

	<div class="ui-widget-footer">
		
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
        /*$(document).on('click', 'a.ui-article-thumb', function() {
        	window.location = $(this).attr('href');
    	});*/
    		
		$('#sw-app-headlines-14').find('img').each(function() {
			if ($.trim(this.src) == '' ) {
				$(this).parent().parent().remove();
			}
		});
        
        // Jason Smith - 12/9/2014 - Removed due to bandwidth implications
		
	});

</script>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-1487'>



<div id='sw-module-210'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '12';
            var PageID = '17';
            var RenderLoc = '0';
            var MIID = '21';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-21" >
<div class="ui-widget app announcements">
	<div class="ui-widget-header">
		<h1>Announcements</h1>
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>This is an important message region using the Announcements app</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Announcement 1" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=21&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=13&PageID=17&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>There is something about parenthood that gives us a sense of history and a deeply rooted desire to send on into the next. generation the great things we have discovered about life. And part of that is the desire to instill in our children the love of science, of learning and</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Announcement 2" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=21&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=14&PageID=17&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero.</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Announcement 3" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=21&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=15&PageID=17&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Announcement 4" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=21&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=16&PageID=17&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
</ul><div class="ui-read-more"><a id='MoreLinkButton21' class='more-link' aria-label='Go to more records' onclick='MoreViewClick(this);' href='https://adamcruse.schoolwires.net/site/default.aspx?PageType=14&DomainID=12&PageID=17&ModuleInstanceID=21&ViewID=606008db-225b-4ad2-8f7b-9ebac54372c1&IsMoreExpandedView=True'><span>more</span></a><div class='more-link-under'>&nbsp;</div></div>
</ul>
</div>
	<div class="ui-widget-footer">
		
		<div class="app-level-social-follow"></div> <div class="app-level-social-rss"><a title='Subscribe to RSS Feed - Announcements' tabindex='0' class='ui-btn-toolbar rss' href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=4&DomainID=12&ModuleInstanceID=21&PageID=17"><span>Subscribe to RSS Feed - Announcements </span></a></div>
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-407'>
<div id="module-content-12" >
<div class="ui-widget app navigation  siteshortcuts">
	<div class="ui-widget-header"><h1>Site Shortcuts</h1></div>
	<div class="ui-widget-detail">
		<ul class="site-shortcuts">
<li id="siteshortcut-76" class=""><a href="http://www.blackboard.com" target="_parent" >Yellow belly and crimson hands</a>
</li>
<li id="siteshortcut-75" class=""><a href="http://www.blackboard.com" target="_parent" >You&#39;re less than half a man</a>
</li>
<li id="siteshortcut-74" class=""><a href="http://www.blackboard.com" target="_parent" >To ask that it be shown to you</a>
</li>
<li id="siteshortcut-73" class=""><a href="http://www.blackboard.com" target="_parent" >What mercy have they known from you</a>
</li>
<li id="siteshortcut-1" class=""><a href="http://www.google.com" target="_blank" >Board of Trustees</a>
<ul>
<li id="siteshortcut-9" class=""><a href="http://google.com" target="_blank" >Meeting Agendas</a>
</li></ul>
</li>
<li id="siteshortcut-8" class="active"><a href="https://adamcruse.schoolwires.net/Page/17" target="_parent" >Superintendent Link</a>
<ul>
<li id="siteshortcut-7" class=""><a href="https://adamcruse.schoolwires.net/Page/2" target="_parent" >Another Quick Link</a>
</li></ul>
</li>
<li id="siteshortcut-5" class=""><a href="http://google.com" target="_blank" >Board of Trustees</a>
</li>
<li id="siteshortcut-3" class=""><a href="http://www.google.com" target="_blank" >Meeting Agendas</a>
</li>
<li id="siteshortcut-2" class=""><a href="https://adamcruse.schoolwires.net/Page/5" target="_parent" >Another Quick Link</a>
<ul>
<li id="siteshortcut-49" class="navigationgroup"><a >Nested Link One</a>
</li></ul>
</li>
<li id="siteshortcut-50" class="navigationgroup"><a >Nested Link Two</a>
</li>
<li id="siteshortcut-6" class=""><a href="http://google.com" target="_blank" >Superintendent Link</a>
</li>
<li id="siteshortcut-4" class=""><a href="http://www.yahoo.com" target="_parent" >Third Nested Item</a>
</li>
<li id="siteshortcut-10" class=""><a href="http://google.com" target="_blank" >Fourth Item</a>
</li></ul>
<div class="app-level-social-follow"></div>
	</div>
	<div class="ui-widget-footer">
	</div>
</div></div>
</div>
</div><div id="sw-content-container3" class="ui-column-one-half region"><div id='pmi-207'>
<div id="module-content-13" ><div class="ui-widget app upcomingevents">
 <div class="ui-widget-header">
     <h1>Upcoming Events</h1>
 </div>
 <div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">June 28, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210628/event/3902">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210628/event/4214">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210628/event/4489">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">July 5, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210705/event/3903">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210705/event/4215">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210705/event/4490">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">July 12, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210712/event/3904">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210712/event/4216">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210712/event/4491">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">July 19, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210719/event/3905">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210719/event/4217">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210719/event/4492">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">July 26, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210726/event/3906">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210726/event/4218">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210726/event/4493">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">August 2, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210802/event/3907">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210802/event/4219">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210802/event/4494">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">August 9, 2021</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210809/event/3908">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=12#calendar1/20210809/event/4220">Name of Event</a></span>
     </p>
</div>
</li>
	</ul>
<a class='view-calendar-link' href="https://adamcruse.schoolwires.net/Page/2"><span>View Calendar</span></a>
 </div>
 <div class="ui-widget-footer">
 </div>
</div>
</div>
</div>
<div id='pmi-2792'>



<div id='sw-module-13700'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '12';
            var PageID = '17';
            var RenderLoc = '0';
            var MIID = '1370';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1370" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header ui-helper-hidden">
		
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
	<div class="ui-article">
		<div class="ui-article-description">
        	<span><span ><p><span class="Grey_Border_Box"><span class="Box_Title">Grey Border</span><br />Header colors match the header colors&nbsp;selected for the subpages. Body text can be set to&nbsp;grey #333333 (default) or black #000000 using the Grey Border Box Text Color setting.&nbsp;<strong><a href="https://adamcruse.schoolwires.net/#" target="_parent">Li</a><a href="https://adamcruse.schoolwires.net/#" target="_parent">nk colors&nbsp;match the Region D, E, F and G Link Color</a><a href="https://adamcruse.schoolwires.net/#" target="_parent">.</a></strong>&nbsp;Font weight is adjusted within the Content App.&nbsp;<br /></span></p>
<p><span class="Primary_Box"><span class="Box_Title">Primary</span><br />Background color and text are set using the Primary Editor Style Background Color and Primary Editor Style Text Color options in the TCW.&nbsp;<strong><a href="https://kfilko.schoolwires.net/#" target="_parent">Link&nbsp;colors are set in the TCW.</a>&nbsp;</strong>Choose Primary Editor Style Link Color located under Global Template Colors.<br /></span></p>
<p><span class="Secondary_Box"><span class="Box_Title">Secondary</span><br />Background color and text are set using the&nbsp;Secondary Editor Style Background Color and&nbsp;Secondary Editor Style Text Color options in the TCW.&nbsp;<strong><a href="https://kfilko.schoolwires.net/#" target="_parent">Link&nbsp;colors are set in the TCW.</a>&nbsp;</strong>Choose&nbsp;Primary Editor Style Link Color located under Global Template Colors.</span></p>
<p><a href="http://www.google.com" target="_blank" rel="noopener noreferrer"><span class="Button">Editor Style Button</span></a></p></span></span>
        </div>
		<div class="clear"></div>
	</div>
</li>
</ul>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=12&PageID=17&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div><div id="sw-content-container4" class="ui-column-one region"></div><div class="clear"></div></div></div><!-- End Centricity Content Area -->
				</div>
			</div>
        </section>
	</main>
	<footer id="gb-footer" class="noprint">
		<section class="footer-row icons" id="gb-icons" data-icon-num='8' data-background-opacity='62 - default'>
			<div class="content-wrap"></div>
		</section>
		<section class="footer-row one">
			<div class="content-wrap">
				<div class="footer-column one">
					<div id="gb-social">

					</div>
					<div id="gb-contact">
						<h2 id="footer-sitename">Cartwright School District</h2>
						<p class="contact-info address" data-content="1628 19th Street">
							<span class="contact-column one">1628 19th Street</span>
							<span class="contact-column two">Lubbock, TX 79401</span>
						</p>
						<p class="contact-info phone-fax">
							<span class="contact-column one" data-content="806-219-0000"><span role="presentation">P </span><span>806-219-0000</span></span>
							<span class="contact-column two" data-content="501.450.4898"><span role="presentation">F </span><span>501.450.4898</span></span>
						</p>
					</div>
				</div>
				<div class="footer-column two">
					<div id="gb-map">
						<a id="gb-google-map-link" aria-label="Go to Google Map for Cartwright School District" href="//www.google.com/maps/place/1628 19th Street Lubbock TX 79401" target="_blank">
		                  	<iframe tabindex="-1" title="Google Map" frameborder="0" width="100%" height="100%" src="https://www.google.com/maps/embed/v1/place?q=1628 19th Street Lubbock TX 79401&zoom=12&key=AIzaSyCpnvqQ2kYwxbdiPGLDp5h8qqTsw_iHeYE"></iframe>
		              	</a>
					</div>
				</div>
			</div>
		</section>
		<section class="footer-row two">
			<div id="gb-to-top" role="button" tabindex="0">
				<span>Back to Top</span>
			</div>
		</section>
		<section class="footer-row three">
			<div class="content-wrap">
				<div class="footer-column one">
					<div id="gb-bb-footer" class="ui-clear">
						<div class="gb-bb-footer logo"></div>
						<div id="gb-schoolwires-footer-links-copyright">
							<div class="gb-bb-footer links ui-clear"></div>
							<div class="gb-bb-footer copyright"></div>
						</div>
					</div>
				</div>
				<div class="footer-column two">
					<a class="footer-link" data-content='#' href='#' target='_self'>
						<span>Accessibility Contact</span>
					</a>
					<a class="footer-link" href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=15&SiteID=4&SectionMax=15&DirectoryType=6">
						<span>Sitemap</span>
					</a>
				</div>
			</div>
		</section>
	</footer>
</div>
<!-- BEGIN - STANDARD FOOTER -->
<div id='sw-footer-outer'>
<div id='sw-footer-inner'>
<div id='sw-footer-left'></div>
<div id='sw-footer-right'>
<div id='sw-footer-links'>
<ul>
<li><a title='Click to email the primary contact' href='mailto:changeme@changeme.com'>Questions or Feedback?</a> | </li>
<li><a href='https://www.blackboard.com/blackboard-web-community-manager-privacy-statement' target="_blank">Blackboard Web Community Manager Privacy Policy (Updated)</a> | </li>
<li><a href='https://help.blackboard.com/Terms_of_Use' target="_blank">Terms of Use</a></li>
</ul>
</div>
<div id='sw-footer-copyright'>Copyright &copy; 2002-2021 Blackboard, Inc. All rights reserved.</div>
<div id='sw-footer-logo'><a href='http://www.blackboard.com' title="Blackboard, Inc. All rights reserved.">
<img src='https://adamcruse.schoolwires.net/Static//GlobalAssets/Images/Navbar/blackboard_logo.png'
 alt="Blackboard, Inc. All rights reserved."/>
</a></div>
</div>
</div>
</div>
<!-- END - STANDARD FOOTER -->
<script type="text/javascript">
   $(document).ready(function(){
      var beaconURL='https://analytics.schoolwires.com/analytics.asmx/Insert?AccountNumber=6boZxfPfyUY810QWRpwW3A%3d%3d&SessionID=01196840-af4c-4795-830f-8edc7bc2f755&SiteID=4&ChannelID=6&SectionID=12&PageID=17&HitDate=6%2f22%2f2021+11%3a02%3a59+AM&Browser=Chrome+87.0&OS=Unknown&IPAddress=10.61.13.231';
      try {
         $.getJSON(beaconURL + '&jsonp=?', function(myData) {});
      } catch(err) { 
         // prevent site error for analytics
      }
   });
</script>

    <input type="hidden" id="hid-pageid" value="17" />

    

    <div id='dialog-overlay-WindowMedium-base' class='ui-dialog-overlay-base' ><div id='WindowMedium' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowMedium-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowMedium-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowMedium");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowMedium-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowMedium-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowSmall-base' class='ui-dialog-overlay-base' ><div id='WindowSmall' role='dialog' tabindex='-1'  class='ui-dialog-overlay small' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowSmall-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowSmall-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowSmall");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowSmall-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowSmall-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowLarge-base' class='ui-dialog-overlay-base' ><div id='WindowLarge' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowLarge-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowLarge-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowLarge");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowLarge-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowLarge-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-WindowMediumModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowMediumModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowMediumModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowMediumModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowMediumModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowMediumModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowMediumModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowSmallModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowSmallModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay small' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowSmallModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowSmallModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowSmallModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowSmallModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowSmallModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowLargeModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowLargeModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowLargeModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowLargeModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowLargeModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowLargeModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowLargeModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowXLargeModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowXLargeModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay xlarge' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowXLargeModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowXLargeModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowXLargeModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowXLargeModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowXLargeModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-MyAccountSubscriptionOverlay-base' class='ui-dialog-overlay-base-modal' ><div id='MyAccountSubscriptionOverlay' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-MyAccountSubscriptionOverlay-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-MyAccountSubscriptionOverlay-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("MyAccountSubscriptionOverlay");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-MyAccountSubscriptionOverlay-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-MyAccountSubscriptionOverlay-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-InsertOverlay-base' class='ui-dialog-overlay-base-modal' ><div id='InsertOverlay' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-InsertOverlay-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-InsertOverlay-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("InsertOverlay");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-InsertOverlay-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-InsertOverlay-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-InsertOverlay2-base' class='ui-dialog-overlay-base-modal' ><div id='InsertOverlay2' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-InsertOverlay2-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-InsertOverlay2-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("InsertOverlay2");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-InsertOverlay2-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-InsertOverlay2-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    
    <div id="videowrapper" class="ui-helper-hidden">
        <div id="videodialog" role="application">
            <a id="videodialog-close" role="button" href="javascript:;" aria-label="Close Overlay" class="close-btn" onclick="closeVideoDialog();">CLOSE</a>
            <div id="videodialog-video" ></div>
            <div id="videodialog-foot" tabindex="0"></div>
        </div>
    </div>
    <div id="attachmentwrapper" class="ui-helper-hidden">
        <div id="attachmentdialog" role="application">
            <a id="attachmentdialog-close" role="button" href="javascript:;" aria-label="Close Overlay" class="close-btn" onclick="closeAttachmentDialog();">CLOSE</a>
            <div id="attachmentdialog-container"></div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {

            removeBrokenImages();
            checkSidebar();
            RemoveCookie();

            $('div.bullet').attr('tabindex', '');

            $('.navigation li.collapsible').each(function () {
                if ($(this).find('ul').length == 0) {
                    $(this).removeClass('collapsible');
                }
            });

            // find page nav state cookie and add open chevron
            var arrValues = GetCookie('SWPageNavState').split('~');

            $.each(arrValues, function () {
                if (this != '') {
                    $('#' + this).addClass('collapsible').prepend("<div class='bullet collapsible' aria-label='Close Page Submenu'/>");
                }
            });

            // find remaining sub menus and add closed chevron and close menu
            $('.navigation li > ul').each(function () {
                var list = $(this);

                if (list.parent().hasClass('active') && !list.parent().hasClass('collapsible')) {
                    // open sub for currently selected page                    
                    list.parent().addClass('collapsible').prepend("<div class='bullet collapsible'aria-label='Close Page Submenu' />");
                } else {
                    if (list.parent().hasClass('collapsible') && !list.siblings('div').hasClass('collapsible')) {
                        // open sub for page with auto expand
                        list.siblings('div.expandable').remove();
                        list.parent().prepend("<div class='bullet collapsible' aria-label='Close Page Submenu' />");
                    }
                }

                if (!list.siblings('div').hasClass('collapsible')) {
                    // keep all closed that aren't already set to open
                    list.parent().addClass('expandable').prepend("<div class='bullet expandable' aria-label='Open Page Submenu' />");
                    ClosePageSubMenu(list.parent());
                } else {
                    OpenPageSubMenu(list.parent());
                }
            });

            // remove bullet from hierarchy if no-bullet set
            $('.navigation li.collapsible').each(function () {
                if ($(this).hasClass('no-bullet')) {
                    if (!$(this).hasClass('navigationgroup')) { $(this).removeClass('collapsible'); }
                    $(this).children('div.collapsible').remove();
                }
            });

            $('.navigation li.expandable').each(function () {
                if ($(this).hasClass('no-bullet')) {
                    if (!$(this).hasClass('navigationgroup')) { $(this).removeClass('expandable'); }
                    $(this).children('div.expandable').remove();
                }
            });

            $('.navigation li:not(.collapsible,.expandable,.no-bullet)').each(function () {
                $(this).prepend("<div class='bullet'/>");
            });

            $('.navigation li.active').parents('ul').each(function () {
                if (!$(this).hasClass('page-navigation')) {
                    OpenPageSubMenu($(this).parent());
                }
            });

            // Set aria ttributes
            $('li.collapsible').each(function () {
                $(this).attr("aria-expanded", "true");
                $(this).find('div:first').attr('aria-pressed', 'true');
            });

            $('li.expandable').each(function () {
                $(this).attr("aria-expanded", "false");
                $(this).find('div:first').attr('aria-pressed', 'false');
            });

            $('div.bullet').each(function () {
                $(this).attr("aria-hidden", "true");
            });

            // set click event for chevron
            $(document).on('click', '.navigation div.collapsible', function () {
                ClosePageSubMenu($(this).parent());
            });

            $(document).on('click', '.navigation div.expandable', function () {
                OpenPageSubMenu($(this).parent());
            });

            // set navigation grouping links
            $(document).on('click', '.navigationgroup.collapsible > a', function () {
                ClosePageSubMenu($(this).parent());
            });

            $(document).on('click', '.navigationgroup.expandable > a', function () {
                OpenPageSubMenu($(this).parent());
            });

            //SW MYSTART DROPDOWNS
            $(document).on('click', '.sw-mystart-dropdown', function () {
                $(this).children(".sw-dropdown").css("display", "block");
            });

            $(".sw-mystart-dropdown").hover(function () { }, function () {
                $(this).children(".sw-dropdown").hide();
                $(this).blur();
            });

            //SW ACCOUNT DROPDOWN
            $(document).on('click', '#sw-mystart-account', function () {
                $(this).children("#sw-myaccount-list").show();
                $(this).addClass("clicked-state");
            });

            $("#sw-mystart-account, #sw-myaccount-list").hover(function () { }, function () {
                $(this).children("#sw-myaccount-list").hide();
                $(this).removeClass("clicked-state");
                $("#sw-myaccount").blur();
            });

            // set hover class for page and section navigation
            $('.ui-widget.app.pagenavigation, .ui-widget.app.sectionnavigation').find('li > a').hover(function () {
                $(this).addClass('hover');
            }, function () {
                $(this).removeClass('hover');
            });

            //set aria-label for home
            $('#navc-HP > a').attr('aria-label', 'Home');

            // set active class on channel and section
            var activeChannelNavType = $('input#hidActiveChannelNavType').val();
            if (activeChannelNavType == -1) {
                // homepage is active
                $('#navc-HP').addClass('active');
            } else if (activeChannelNavType == 1) {
                // calendar page is active
                $('#navc-CA').addClass('active');
            } else {
                // channel is active - set the active class on the channel
                var activeSelectorID = $('input#hidActiveChannel').val();
                $('#navc-' + activeSelectorID).addClass('active');

                // set the breadcrumb channel href to the channel nav href
                $('li[data-bccID=' + activeSelectorID + '] a').attr('href', $('#navc-' + activeSelectorID + ' a').attr('href'));
                $('li[data-bccID=' + activeSelectorID + '] a span').text($('#navc-' + activeSelectorID + ' a span').first().text());

                // set the active class on the section
                activeSelectorID = $('input#hidActiveSection').val();
                $('#navs-' + activeSelectorID).addClass('active');

                // set the breadcrumb section href to the channel nav href
                $('li[data-bcsID=' + activeSelectorID + '] a').attr('href', $('#navs-' + activeSelectorID + ' a').attr('href'));
                if ($('#navs-' + activeSelectorID + ' a').attr('target') !== undefined) {
                    $('li[data-bcsID=' + activeSelectorID + '] a').attr('target', $('#navs-' + activeSelectorID + ' a').attr('target'));
                }
                $('li[data-bcsID=' + activeSelectorID + '] span').text($('#navs-' + activeSelectorID + ' a span').text());

                if ($('.sw-directory-columns').length > 0) {
                    $('ul.ui-breadcrumbs li:last-child').remove();
                    $('ul.ui-breadcrumbs li:last-child a').replaceWith(function() { return $('span', this); });
                    $('ul.ui-breadcrumbs li:last-child span').append(' Directory');
                }
            }
        }); // end document ready

        function OpenPageSubMenu(li) {
            if (li.prop('tagName').toLowerCase() == "li") {
                if (li.hasClass('expandable')) {
                    li.removeClass('expandable').addClass('collapsible');
                }
                if (li.find('div:first').hasClass('expandable')) {
                    li.find('div:first').removeClass('expandable').addClass('collapsible').attr('aria-pressed', 'true').attr('aria-label','Close Page Submenu');
                }
                li.find('ul:first').attr('aria-hidden', 'false').show();

                li.attr("aria-expanded", "true");

                PageNavigationStateCookie();
            }
        }

        function ClosePageSubMenu(li) {
            if (li.prop('tagName').toLowerCase() == "li") {
                li.removeClass('collapsible').addClass('expandable');
                li.find('div:first').removeClass('collapsible').addClass('expandable').attr('aria-pressed', 'false').attr('aria-label','Open Page Submenu');
                li.find('ul:first').attr('aria-hidden', 'true').hide();

                li.attr("aria-expanded", "false");

                PageNavigationStateCookie();
            }
        }

        function PageNavigationStateCookie() {
            var strCookie = "";

            $('.pagenavigation li > ul').each(function () {
                var item = $(this).parent('li');
                if (item.hasClass('collapsible') && !item.hasClass('no-bullet')) {
                    strCookie += $(this).parent().attr('id') + '~';
                }
            });

            SetCookie('SWPageNavState', strCookie);
        }

        function checkSidebar() {
            $(".ui-widget-sidebar").each(function () {
                if ($.trim($(this).html()) != "") {
                    $(this).show();
                    $(this).siblings(".ui-widget-detail").addClass("with-sidebar");
                }
            });
        }

        function removeBrokenImages() {
            //REMOVES ANY BROKEN IMAGES
            $("span.img img").each(function () {
                if ($(this).attr("src") !== undefined && $(this).attr("src") != '../../') {
                    $(this).parent().parent().show();
                    $(this).parent().parent().siblings().addClass("has-thumb");
                }
            });
        }

        function LoadEventDetailUE(moduleInstanceID, eventDateID, userRegID, isEdit) {
            (userRegID === undefined ? userRegID = 0 : '');
            (isEdit === undefined ? isEdit = false : '');
            OpenDialogOverlay("WindowMediumModal", { LoadType: "U", LoadURL: "https://adamcruse.schoolwires.net//site/UserControls/Calendar/EventDetailWrapper.aspx?ModuleInstanceID=" + moduleInstanceID + "&EventDateID=" + eventDateID + "&UserRegID=" + userRegID + "&IsEdit=" + isEdit });
        }

        function RemoveCookie() {
            // There are no sub page            
            if ($('.pagenavigation li li').length == 0) {
                //return false;
                PageNavigationStateCookie();
            }
        }
    </script>

    <script type="text/javascript">

        function AddOffCanvasMenuHeightForSiteNav() {
            var sitenavulHeight = 0;

            if ($('#sw-pg-sitenav-ul').length > 0) {
                sitenavulHeight = parseInt($("#sw-pg-sitenav-ul").height());
            }

            var swinnerwrapHeight = 0;

            if ($('#sw-inner-wrap').length > 0) {
                swinnerwrapHeight = parseInt($("#sw-inner-wrap").height());
            }

            // 360px is abount 5 li height
            if (sitenavulHeight + 360 >= swinnerwrapHeight) {
                $("#sw-inner-wrap").height(sitenavulHeight + 360);
            }
        }

        function AddOffCanvasMenuHeightForSelectSchool() {
            var selectschoolulHeight = 0;

            if ($('#sw-pg-selectschool-ul').length > 0) {
                selectschoolulHeight = parseInt($("#sw-pg-selectschool-ul").height());
            }

            var swinnerwrapHeight = 0;

            if ($('#sw-inner-wrap').length > 0) {
                swinnerwrapHeight = parseInt($("#sw-inner-wrap").height());
            }

            // 360px is abount 5 li height
            if (selectschoolulHeight + 360 >= swinnerwrapHeight) {
                $("#sw-inner-wrap").height(selectschoolulHeight + 360);
            }
        }

        $(document).ready(function () {
            if ($("#sw-pg-sitenav-a").length > 0) {
                $(document).on('click', '#sw-pg-sitenav-a', function () {
                    if ($("#sw-pg-sitenav-ul").hasClass('sw-pgmenu-closed')) {
                        AddOffCanvasMenuHeightForSiteNav();

                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-sitenav-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-sitenav-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-sitenav-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-sitenav-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '#sw-pg-selectschool-a', function () {
                    if ($("#sw-pg-selectschool-ul").hasClass('sw-pgmenu-closed')) {
                        AddOffCanvasMenuHeightForSelectSchool();

                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-selectschool-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-selectschool-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-selectschool-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-selectschool-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '#sw-pg-myaccount-a', function () {
                    if ($("#sw-pg-myaccount-ul").hasClass('sw-pgmenu-closed')) {
                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-myaccount-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-myaccount-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-myaccount-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-myaccount-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '.pg-list-bullet', function () {
                    $(this).prev().toggle();

                    if ($(this).hasClass('closed')) {
                        AddOffCanvasMenuHeightForSiteNav();

                        $(this).removeClass('closed');
                        $(this).addClass('open');
                    } else {
                        $(this).removeClass('open');
                        $(this).addClass('closed');
                    }
                });

                $(document).on('mouseover', '#sw-pg-selectschool', function () {
                    $("#sw-pg-selectschool-firstli").removeClass('sw-pg-selectschool-firstli-mouseout').addClass('sw-pg-selectschool-firstli-mouseover');
                    $("#sw-pg-selectschool-firstli a").addClass('sw-pg-selectschool-firstli-a-mouseover').removeClass('sw-pg-selectschool-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-selectschool', function () {
                    $("#sw-pg-selectschool-firstli").removeClass('sw-pg-selectschool-firstli-mouseover').addClass('sw-pg-selectschool-firstli-mouseout');
                    $("#sw-pg-selectschool-firstli a").addClass('sw-pg-selectschool-firstli-a-mouseout').removeClass('sw-pg-selectschool-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-myaccount', function () {
                    $("#sw-pg-myaccount-firstli").removeClass('sw-pg-myaccount-firstli-mouseout').addClass('sw-pg-myaccount-firstli-mouseover');
                    $("#sw-pg-myaccount-firstli a").addClass('sw-pg-myaccount-firstli-a-mouseover').removeClass('sw-pg-myaccount-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-myaccount', function () {
                    $("#sw-pg-myaccount-firstli").removeClass('sw-pg-myaccount-firstli-mouseover').addClass('sw-pg-myaccount-firstli-mouseout');
                    $("#sw-pg-myaccount-firstli a").addClass('sw-pg-myaccount-firstli-a-mouseout').removeClass('sw-pg-myaccount-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-sitenav', function () {
                    $("#sw-pg-sitenav-firstli").removeClass('sw-pg-sitenav-firstli-mouseout').addClass('sw-pg-sitenav-firstli-mouseover');
                    $("#sw-pg-sitenav-firstli a").addClass('sw-pg-sitenav-firstli-a-mouseover').removeClass('sw-pg-sitenav-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-sitenav', function () {
                    $("#sw-pg-sitenav-firstli").removeClass('sw-pg-sitenav-firstli-mouseover').addClass('sw-pg-sitenav-firstli-mouseout');
                    $("#sw-pg-sitenav-firstli a").addClass('sw-pg-sitenav-firstli-a-mouseout').removeClass('sw-pg-sitenav-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-district', function () {
                    $("#sw-pg-district-firstli").removeClass('sw-pg-district-firstli-mouseout').addClass('sw-pg-district-firstli-mouseover');
                    $("#sw-pg-district-firstli a").addClass('sw-pg-district-firstli-a-mouseover').removeClass('sw-pg-district-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-district', function () {
                    $("#sw-pg-district-firstli").removeClass('sw-pg-district-firstli-mouseover').addClass('sw-pg-district-firstli-mouseout');
                    $("#sw-pg-district-firstli a").addClass('sw-pg-district-firstli-a-mouseout').removeClass('sw-pg-district-firstli-a-mouseover');
                });
            }
        });


    </script>
    <script src='https://adamcruse.schoolwires.net/Static//GlobalAssets/Scripts/min/jquery-ui-1.12.0.min.js' type='text/javascript'></script>
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/SW-UI.min.js" type='text/javascript'></script>
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/jquery.sectionlayer.js" type='text/javascript'></script>
    
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/swfobject.min.js" type="text/javascript"></script>
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/jquery.ajaxupload_2440.min.js" type="text/javascript"></script>

    <!-- Begin swuc.CheckScript -->
  <script type="text/javascript" src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/json2.js"></script>
  
<script>var homeURL = location.protocol + "//" + window.location.hostname;

function parseXML(xml) {
    if (window.ActiveXObject && window.GetObject) {
        var dom = new ActiveXObject('Microsoft.XMLDOM');
        dom.loadXML(xml);
        return dom;
    }

    if (window.DOMParser) {
        return new DOMParser().parseFromString(xml, 'text/xml');
    } else {
        throw new Error('No XML parser available');
    }
}

function GetContent(URL, TargetClientID, Loadingtype, SuccessCallback, FailureCallback, IsOverlay, Append) {
    (Loadingtype === undefined ? LoadingType = 3 : '');
    (SuccessCallback === undefined ? SuccessCallback = '' : '');
    (FailureCallback === undefined ? FailureCallback = '' : '');
    (IsOverlay === undefined ? IsOverlay = '0' : '');
    (Append === undefined ? Append = false : '');

    var LoadingHTML;
    var Selector;

    switch (Loadingtype) {
        //Small
        case 1:
            LoadingHTML = "SMALL LOADER HERE";
            break;
            //Large
        case 2:
            LoadingHTML = "<div class='ui-loading large' role='alert' aria-label='Loading content'></div>";
            break;
            // None
        case 3:
            LoadingHTML = "";
            break;
    }

    Selector = "#" + TargetClientID;

    ajaxCall = $.ajax({
        url: URL,
        cache: false,
        beforeSend: function () {
            if (Loadingtype != 3) { BlockUserInteraction(TargetClientID, LoadingHTML, Loadingtype, 0, IsOverlay); }
        },
        success: function (strhtml) {

            // check for calendar and empty div directly surrounding eventlist uc first 
            //      to avoid memory error in IE (Access violation reading location 0x00000018)
            //      need to figure out exactly why this is happening..
            //      Partially to do with this? http://support.microsoft.com/kb/927917/en-us
            //      The error/crash happens when .empty() is called on Selector 
            //          (.html() calls .empty().append() in jquery)
            //      * no one has come across this issue anywhere else in the product so far

            if ($(Selector).find('#calendar-pnl-calendarlist').length > 0) {
                $('#calendar-pnl-calendarlist').empty();
                $(Selector).html(strhtml);
            } else if (Append) {
                $(Selector).append(strhtml);
            }
            else {
                $(Selector).html(strhtml);
            }


            // check for tabindex 
            if ($(Selector).find(":input[tabindex='1']:first").length > 0) {
                $(Selector).find(":input[tabindex='1']:first").focus();
            } else {
                $(Selector).find(":input[type='text']:first").not('.nofocus').focus();
            }

            //if (CheckDirty(Selector) === true) { BindSetDirty(Selector); }
            //CheckDirty(Selector);
            BlockUserInteraction(TargetClientID, '', '', 1);
            (SuccessCallback != '' ? eval(SuccessCallback) : '');
        },
        failure: function () {
            BlockUserInteraction(TargetClientID, '', '', 1);
            (FailureCallback != '' ? eval(FailureCallback) : '');
        }
    });
}

function BlockUserInteraction(TargetClientID, LoadingHTML, Loadingtype, Unblock, IsOverlay) {
    if (LoadingHTML === undefined) {
        LoadingHTML = "<div class='ui-loading large'></div>";
    }

    if (Unblock == 1) {
        $('#' + TargetClientID).unblockinteraction();
    } else {
        if (IsOverlay == 1) {
            $('#' + TargetClientID).blockinteraction({ message: LoadingHTML, type: Loadingtype, isOverlay: true });
        } else {
            $('#' + TargetClientID).blockinteraction({ message: LoadingHTML, type: Loadingtype });
        }
    }
}

function OpenUltraDialogOverlay(OverlayClientID, options, Callback) {
    lastItemClicked = document.activeElement;
    var defaults = {
        LoadType: "U",
        LoadURL: "",
        TargetDivID: "",
        LoadContent: "",
        NoResize: false,
        ScrollTop: false,
        CloseCallback: undefined
    };

    jQuery.extend(defaults, options);

    // check what browser/version we're on
    var isIE = GetIEVersion();

    if (isIE == 0) {
        if ($.browser.mozilla) {
            //Firefox/Chrome
            $("body").css("overflow", "hidden");
        } else {
            //Safari
            $("html").css("overflow", "hidden");
        }
    } else {
        // IE
        $("html").css("overflow", "hidden");
    }

    var OverlaySelector;
    var BodyClientID;
    var TargetDivSelector;
    var CurrentScrollPosition;

    if (defaults.ScrollTop) {
        $.scrollTo(0, { duration: 0 });
    }

    OverlaySelector = "#dialog-ultra-overlay-" + OverlayClientID + "-base";
    BodyClientID = "dialog-ultra-overlay-" + OverlayClientID + "-holder";
    TargetDivSelector = "#" + defaults.TargetDivID;

    if ($.trim($("#dialog-ultra-overlay-" + OverlayClientID + "-holder").html()) != "") {
        CloseUltraDialogOverlay(OverlayClientID);
    }

    // U = URL
    // H - HTML
    // D - Divider

    var success = "";

    if (Callback !== undefined) {
        success += Callback;
    }

    // block user interaction
    BlockUserInteraction(BodyClientID, "<div class='ui-loading large'></div>", 2, 0, 1);

    switch (defaults.LoadType) {
        case 'U':
            GetContent(defaults.LoadURL, BodyClientID, 3, success);
            break;
        case 'H':
            $("#" + BodyClientID).html(defaults.LoadContent);
            break;
        case 'D':
            $("#" + BodyClientID).html($(TargetDivSelector).html());
            break;
    };

    // open the lateral panel
    var browserWidth = $(document).width();

    if (OverlayClientID == "UltraOverlayLarge") {
        browserWidth = browserWidth - 200;
    } else {
        browserWidth = browserWidth - 280;
    }

    $("#dialog-ultra-overlay-" + OverlayClientID + "-body").css("width", browserWidth);
    $(OverlaySelector).addClass("is-visible");

    // hide 'X' button if second window opened
    if (OverlayClientID == "UltraOverlayMedium") {
        $("#dialog-ultra-overlay-UltraOverlayLarge-close").hide();
    }
}

function CloseUltraDialogOverlay(OverlayClientID) {
    $("#dialog-ultra-overlay-" + OverlayClientID + "-base").removeClass("is-visible");

    // show 'X' button if second window closed
    if (OverlayClientID == "UltraOverlayMedium") {
        $("#dialog-ultra-overlay-UltraOverlayLarge-close").show();
    }

    if (OverlayClientID != "UltraOverlayMedium") {
        // check what browser/version we're on
        var isIE = GetIEVersion();

        if (isIE == 0) {
            if ($.browser.mozilla) {
                //Firefox/Chrome
                $("body").css("overflow", "auto");
            } else {
                //Safari
                $("html").css("overflow", "auto");
            }
        } else {
            // IE
            $("html").css("overflow", "auto");
        }
    }
    //focus back on the element clicked to open the dialog
    if (lastItemClicked !== undefined) {
        lastItemClicked.focus();
        lastItemClicked = undefined;
    }
}

//Save the last item clicked when opening a dialog overlay so the focus can go there upon close
var lastItemClicked;
function OpenDialogOverlay(OverlayClientID, options, Callback) {
    lastItemClicked = document.activeElement;
    var defaults = {
        LoadType: 'U',
        LoadURL: '',
        TargetDivID: '',
        LoadContent: '',
        NoResize: false,
        ScrollTop: false,
        CloseCallback: undefined
    };

    jQuery.extend(defaults, options);

    //check what browser/version we're on
    var isIE = GetIEVersion();

    //if (isIE == 0) {
    //    if ($.browser.mozilla) {
    //        //Firefox/Chrome
    //        $('body').css('overflow', 'hidden');
    //    } else {
    //        //Safari
    //        $('html').css('overflow', 'hidden');
    //    }
    //} else {
    //    // IE
    //    $('html').css('overflow', 'hidden');
    //}
    $('html').css('overflow', 'hidden');

    var OverlaySelector;
    var BodyClientID;
    var TargetDivSelector;

    if (defaults.ScrollTop) {
        $.scrollTo(0, { duration: 0 });
    }

    OverlaySelector = "#dialog-overlay-" + OverlayClientID + "-base";
    BodyClientID = "dialog-overlay-" + OverlayClientID + "-body";
    TargetDivSelector = "#" + defaults.TargetDivID;

    $(OverlaySelector).appendTo('body');

    $("#" + OverlayClientID).css("top", "5%");

    $("#" + BodyClientID).html("");

    if (isIEorEdge()) {
        $(OverlaySelector).show();
    } else {
        $(OverlaySelector).fadeIn();
    }

    if ($.trim($('#dialog-overlay-' + OverlayClientID + '-body').html()) != "") {
        CloseDialogOverlay(OverlayClientID);
    }

    // U = URL
    // H - HTML
    // D - Divider

    var success = "";

    if (Callback !== undefined) {
        success += Callback;
    }

    // Block user interaction
    $('#' + BodyClientID).css({ 'min-height': '100px' });
    BlockUserInteraction(BodyClientID, "<div class='ui-loading large'></div>", 2, 0, 1);

    switch (defaults.LoadType) {
        case 'U':
            GetContent(defaults.LoadURL, BodyClientID, 3, success);
            break;
        case 'H':
            $("#" + BodyClientID).html(defaults.LoadContent);
            break;
        case 'D':
            $("#" + BodyClientID).html($(TargetDivSelector).html());
            break;

    };

    if (defaults.CloseCallback !== undefined) {
        $(OverlaySelector + ' .ui-dialog-overlay-close').attr('onclick', 'CloseDialogOverlay(\'' + OverlayClientID + '\',' + defaults.CloseCallback + ')')
    }

    // check for tabindex 
    if ($("#" + BodyClientID).find(":input[tabindex='1']:first").length > 0) {
        $("#" + BodyClientID).find(":input[tabindex='1']:first").focus();
    } else if ($("#" + BodyClientID).find(":input[type='text']:first").length > 0) {
        $("#" + BodyClientID).find(":input[type='text']:first").focus();
    } else {
        $("#" + BodyClientID).parent().focus();
    }

    $('#dialog-overlay-' + OverlayClientID + '-base').css({'overflow': 'auto', 'top': '0', 'width': '100%', 'left': '0' });
}

function GetIEVersion() {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");

    if (Idx > 0) {
        // If IE, return version number
        return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
    } else if (!!navigator.userAgent.match(/Trident\/7\./)) {
        // If IE 11 then look for Updated user agent string
        return 11;
    } else {
        //It is not IE
        return 0;
    }
}

function isIEorEdge() {
    var agent = window.navigator.userAgent;

    var ie = agent.indexOf('MSIE ');
    if (ie > 0) {
        // IE <= 10
        return parseInt(agent.substring(ie + 5, uaagentindexOf('.', ie)), 10);
    }

    var gum = agent.indexOf('Trident/');
    if (gum > 0) {
        // IE 11
        var camper = agent.indexOf('rv:');
        return parseInt(agent.substring(camper + 3, agent.indexOf('.', camper)), 10);
    }

    var linkinPark = agent.indexOf('Edge/');
    if (linkinPark > 0) {
        return parseInt(agent.substring(linkinPark + 5, agent.indexOf('.', linkinPark)), 10);
    }

    // other browser
    return false;
}

function SendEmail(to, from, subject, body, callback) {

    var data = "{ToEmailAddress: '" + to + "', " +
        "FromEmailAddress: '" + from + "', " +
        "Subject: '" + subject + "', " +
        "Body: '" + body + "'}";
    var url = homeURL + "/GlobalUserControls/SE/SEController.aspx/SE";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (msg) {
            callback(msg.d);
        },
        headers: { 'X-Csrf-Token': GetCookie('CSRFToken') }
    });
}

// DETERMINE TEXT COLOR 

function rgbstringToTriplet(rgbstring) {
    var commadelim = rgbstring.substring(4, rgbstring.length - 1);
    var strings = commadelim.split(",");
    var numeric = [];

    for (var i = 0; i < 3; i++) {
        numeric[i] = parseInt(strings[i]);
    }

    return numeric;
}

function adjustColour(someelement) {
    var rgbstring = someelement.css('background-color');
    var triplet = [];
    var newtriplet = [];

    if (rgbstring != 'transparent') {
        if (/rgba\(0, 0, 0, 0\)/.exec(rgbstring)) {
            triplet = [255, 255, 255];
        } else {
            if (rgbstring.substring(0, 1).toLowerCase() != 'r') {
                CheckScript('RGBColor', staticURL + '/GlobalAssets/Scripts/ThirdParty/rgbcolor.js');
                // not rgb, convert it
                var color = new RGBColor(rgbstring);
                rgbstring = color.toRGB();
            }
            triplet = rgbstringToTriplet(rgbstring);
        }
    } else {
        triplet = [255, 255, 255];
    }

    // black or white:
    var total = 0; for (var i = 0; i < triplet.length; i++) { total += triplet[i]; }

    if (total > (3 * 256 / 2)) {
        newtriplet = [0, 0, 0];
    } else {
        newtriplet = [255, 255, 255];
    }

    var newstring = "rgb(" + newtriplet.join(",") + ")";

    someelement.css('color', newstring);
    someelement.find('*').css('color', newstring);

    return true;
}


// END DETERMINE TEXT color

function CheckScript2(ModuleName, ScriptSRC) {
    $.ajax({
        url: ScriptSRC,
        async: false,
        //context: document.body,
        success: function (html) {
            var script =
				document.createElement('script');
            document.getElementsByTagName('head')[0].appendChild(script);
            script.text = html;
        }
    });
}

// AREA / SCREEN CODES

function setCurrentScreenCode(screenCode) {
    SetCookie('currentScreenCode', screenCode);
    AddAnalyticsEvent(getCurrentAreaCode(), screenCode, 'Page View');
}

function getCurrentScreenCode() {
    var cookieValue = GetCookie('currentScreenCode');
    return (cookieValue != '' ? cookieValue : 0);
}

function setCurrentAreaCode(areaCode) {
    SetCookie('currentAreaCode', areaCode);
}

function getCurrentAreaCode() {
    var cookieValue = GetCookie('currentAreaCode');
    return (cookieValue != '' ? cookieValue : 0);
}

// END AREA / SCREEN CODES

// CLICK HOME TAB IN HEADER SECTION

function GoHome() {
    window.location.href = homeURL + "/cms/Workspace";
}

// END CLICK HOME TAB IN HEADER SECTION

// HELP PANEL

function OpenHelpPanel() {
    var URLScreenCode = getCurrentScreenCode();
    
    if (URLScreenCode == "" || URLScreenCode == 0) {
        URLScreenCode = getCurrentAreaCode();
        if (URLScreenCode == 0) {
            URLScreenCode = "";
        }
    } 

    AddAnalyticsEvent("Help", "How Do I...?", URLScreenCode);

    //help site url stored in webconfig, passed in to BBHelpURL from GlobalJSVar and GlobalJS.cs
    var HelpURL = BBHelpURL + URLScreenCode;
    window.open(HelpURL,"_blank");
}

// END HELP PANEL

// COOKIES
function SetCookie(name, value, days, ms) {
    var expires = "";

    if (ms) {
        var date = new Date();

        date.setMilliseconds(date.getMilliseconds() + ms);
        expires = "; expires=" + date.toGMTString();
    } else if (days) {
        var date = new Date();

        date.setDate(date.getDate() + days);
        expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + escape(value) + expires + "; path=/";
}

function GetCookie(name) {
    var value = "";

    if (document.cookie.length > 0) {
        var start = document.cookie.indexOf(name + "=");

        if (start != -1) {
            start = start + name.length + 1;

            var end = document.cookie.indexOf(";", start);

            if (end == -1) end = document.cookie.length;
            value = unescape(document.cookie.substring(start, end));
        }
    }

    return value;
}

function DeleteCookie(name) {
    SetCookie(name, '', -1);
}

function SetUnescapedCookie(name, value, days, ms) {
    var expires = "";

    if (ms) {
        var date = new Date();

        date.setMilliseconds(date.getMilliseconds() + ms);
        expires = "; expires=" + date.toGMTString();
    } else if (days) {
        var date = new Date();

        date.setDate(date.getDate() + days);
        expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + value + expires + "; path=/";
}
// END COOKIES



// IFRAME FUNCTIONS
function BindResizeFrame(FrameID) {
    $(document).on('load', "#" + FrameID, function () {
        var bodyHeight = $("#" + FrameID).contents().height() + 40;
        $("#" + FrameID).attr("height", bodyHeight + "px");
    });
}

function AdjustLinkTarget(FrameID) {
    $(document).on('load', "#" + FrameID, function () {
        $("#" + FrameID).contents().find("a").attr("target", "_parent");
    });
}

function ReloadDocViewer(moduleInstanceID, retryCount) {
    var iframe = document.getElementById('doc-viewer-' + moduleInstanceID);

    //if document failed to load and retry count is less or equal to 3, reload document and check again
    if (iframe.contentWindow.frames.length == 0 && retryCount <= 3) {
        retryCount = retryCount + 1;

        //reload the iFrame
        document.getElementById('doc-viewer-' + moduleInstanceID).src += '';

        //Check if document loaded again in 7.5 seconds
        setTimeout(ReloadDocViewer, (1000 * retryCount), moduleInstanceID, retryCount);

    } else if (iframe.contentWindow.frames.length == 0 && retryCount > 3) {
        $('#doc-viewer-' + moduleInstanceID).css('background', '');
        iframe.src = "/Errors/ReloadPage.aspx";
        iframe.height = 200;
    }

    if (iframe.contentWindow.frames.length == 1) {
        $('#doc-viewer-' + moduleInstanceID).css('background', '');
    }
}
// END IFRAME FUNCTIONS


// SCROLL TOP

function ScrollTop() {
    $.scrollTo(0, { duration: 1000 });
}

// END SCROLL TOP

// ANALYTICS TRACKING

function AddAnalyticsEvent(category, action, label) {
    ga('BBTracker.send', 'event', category, action, label);
}

// END ANYLYTICS TRACKING


// BEGIN INCLUDE DOC READY SCRIPTS

function IncludeDocReadyScripts() {
    var arrScripts = [
		staticURL + '/GlobalAssets/Scripts/min/external-combined.min.js',
		staticURL + '/GlobalAssets/Scripts/Utilities.js'

    ];

    var script = document.createElement('script');
    script.type = 'text/javascript';
    $.each(arrScripts, function () {script.src = this;$('head').append(script);;
        
    });

}
// END INCLUDE DOC READY SCRIPTS

//BEGIN ONSCREEN ALERT SCRIPTS
function OnScreenAlertDialogInit() {
    $("#onscreenalert-message-dialog").hide();
    $(".titleMessage").hide();
    $("#onscreenalert-ctrl-msglist").hide();
    $(".onscreenalert-ctrl-footer").hide();

    $(".icon-icon_alert_round_message").addClass("noBorder");
    $('.onscreenalert-ctrl-modal').addClass('MoveIt2 MoveIt1 Smaller');
    $('.onscreenalert-ctrl-modal').css({ "top": "88%" });
    $("#onscreenalert-message-dialog").show();
    $('#onscreenalert-message-dialog-icon').focus();
}

function OnScreenAlertGotItDialog(serverTime) {
    var alertsID = '';
    alertsID = GetCookie("Alerts");
    var arr = [];

    if (alertsID.length > 0) {
        arr = alertsID.split(',')
    }

    $("#onscreenalertdialoglist li").each(function () {
        arr.push($(this).attr('id'));
    });

    arr.sort();
    var newarr = $.unique(arr);

    SetUnescapedCookie("Alerts", newarr.join(','), 365);
    OnScreenAlertMaskShow(false);

    $('.onscreenalert-ctrl-modal').removeClass("notransition");
    $(".onscreenalert-ctrl-footer").slideUp(200);
    $("#onscreenalert-ctrl-msglist").slideUp(200);

    // check what browser/version we're on
    // if IE 9 or less, don't do CSS 3 transitions (not supported)
    var isIE = GetIEVersion();

    if (isIE == 0) {
        // not IE
        hideOnScreenAlertTransitions();
    } else {
        // IE
        if (isIE == 8 || isIE == 9) {
            hideOnScreenAlertNoTransitions();
        } else if (isIE >= 10) {
            hideOnScreenAlertTransitions();
        }
    }
}

function OnScreenAlertOpenDialog(SiteID) {
    $('#sw-important-message-tooltip').hide();
    var onscreenAlertCookie = GetCookie('Alerts');

    if (onscreenAlertCookie != '' && onscreenAlertCookie != undefined) {
        GetContent(homeURL + "/cms/Tools/OnScreenAlerts/UserControls/OnScreenAlertDialogListWrapper.aspx?OnScreenAlertCookie=" + onscreenAlertCookie + "&SiteID=" + SiteID, "onscreenalert-ctrl-cookielist", 2, "OnScreenAlertCookieSuccess();");
        $('.onscreenalert-ctrl-content').focus();
    }
}

function OnScreenAlertCookieSuccess() {
    OnScreenAlertMaskShow(true);

    $('.onscreenalert-ctrl-modal').removeClass("notransition");
    $('.onscreenalert-ctrl-modal').removeClass("Smaller");

    // check what browser/version we're on
    // if IE 9 or less, don't do CSS 3 transitions (not supported)
    var isIE = GetIEVersion();

    if (isIE == 0) {
        // not IE
        showOnScreenAlertTransitions();
    } else {
        // IE
        if (isIE == 8 || isIE == 9) {
            showOnScreenAlertNoTransitions();
        } else if (isIE >= 10) {
            showOnScreenAlertTransitions();
        }
    }

    $('.onscreenalert-ctrl-modal').addClass('MoveIt3');
    $('.onscreenalert-ctrl-modal').attr('style', '');

    // move div to top of viewport
    $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
        var top = $('.onscreenalert-ctrl-modal').offset().top;

        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt1').addClass('MoveIt4');

        $('.onscreenalert-ctrl-modal').removeClass('MoveIt2');
    });
}

function showOnScreenAlertTransitions() {
    $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
        $(".icon-icon_alert_round_message").removeClass("noBorder");
        $(".titleMessage").show();
        $("#onscreenalert-ctrl-msglist").slideDown(200);
        $(".onscreenalert-ctrl-footer").slideDown(200);
    });
}

function showOnScreenAlertNoTransitions() {
    $(".icon-icon_alert_round_message").removeClass("noBorder");
    $(".titleMessage").show();
    $("#onscreenalert-ctrl-msglist").slideDown(200);
    $(".onscreenalert-ctrl-footer").slideDown(200);
}

function hideOnScreenAlertTransitions() {
    $(".titleMessage").fadeOut(200, function () {
        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt4').addClass('MoveIt1');
        $('.onscreenalert-ctrl-modal').attr('style', '');
        $('.onscreenalert-ctrl-modal').addClass("Smaller");
        $(".icon-icon_alert_round_message").addClass("noBorder");

        $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
            $('.onscreenalert-ctrl-modal').removeClass("notransition");
            $('.onscreenalert-ctrl-modal').removeClass('MoveIt3').addClass('MoveIt2');
            $('.onscreenalert-ctrl-modal').css({
                "top": "88%"
            });
        });
    });
}

function hideOnScreenAlertNoTransitions() {
    $(".titleMessage").fadeOut(200, function () {
        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt4').addClass('MoveIt1');
        $('.onscreenalert-ctrl-modal').attr('style', '');
        $('.onscreenalert-ctrl-modal').addClass("Smaller");
        $(".icon-icon_alert_round_message").addClass("noBorder");

        $('.onscreenalert-ctrl-modal').removeClass("notransition");
        $('.onscreenalert-ctrl-modal').removeClass('MoveIt3').addClass('MoveIt2');
        $('.onscreenalert-ctrl-modal').css({
            "top": "88%"
        });
    });
}

function OnScreenAlertCheckListItem() {
    var ShowOkGotIt = $("#onscreenalertdialoglist-hid-showokgotit").val();
    var ShowStickyBar = $("#onscreenalertdialoglist-hid-showstickybar").val();

    if (ShowOkGotIt == "True" && ShowStickyBar == "False") {
        OnScreenAlertShowCtrls(true, true, false);
    } else if (ShowOkGotIt == "False" && ShowStickyBar == "True") {
        OnScreenAlertShowCtrls(true, false, true);
    } else {
        OnScreenAlertShowCtrls(false, false, false);
    }
}

function OnScreenAlertShowCtrls(boolOkGotIt, boolMask, boolStickyBar) {
    if (boolOkGotIt == false) {
        $("#onscreenalert-message-dialog").hide()
    }

    (boolMask == true) ? (OnScreenAlertMaskShow(true)) : (OnScreenAlertMaskShow(false));

    if (boolStickyBar == true) {
        OnScreenAlertDialogInit();
    }
    else {
        $('.onscreenalert-ctrl-content').focus();
    }
    
}

function OnScreenAlertMaskShow(boolShow) {
    if (boolShow == true) {
        $("#onscreenalert-ctrl-mask").css({ "position": "absolute", "background": "#000", "filter": "alpha(opacity=70)", "-moz-opacity": "0.7", "-khtml-opacity": "0.7", "opacity": "0.7", "z-index": "9991", "top": "0", "left": "0" });
        $("#onscreenalert-ctrl-mask").css({ "height": function () { return $(document).height(); } });
        $("#onscreenalert-ctrl-mask").css({ "width": function () { return $(document).width(); } });
        $("#onscreenalert-ctrl-mask").show();
        $('body').css('overflow', 'hidden');
    } else {
        $("#onscreenalert-ctrl-mask").hide();
        $('body').css('overflow', 'scroll');
    }
}
// END ONSCREEN ALERT SCRIPTS

// Encoding - obfuscation
function swrot13(s) {
    return s.replace(/[a-zA-Z]/g, function(c) {return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);})
}

// START SIDEBAR TOUR SCRIPTS
/* 
 *  Wrapping sidebar tour in a function 
 *  to be called when needed.
 */

var hasPasskeys = false;
var hasStudents = false;
var isParentLinkStudent = false;
var hasNotifications = false;

function startSidebarTour() {
    // define tour
    var tour = new Shepherd.Tour({
        defaults: {
            classes: 'shepherd-theme-default'
        }
    });

    if ($('#dashboard-sidebar-student0').length > 0) {
        hasStudents = true;
    }

    if ($('#dashboard-sidebar-profile').length > 0) {
        isParentLinkStudent = true;
    }

    if ($("#dashboard-sidebar-passkeys").get(0)) {
        hasPasskeys = true;
    }

    if ($("#dashboard-sidebar-notification").get(0)) {
        hasNotifications = true;
    }


    // define steps

    tour.addStep('Intro', {
        //title: '',
        text: '<div id="tour-lightbulb-icon"></div><p>Introducing your new personalized dashboard. Would you like to take a quick tour?</p>',
        attachTo: 'body',
        classes: 'dashboard-sidebar-tour-firststep shepherd-theme-default',
        when: {
            show: function () {
                AddAnalyticsEvent('Dashboard', 'Tour', 'View');
            }
        },
        buttons: [
          {
              text: 'Yes! Let\'s go.',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Continue');
                  tour.next();
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour);
                  }
              }
          },
          {
              text: 'Maybe later.',
              classes: 'shepherd-button-sec',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Hide For Now');
                  tour.show('Later');
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour, 'Later');
                  }
              }
          },
          {
              text: 'No thanks. I\'ll explore on my own.',
              classes: 'shepherd-button-suboption',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Hide Forever');
                  tour.show('Never');
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour, 'Never');
                  }
              }
          }
        ]
    }).addStep('Avatar', {
        text: 'Click on your user avatar to access and update your personal information and subscriptions.',
        attachTo: '#dashboard-sidebar-avatar-container right',
        when: {
            show: function() {
                $('h3.shepherd-title').attr('role', 'none'); //ADA compliance
            }
        },
        classes: 'shepherd-theme-default shepherd-element-custom',
        buttons: [
          {
              text: 'Continue',
              action: function () {
                tour.next();
                TourADAFocus();

              },
              events: {
                  'keydown': function (e) {
                      if (hasPasskeys) {
                          TourButtonPress(e, tour);
                      } else {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
          }
        ]
    }).addStep('Stream', {
        text: 'Go to your stream to see updates from your district and schools.',
        attachTo: '#dashboard-sidebar-stream right',
        buttons: [
          {
              text: 'Continue',
              action: function () {
                  if (hasPasskeys) {
                      tour.next();
                      TourADAFocus();
                  } else {
                      tour.show('Finish');
                      TourADAFocus();
                  }
              },
              events: {
                  'keydown': function (e) {
                      if(hasPasskeys){
                          TourButtonPress(e, tour);
                      } else {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
          }
        ]
    });

    if (hasPasskeys) {
        tour.addStep('Passkeys', {
            text: 'Use your passkeys to log into other district applications or websites.',
            attachTo: '#dashboard-sidebar-passkeys right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      if (hasNotifications) {
                          tour.show('Notifications');
                          TourADAFocus();
                      }
                      else if (hasStudents) {
                          tour.show('Students');
                          TourADAFocus();
                      }
                      else if (isParentLinkStudent) {
                          tour.show('ParentLinkStudent');
                          TourADAFocus();
                      }
                      else {
                          tour.show('Finish');
                          TourADAFocus();
                      }
                  },
                  events: {
                      'keydown': function (e) {
                          if (hasStudents) {
                              TourButtonPress(e, tour, 'Notifications');
                          }
                          else {
                              TourButtonPress(e, tour, 'Finish');
                          }
                      }
                  }
              }
            ]
        })
    }

    if (hasNotifications) {
        tour.addStep('Notifications', {
            text: 'Open your notifications to review messages.',
            attachTo: '#dashboard-sidebar-notification right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      if (hasStudents) {
                          tour.show('Students');
                          TourADAFocus();
                      }
                      else if (isParentLinkStudent) {
                          tour.show('ParentLinkStudent');
                          TourADAFocus();
                      }
                      else {
                          tour.show('Finish');
                          TourADAFocus();
                      }
                  },
                  events: {
                      'keydown': function (e) {
                          if (hasStudents) {
                              TourButtonPress(e, tour, 'Students');
                          }
                          else {
                              TourButtonPress(e, tour, 'Finish');
                          }
                      }
                  }
              }
            ]
        })
    }

    if (hasStudents) {
        tour.addStep('Students', {
            text: 'Select a student to view his or her information and records.',
            attachTo: '#dashboard-sidebar-student0 right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      tour.show('Finish');
                      TourADAFocus();
                  },
                  events: {
                      'keydown': function (e) {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
            ]
        })
    }

    if (isParentLinkStudent) {
        tour.addStep('ParentLinkStudent', {
            text: 'View your information or other helpful resources.',
            attachTo: '#dashboard-sidebar-profile right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      tour.show('Finish');
                      TourADAFocus();
                  },
                  events: {
                      'keydown': function (e) {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
            ]
        })
    }

    tour.addStep('Later', {
        text: 'Ok, we\'ll remind you later. You can access the tour here any time you want.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  tour.cancel();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          tour.cancel();
                      }
                  }
              }
          }
        ]

    }).addStep('Never', {
        text: 'Ok, we won\'t ask you again. If you change your mind, you can access the tour here any time you want.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  DenyTour();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          DenyTour();
                      }
                  }
              }
          }
        ]

    }).addStep('Finish', {
        text: 'All finished! Go here any time to view this tour again.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  FinishTour();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          FinishTour();
                      }
                  }
              }
          }
        ]
    });

    // start tour
    tour.start();
    TourADA();
}

function TourADA() {
    $(".shepherd-content").attr("tabindex", "0");
    $(".shepherd-button").each(function () {
        var $this = $(this);
        $this.attr("title", $this.text());
        $this.attr("tabindex", "0");
    });
}

function TourADAFocus() {
    TourADA();
    $(".shepherd-content").focus();
}

function TourButtonPress(e, tour, next) {
    e.stopImmediatePropagation();
    if (e.keyCode == 13) {
        if (next == undefined) {
            tour.next();
        } else {
            tour.show(next);
        }
        TourADAFocus();
    }
}

function FinishTour() {
    var data = "";
    var success = 'ActiveTourCancel();';
    var failure = 'CallControllerFailure(result[0].errormessage);';
    CallController(homeURL + "/GlobalUserControls/DashBoardSideBar/DashBoardSideBarController.aspx/FinishTour", data, success, failure);
}

function DenyTour() {
    var data = "";
    var success = 'ActiveTourCancel();';
    var failure = 'CallControllerFailure(result[0].errormessage);';
    CallController(homeURL + "/GlobalUserControls/DashBoardSideBar/DashBoardSideBarController.aspx/DenyTour", data, success, failure);
}

function ActiveTourCancel() {
    Shepherd.activeTour.cancel();
}
// END SIDEBAR TOUR SCRIPTS

// BEGIN DOCUMENT READY
$(document).ready(function () {

    if ($('#sw-sidebar').height() > 1500) {
        $('#sw-page').css('min-height', $('#sw-sidebar').height() + 'px');
        $('#sw-inner').css('min-height', $('#sw-sidebar').height() + 'px');
    } else {
        $('#sw-page').css('min-height', $(document).height() + 'px');
        $('#sw-inner').css('min-height', $(document).height() + 'px');
    }

    $('#sw-footer').show();

    // add focus class to textboxes for IE
    $(document).on('focus', 'input', function () {
        $(this).addClass('focus');
    });

    $(document).on('blur', 'input', function () {
        $(this).removeClass('focus');
    });

    // default ajax setup
    $.ajaxSetup({
        cache: false,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //swalert(XMLHttpRequest.status + ' ' + textStatus + ': ' + errorThrown, 'Ajax Error', 'critical', 'ok');
            swalert("Something went wrong. We're sorry this happened. Please refresh your page and try again.", "Error", "critical", "ok");
        }
    });

    // make :Contains (case-insensitive version of :contains)
    jQuery.expr[':'].Contains = function (a, i, m) {
        return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };

    // HIDE / SHOW DETAILS IN LIST SCREEN
    $(document).on('click', 'span.ui-show-detail', function () {
        var $this = $(this);

        if ($this.hasClass('open')) {
            // do nothing
        } else {
            $this.addClass('open').parent('div.ui-article-header').nextAll('div.ui-article-detail').slideDown(function () {
                // add close button
                $this.append("<span class='ui-article-detail-close'></span>");
            });
        }
    });

    $(document).on('click', 'span.ui-article-detail-close', function () {
        $this = $(this);
        $this.parents('.ui-article-header').nextAll('.ui-article-detail').slideUp(function () {
            $this.parent('.ui-show-detail').removeClass('open');
            // remove close button
            $this.remove();
        });
    });


    // LIST/EXPANDED VIEW ON LIST PAGES
    $(document).on('click', '#show-list-view', function () {
        $(this).addClass('ui-btn-toolbar-primary').removeClass('ui-btn-toolbar');
        $('#show-expanded-view').addClass('ui-btn-toolbar').removeClass('ui-btn-toolbar-primary');
        $('div.ui-article-detail').slideUp(function () {
            // remove close buttons
            $(this).prevAll('.ui-article-header').find('.ui-article-detail-close').remove();
            //$this.children('.ui-article-detail-close').remove();
        });
        $('span.ui-show-detail').removeClass('open');
    });

    $(document).on('click', '#show-expanded-view', function (i) {
        $(this).addClass('ui-btn-toolbar-primary').removeClass('ui-btn-toolbar');
        $('#show-list-view').addClass('ui-btn-toolbar').removeClass('ui-btn-toolbar-primary');
        $('div.ui-article-detail').slideDown('', function () {
            // add close buttons
            $(this).prevAll('.ui-article-header').children('.ui-show-detail').append("<span class='ui-article-detail-close'></span>");
        });
        $('span.ui-show-detail').addClass('open');
    });

    //Important Message Ok, got it tab=0
    $('#onscreenalert-ctrl-gotit').keydown(function (e) {
        e.stopImmediatePropagation();
        if (e.keyCode == 13) {
            $(this).click();
        }
    });

    $('#onscreenalert-message-dialog-icon').keydown(function (e) {
        e.stopImmediatePropagation();
        if (e.keyCode == 13) {
            $(this).click();
        }
    });
    
}); // end document ready

// load scripts after everything else
$(window).on('load',  IncludeDocReadyScripts);
﻿/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
// Check for included script
function CheckScript(ModuleName, ScriptSRC, FunctionName) {

    var loadScriptFile = true;

    switch (ModuleName.toLowerCase()) {
        case 'sectionrobot':
            FunctionName = 'CheckSectionRobotScript';
            ScriptSRC = homeURL + '/cms/Tools/SectionRobot/SectionRobot.js';
            break;
        case 'assignments':
            FunctionName = 'CheckAssignmentsScript';
            ScriptSRC = homeURL + '/cms/Module/Assignments/Assignments.js';
            break;
        case 'spacedirectory':
            FunctionName = 'CheckSpaceDirectoryScript';
            ScriptSRC = homeURL + '/cms/Module/SpaceDirectory/SpaceDirectory.js';
            break;
        case 'calendar':
            FunctionName = 'CheckCalendarScript';
            ScriptSRC = homeURL + '/cms/Module/Calendar/Calendar.js';
            break;
        case 'fullcalendar':
            FunctionName = '$.fn.fullCalendar';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/jquery.fullcalendar1.6.1.js';
            break;
        case 'links':
            FunctionName = 'CheckLinksScript';
            ScriptSRC = staticURL + '/cms/Module/Links/Links.js';
            break;
        case 'minibase':
            FunctionName = 'CheckMinibaseScript';
            ScriptSRC = homeURL + '/cms/Module/Minibase/Minibase.js';
            break;
        case 'moduleinstance':
            var randNum = Math.floor(Math.random() * (1000 - 10 + 1) + 1000);
            FunctionName = 'CheckModuleInstanceScript';
            ScriptSRC = homeURL + '/cms/Module/ModuleInstance/ModuleInstance.js?rand=' + randNum;
            break;
        case 'photogallery':
            FunctionName = 'CheckPhotoGalleryScript';
            ScriptSRC = staticURL + '/cms/Module/PhotoGallery/PhotoGallery_2520.js';
            break;
        case 'comments':
            FunctionName = 'CheckModerateCommentsScript';
            ScriptSRC = homeURL + '/cms/Tools/ModerateComments/ModerateComments.js';
            break;
        case 'postings':
            FunctionName = 'CheckModeratePostingsScript';
            ScriptSRC = homeURL + '/cms/Tools/ModerateContribution/ModerateContribution.js';
            break;
        case 'myaccount':
            FunctionName = 'CheckMyAccountScript';
            ScriptSRC = homeURL + '/cms/UserControls/MyAccount/MyAccount.js';
            break;
        case 'formsurvey':
            FunctionName = 'CheckFormSurveyScript';
            ScriptSRC = homeURL + '/cms/tools/FormsAndSurveys/Surveys.js';
            break;
        case 'alerts':
            FunctionName = 'CheckAlertsScript';
            ScriptSRC = homeURL + '/cms/tools/Alerts/Alerts.js';
            break;
        case 'onscreenalerts':
            FunctionName = 'CheckOnScreenalertsScript';
            ScriptSRC = homeURL + '/cms/tools/OnScreenAlerts/OnScreenAlerts.js';
            break;
        case 'workspace':
            FunctionName = 'CheckWorkspaceScript';
            ScriptSRC = staticURL + '/cms/Workspace/PageList.js';
            break;
        case 'moduleview':
            FunctionName = 'CheckModuleViewScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/ModuleViewRenderer_2460.js';
            break;
        case 'pageeditingmoduleview':
            FunctionName = 'CheckPageEditingModuleViewScript';
            //ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/PageEditingModuleViewRenderer.js';
            ScriptSRC = homeURL + '/cms/UserControls/ModuleView/PageEditingModuleViewRenderer.js';
            break;
        case 'editarea':
            FunctionName = 'CheckEditAreaScript';
            ScriptSRC = homeURL + '/GlobalUserControls/EditArea/edit_area_full.js';
            break;
        case 'rating':
            FunctionName = '$.fn.rating';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/jquery.rating.min.js';
            break;
        case 'metadata':
            FunctionName = '$.fn.metadata';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/jquery.metadata.min.js';
            break;
        case 'pwcalendar':
            FunctionName = 'CheckPWCalendarScript';
            ScriptSRC = homeURL + '/myview/UserControls/Calendar/Calendar.js';
            break;
        case 'importwizard':
            FunctionName = 'CheckImportWizardScript';
            ScriptSRC = homeURL + '/cms/UserControls/ImportDialog/ImportWizard.js';
            break;
        case 'mustache':
            FunctionName = 'CheckMustacheScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/mustache.js';
            break;
        case 'slick':
            FunctionName = 'CheckSlickScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/Slick/slick.min.js';
            break;
        case 'galleria':
            FunctionName = 'CheckGalleriaScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/galleria-custom-129_2520/galleria-1.2.9.min.js';
            break;
        case 'fine-uploader':
            FunctionName = 'CheckFineUploaderScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/fine-uploader/fine-uploader.min.js';
            break;
        case 'tinymce':
            FunctionName = 'tinyMCE';
            ScriptSRC = homeURL + '/cms/Module/selectsurvey/ClientInclude/tinyMCE/jquery.tinymce.min.js';
            break;
        case 'attachmentview':
            FunctionName = 'CheckAttachmentScript';
            ScriptSRC = homeURL + '/GlobalUserControls/Attachment/AttachmentView.js';
            break;
        default:
            // module name not found
            if (ScriptSRC !== undefined && ScriptSRC !== null && ScriptSRC.length > 0) {
                // script src was specified in parameter
                if (FunctionName === undefined && ModuleName.length > 0) {
                    // default the function name for lookup from the module name
                    FunctionName = 'Check' + ModuleName + 'Script';
                }
            } else {
                // can't load a script file without at least a src and function name to test
                loadScriptFile = false;
            }
            break;
    }

    if (loadScriptFile === true) {
        try {
            if (eval("typeof " + FunctionName + " == 'function'")) {
                // do nothing, it's already included
            } else {
                var script = document.createElement('script');script.type = 'text/javascript';script.src = ScriptSRC;$('head').append(script);;
            }
        } catch (err) {
        }
    }
}
// End Check for included script
</script><!-- End swuc.CheckScript -->


    <!-- Server Load Time (01): 0.2839007 Seconds -->

    

    <!-- off-canvas menu enabled-->
    

    <!-- Ally Alternative Formats Configure START   -->
    
    <!-- Ally Alternative Formats Configure END     -->

</body>
</html>
